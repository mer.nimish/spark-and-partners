<?php
session_start();
require 'config/config.php';
require 'model/model.php';
global $conn;


if (isset($_SESSION['first_name']) && isset($_SESSION['last_name'])) {
    $first_name = $_SESSION['first_name'];
    $last_name = $_SESSION['last_name'];
} else {
    echo "<script>window.location='index.php'</script>";
    exit;
}

if ($_REQUEST['ct_id'] != '') {
    $single_contact = singleContact($_REQUEST['ct_id']);   
    if (empty($single_contact)) {
        echo "<script>window.location='index.php'</script>";
        exit;
    }
} else {
    echo "<script>window.location='index.php'</script>";
    exit;
}


include('header.php');
include('left_sidebar.php');
?>
<link href="dist/css/lightbox.min.css" rel="stylesheet" type="text/css">
<div class="page-wrapper">
    <div class="container-fluid pt-25">	

        <!-- Row -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default card-view building">

                    <div class="panel-wrapper collapse in">
                        <h5><i class="fa fa-user"></i> Contact details</h5>
                        <div class="row">
                            <div class="col-md-6">                                    
                                <div class="building-group">
                                    <div class="building-detail">
                                        <div class="item">Name</div>
                                        <div class="value"><?php echo $single_contact['vFirstName'] . ' ' . $single_contact['vLastName']; ?></div>
                                    </div>
                                </div>
                                <div class="building-group">
                                    <div class="building-detail">
                                        <div class="item">Type</div>
                                        <div class="value"><?php echo $single_contact['vContactType']; ?></div>
                                    </div>
                                </div>
                                <div class="building-group">
                                    <div class="building-detail">
                                        <div class="item">Phone</div>
                                        <div class="value"><?php echo $single_contact['vPhone']; ?></div>
                                    </div>
                                </div>
                                <div class="building-group">
                                    <div class="building-detail">
                                        <div class="item">Email</div>
                                        <div class="value"><?php echo $single_contact['vEmail']; ?></div>
                                    </div>
                                </div>
                                <div class="building-group">
                                    <div class="building-detail">
                                        <div class="item">Date of birth</div>
                                        <div class="value"><?php echo $single_contact['dDateofbirth']; ?></div>
                                    </div>
                                </div>

                            </div> 
                            <div class="col-md-6">
                                <div class="building-group">
                                    <div class="building-detail">
                                        <div class="item">Address</div>
                                        <div class="value"><?php echo $single_contact['vAddress']; ?></div>
                                    </div>
                                </div>
                                <div class="building-group">
                                    <div class="building-detail">
                                        <div class="item">City</div>
                                        <div class="value"><?php echo $single_contact['vCity']; ?></div>
                                    </div>
                                </div>

                                <div class="building-group">
                                    <div class="building-detail">
                                        <div class="item">Country</div>                                            
                                        <div class="value"><?php echo $single_contact['country_nicename']; ?></div>
                                    </div>
                                </div>    

                                <div class="building-group">
                                    <div class="building-detail">
                                        <div class="item">Citizenship </div>
                                        <div class="value"><?php echo $single_contact['vCitizenship']; ?></div>
                                    </div>
                                </div>
                            </div> 
                        </div>  

                        <div class="row">
                            <div class="col-md-6">
                                <div class="building-group">
                                    <div class="building-detail">
                                        <div class="item">Id Proof</div>
                                        <div class="value">                                           
                                             <a class="example-image-link" href="<?php echo ID_PROOF_IMAGE_PATH . $single_contact['tUploadID']; ?>" data-lightbox="example-2"><img class="example-image" src="<?php echo ID_PROOF_IMAGE_THUMB_PATH . $single_contact['tUploadID']; ?>" alt="idproof" /></a> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="building-group">
                                    <div class="building-detail">
                                        <div class="item">Address Proof</div>
                                        <div class="value">
                                            <a class="example-image-link" href="<?php echo ADD_PROOF_IMAGE_PATH . $single_contact['tAddressProof']; ?>" data-lightbox="example-1"><img class="example-image" src="<?php echo ADD_PROOF_IMAGE_THUMB_PATH . $single_contact['tAddressProof']; ?>" alt="addressproof" /></a>                                            
                                        </div>
                                    </div>
                                </div>   
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>        
        <?php include('footer.php'); ?>
<script src="dist/js/lightbox-plus-jquery.min.js"></script>
       