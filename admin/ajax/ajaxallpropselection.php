<?php
session_start();
require '../config/config.php';
require '../model/model.php';


## Read value
$draw = $_POST['draw'];
$row = $_POST['start'];
$rowperpage = $_POST['length']; // Rows display per page
$columnIndex = 1; // Column index
$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
$searchValue = $_POST['search']['value']; // Search value



## Search 
$searchQuery = "";

$searchQuery .= "(property_id in (".$_SESSION['tempselection']."))";


## Total number of records without filtering
$sel = mysqli_query($conn,"select count(*) as allcount from tbl_property");
$records = mysqli_fetch_assoc($sel);

$totalRecords = $records['allcount'];

## Total number of records with filtering
$sel = mysqli_query($conn,"select count(*) as allcount from tbl_property WHERE  ".$searchQuery);
$records = mysqli_fetch_assoc($sel);
$totalRecordwithFilter = $records['allcount'];

## Fetch records
$empQuery = "select * from tbl_property WHERE  ".$searchQuery." order by ".$columnName." ".$columnSortOrder." limit ".$row.",".$rowperpage;

$empRecords = mysqli_query($conn, $empQuery);
$data = array();

while ($row = mysqli_fetch_assoc($empRecords)) {

    if($row['price'] != '' && $row['total_area'] != ''){
        $monthly_rent = number_format($row['price']/$row['total_area'],'2').' €/m²';
    }else{
        $monthly_rent = '';
    }
    
    $data[] = array(
            "DT_RowId" => 'row_'.$row['property_id'],
            "#" => '<input type="checkbox" name="tempselprop" class="tempselectionpropdata" id="tempdata-'.$row['property_id'].'" checked dataval="'.$row['property_id'].'"><input type="hidden" name="temppropdata[]" class="temppropdata-'.$row['property_id'].'" value="'.$row['property_id'].'">',
		        "updated_at"=>date("d/m/Y", strtotime($row['updated_at'])),
		        "reference"=>$row['reference'],
		        "property_title"=>$row['property_title'],
            "building_id"=>buildingName($row['building_id']),
            "property_type"=>propertyType($row['property_type']),
            "transaction_type"=>transactionType($row['transaction_type']),
            "price"=>$row['price'],
            "diffusion"=>date("d/m/Y", strtotime($row['updated_at'])),
            "num_rooms"=>$row['num_rooms'],
            "monthly_rent"=> $monthly_rent,
            "property_description" => transactionType($row['transaction_type']).'-'.propertyType($row['property_type']).' '.$row['num_rooms'].' rooms<br/>'.$row['property_title'],	
            "created_at" => date("d/m/Y", strtotime($row['created_at'])),
            "release_date"=> date("d/m/Y", strtotime($row['release_date'])),
            "floor" => $row['floor'],
            "photo_id" => '1',
            "total_area" => $row['total_area'].'/m²',
            "retrocession"=> $row['no_retrocession'],            
    	);
}

## Response
$response = array(
    "draw" => intval($draw),
    "iTotalRecords" => $totalRecords,
    "iTotalDisplayRecords" => $totalRecordwithFilter,
    "aaData" => $data
);

echo json_encode($response);
