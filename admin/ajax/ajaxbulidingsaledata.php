<?php
session_start();
require '../config/config.php';
require '../model/model.php';

## Read value
$draw = $_POST['draw'];
$row = $_POST['start'];
$rowperpage = $_POST['length']; // Rows display per page
$columnIndex = $_POST['order'][0]['column']; // Column index
$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
$searchValue = $_POST['search']['value']; // Search value

## Custom Field value
if($transectionType != ''){
    $transectionType = $_POST['transectionType'];  
}else{
    $transectionType = '1';
}


$proType = implode(',', $_POST['proType']);

if(in_array("1", $_POST['statePublication'])){
  $statePublication_draft = 1;
}
else{
  $statePublication_draft = ''; 
}
if(in_array("2", $_POST['statePublication'])){
  $statePublication_offmarket = 1;
}
if(in_array("3", $_POST['statePublication'])){
  $statePublication_public = 1;
}
if(in_array("4", $_POST['statePublication'])){
  $statePublication_archive = 1;
}

$reference = $_POST['reference'];
$country = $_POST['country'];
$nubrooms = implode(',', $_POST['nubRooms']);
$startdate = $_POST['startDate'];
$enddate =$_POST['endDate'];

if(in_array("mixeduse", $_POST['otherCriteria']))
{
  $mixeduse = 1;
}

if(in_array("furniture", $_POST['otherCriteria']))
{
  $furniture = 1;
}

if(in_array("interesting", $_POST['otherCriteria']))
{
  $interesting = 1;
}

if(in_array("parking", $_POST['otherCriteria']))
{
  $parking = true;
}


## Search 
$searchQuery = "";


$searchQuery .= "is_delete = 0";


if($statePublication_draft != ''){
    $searchQuery .= " OR is_draft ='".$statePublication_draft."'";   
}
if($statePublication_offmarket != ''){
    $searchQuery .= " OR is_offmarket ='".$statePublication_offmarket."'";   
}
if($statePublication_public != ''){
    $searchQuery .= " OR is_public ='".$statePublication_public."'";   
}
if($statePublication_archive != ''){
    $searchQuery .= " OR is_archive ='".$statePublication_archive."'";   
}
if($_POST['buildingid'] != ''){    
    $searchQuery .= " AND building_id = ".$_POST['buildingid']." AND user_id = ".$_SESSION['user_id']."";
}
if($transectionType != ''){
    $searchQuery .= " AND (transaction_type ='".$transectionType."' ) ";    
}
if($reference != ''){
    $searchQuery .= " AND (reference like '%".$reference."%' ) ";
}  
if($country != '' ){
    $searchQuery .= " AND (country ='".$country."' ) ";
}
if($nubrooms != ''){
    $searchQuery .= " AND (num_rooms in (".$nubrooms.")) ";
}
if($mixeduse != ''){
    $searchQuery .= " AND (mixed_use ='".$mixeduse."' ) ";
}
if($furniture != ''){
    $searchQuery .= " AND (furnished ='".$furniture."' ) ";
}
if($interesting != ''){
    $searchQuery .= " AND (interesting_view ='".$interesting."' ) ";
}
if($parking != ''){
    $searchQuery .= " AND (num_parking !='' ) ";
}
if($startdate != '' && $enddate != ''){
    $searchQuery .= " AND (release_date BETWEEN '".date("d/m/Y", strtotime($startdate))."' AND '".date("d/m/Y", strtotime($enddate))."' ) ";
}
if($proType != ''){
    $searchQuery .= " AND (property_type in (".$proType.")) ";
}

if($_POST['minArea'] != '' && $_POST['maxArea'] != ''){
    $searchQuery .= " AND (total_area >='".$_POST['minArea']."' AND total_area <='".$_POST['maxArea']."') ";
}



// if($searchValue != ''){
//  $searchQuery .= " and (emp_name like '%".$searchValue."%') ";
// }

## Total number of records without filtering
$sel = mysqli_query($conn,"select count(*) as allcount from tbl_property");
$records = mysqli_fetch_assoc($sel);

$totalRecords = $records['allcount'];

## Total number of records with filtering
$sel = mysqli_query($conn,"select count(*) as allcount from tbl_property WHERE  ".$searchQuery);
$records = mysqli_fetch_assoc($sel);
$totalRecordwithFilter = $records['allcount'];

## Fetch records
$empQuery = "select * from tbl_property WHERE  ".$searchQuery." order by ".$columnName." ".$columnSortOrder." limit ".$row.",".$rowperpage;

$empRecords = mysqli_query($conn, $empQuery);
$data = array();

while ($row = mysqli_fetch_assoc($empRecords)) {
   if($row['price'] != '' && $row['total_area'] != ''){
        $monthly_rent = number_format($row['price']/$row['total_area'],'2').' €/m²';
    }else{
        $monthly_rent = '';
    }
    $data[] = array(
            "DT_RowId" => 'row_'.$row['property_id'],
        "updated_at"=>date("d/m/Y", strtotime($row['updated_at'])),
        "reference"=>$row['reference'],
        "property_title"=>$row['property_title'],
            "building_id"=>buildingName($row['building_id']),
            "property_type"=>propertyType($row['property_type']),
            "transaction_type"=>transactionType($row['transaction_type']),
            "price"=>$row['price'],
            "diffusion"=>date("d/m/Y", strtotime($row['updated_at'])),
            "num_rooms"=>$row['num_rooms'],
            "monthly_rent"=> $monthly_rent,
            "property_description" => transactionType($row['transaction_type']).'-'.propertyType($row['property_type']).' '.$row['num_rooms'].' rooms<br/>'.$row['property_title'], 
            "created_at" => date("d/m/Y", strtotime($row['created_at'])),
            "release_date"=> date("d/m/Y", strtotime($row['release_date'])),
            "floor" => $row['floor'],
            "photo_id" => '1',
            "total_area" => $row['total_area'].'/m²',
            "retrocession"=> $row['no_retrocession'],
            "action" => '<a href="single_property.php?propertyid='.$row['property_id'].'""><button class="btn btn-primary btn-icon-anim btn-square"><i class="fa fa-desktop"></i></button></a>
                                         <a href="#"><button class="btn btn-default btn-icon-anim btn-square"><i class="fa fa-print"></i></button></a>'
      );
}

## Response
$response = array(
    "draw" => intval($draw),
    "iTotalRecords" => $totalRecords,
    "iTotalDisplayRecords" => $totalRecordwithFilter,
    "aaData" => $data
);

echo json_encode($response);
