<?php
session_start();
require '../config/config.php';
require '../config/resize-class.php';
require '../model/model.php';
global $conn;
if (isset($_SESSION['first_name']) && isset($_SESSION['last_name']) && $_SESSION['add_contact'] == '1') {
    $first_name = $_SESSION['first_name'];
    $last_name = $_SESSION['last_name'];
} else {
    echo "<script>window.location='index.php'</script>";
    exit;
}

if (!empty($_FILES)) {
    $msgdata = '';
    $fileName = '';
    $fileName = $_FILES['file']['name'];
    $fileTmpLoc = $_FILES['file']['tmp_name'];
    $newfilename = '';
    $check = getimagesize($_FILES["file"]["tmp_name"]);
    if ($check !== false) {
        if (!empty($_FILES['file']) && is_array($_FILES['file']) && $_FILES['file']['error'] == 0) {
            $ext = pathinfo($fileName, PATHINFO_EXTENSION);
            $ext = strtolower($ext);

            $filepath = MEDIA_IMAGE_PATH.$fileName;            
            
            if (!$fileTmpLoc) { // if file not chosen
                $message = "<div class='alert alert-error'><button type='button' class='close' data-dismiss='alert'>&times;</button><strong>Error!</strong> ERROR: Please browse for a file before clicking the upload button.<div class='alert alert-success' style='display:none'>
                                    <button type='button' class='close' data-dismiss='alert'>&times;</button>
                                    <strong>Error!</strong> ERROR: Please browse for a file before clicking the upload button.
                            </div></div>";
                exit;
            } else if (!preg_match("/.(gif|jpg|png|jpeg|JPG|PNG|GIF|JPEG)$/i", $fileName)) {
                // This condition is only if you wish to allow uploading of specific file types    
                $message = "<div class='alert alert-error'><button type='button' class='close' data-dismiss='alert'>&times;</button><strong>Error!</strong> ERROR:Your image was not .gif, .jpg, .png !<div class='alert alert-success' style='display:none'>
                                    <button type='button' class='close' data-dismiss='alert'>&times;</button>
                                    <strong>Error!</strong> ERROR: Your image was not .gif, .jpg, .png !
                            </div></div>";
                unlink($fileTmpLoc);
                exit;
            }

            $temp = explode(".", $_FILES["file"]["name"]);           
            $newfilename = strtoupper($temp[0]) . "-" . rand(1, 9999999) . '.' . $ext;
           
            move_uploaded_file($fileTmpLoc, MEDIA_IMAGE_PATH . $newfilename);
            chmod(MEDIA_IMAGE_PATH. $newfilename, 0777);

            $resizeObj = new resize(MEDIA_IMAGE_PATH . $newfilename);
            $resizeObj->resizeImage(100, 100, 'auto');
            $resizeObj->saveImage(MEDIA_IMAGE_THUMB_PATH . $newfilename, 100);

            chmod(MEDIA_IMAGE_THUMB_PATH . $newfilename, 0777);

            $filepath = MEDIA_IMAGE_THUMB_PATH . $newfilename;
            if (!file_exists($filepath)) {
                $message = "<div class='alert alert-error'><button type='button' class='close' data-dismiss='alert'>&times;</button><strong>Error!</strong> ERROR: File not uploaded! <div class='alert alert-success' style='display:none'>
                                    <button type='button' class='close' data-dismiss='alert'>&times;</button>
                                    <strong>Error!</strong> ERROR: File not uploaded!
                            </div></div>";
                unlink($fileTmpLoc);
                exit;
            }
        }
    } else {
        $msgdata = '<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="zmdi zmdi-block pr-15 pull-left"></i><p class="pull-left">Opps! Somthing went wrong.Please upload image file only.</p>
						<div class="clearfix"></div>
					</div>';
        unlink($fileTmpLoc);
    }


    $last_inserted_id = '';
    //if ($newfilename != '' && $newfilenameadd != '') {
    echo "<pre>";
    print_r($newfilename);
    exit();
    $last_inserted_id = addContact($newfilename);

    if ($last_inserted_id > 0 && $last_inserted_id != '') {
        $message = '<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="zmdi zmdi-check pr-15 pull-left"></i><p class="pull-left">Success! Contact has been added successfully.</p> 
						<div class="clearfix"></div>
					</div>';
    } else {
        $message = '<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="zmdi zmdi-block pr-15 pull-left"></i><p class="pull-left">Opps! Somthing went wrong.Please try again.</p>
						<div class="clearfix"></div>
					</div>';
    }
    //}
}