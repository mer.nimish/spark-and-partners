<?php

require '../config/config.php';
require '../model/model.php';
global $conn;
$response = array();

if (isset($_POST['action']) && $_POST['action'] == 'agent_list') {
    $query = "SELECT tu.*,usertp.user_type_id,usertp.user_type from tbl_user as tu LEFT JOIN tbl_user_type as usertp ON tu.user_type_id = usertp.user_type_id WHERE tu.is_delete = '0' AND usertp.user_type LIKE 'Agent' ";    
    $res = mysqli_query($conn, $query);
    $temp = array();
    if (mysqli_num_rows($res) > 0) {
        $i = 0;        
        while ($row = mysqli_fetch_array($res)) {
            $temp[$i]['user_id'] = $row['user_id'];
            $temp[$i]['first_name'] = $row['first_name'];
            $temp[$i]['last_name'] = $row['last_name'];
            $temp[$i]['email'] = $row['email'];
            $temp[$i]['user_type'] = $row['user_type'];
            $i++;
        }                
    }       
    echo json_encode($temp);
}else{
    echo "<script>window.location='index.php'</script>";
    exit;
}
?>
