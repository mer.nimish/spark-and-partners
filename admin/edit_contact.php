<?php
session_start();
require 'config/config.php';
require 'config/resize-class.php';
require 'model/model.php';
global $conn;

if (isset($_SESSION['first_name']) && isset($_SESSION['last_name']) && $_SESSION['add_contact'] == '1') {
    $first_name = $_SESSION['first_name'];
    $last_name = $_SESSION['last_name'];
} else {
    echo "<script>window.location='index.php'</script>";
    exit;
}



if (isset($_POST['editcontactbtn']) && $_POST['editcontactbtn'] != '') {

    $contactid = $_POST['contactid'];

    $fileName = '';
    $fileName = $_FILES['tUploadID']['name'];
    $fileTmpLoc = $_FILES['tUploadID']['tmp_name'];
    $newfilename = '';
    $check = getimagesize($_FILES["tUploadID"]["tmp_name"]);
    if ($check !== false && $_FILES['tUploadID']['error'] != 4) {
        if (!empty($_FILES['tUploadID']) && is_array($_FILES['tUploadID']) && $_FILES['tUploadID']['error'] == 0) {
            $ext = pathinfo($fileName, PATHINFO_EXTENSION);
            $ext = strtolower($ext);

            $filepath = ID_PROOF_IMAGE_PATH . $fileName;

            if (!$fileTmpLoc) { // if file not chosen
                $message = "<div class='alert alert-error'><button type='button' class='close' data-dismiss='alert'>&times;</button><strong>Error!</strong> ERROR: Please browse for a file before clicking the upload button.<div class='alert alert-success' style='display:none'>
                                    <button type='button' class='close' data-dismiss='alert'>&times;</button>
                                    <strong>Error!</strong> ERROR: Please browse for a file before clicking the upload button.
                            </div></div>";
                exit;
            } else if (!preg_match("/.(gif|jpg|png|jpeg|JPG|PNG|GIF|JPEG)$/i", $fileName)) {
                // This condition is only if you wish to allow uploading of specific file types    
                $message = "<div class='alert alert-error'><button type='button' class='close' data-dismiss='alert'>&times;</button><strong>Error!</strong> ERROR:Your image was not .gif, .jpg, .png !<div class='alert alert-success' style='display:none'>
                                    <button type='button' class='close' data-dismiss='alert'>&times;</button>
                                    <strong>Error!</strong> ERROR: Your image was not .gif, .jpg, .png !
                            </div></div>";
                unlink($fileTmpLoc);
                exit;
            }

            $temp = explode(".", $_FILES["tUploadID"]["name"]);
            $newfilename = rand(1, 99999) . time() . '.' . $ext;
            move_uploaded_file($fileTmpLoc, ID_PROOF_IMAGE_PATH . $newfilename);
            chmod(ID_PROOF_IMAGE_PATH . $newfilename, 0777);

            $resizeObj = new resize(ID_PROOF_IMAGE_PATH . $newfilename);
            $resizeObj->resizeImage(100, 100, 'auto');
            $resizeObj->saveImage(ID_PROOF_IMAGE_THUMB_PATH . $newfilename, 100);

            chmod(ID_PROOF_IMAGE_THUMB_PATH . $newfilename, 0777);

            $filepath = ID_PROOF_IMAGE_THUMB_PATH . $newfilename;
            if (!file_exists($filepath)) {
                $message = "<div class='alert alert-error'><button type='button' class='close' data-dismiss='alert'>&times;</button><strong>Error!</strong> ERROR: File not uploaded! <div class='alert alert-success' style='display:none'>
                                    <button type='button' class='close' data-dismiss='alert'>&times;</button>
                                    <strong>Error!</strong> ERROR: File not uploaded!
                            </div></div>";
                unlink($fileTmpLoc);
                exit;
            }
        }
    } 
//    else {
//        $msgdata = '<div class="alert alert-danger alert-dismissable">
//						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
//						<i class="zmdi zmdi-block pr-15 pull-left"></i><p class="pull-left">Opps! Somthing went wrong.Please upload image file only.</p>
//						<div class="clearfix"></div>
//					</div>';
//        unlink($fileTmpLoc);
//    }


    $fileNameadd = '';
    $fileNameadd = $_FILES['tAddressProof']['name'];
    $fileTmpLocadd = $_FILES['tAddressProof']['tmp_name'];
    $newfilenameadd = '';
    $checkdata = getimagesize($_FILES["tAddressProof"]["tmp_name"]);
    if ($checkdata !== false && $_FILES['tAddressProof']['error'] != 4) {
        
        if (!empty($_FILES['tAddressProof']) && is_array($_FILES['tAddressProof']) && $_FILES['tAddressProof']['error'] == 0) {
            $exte = pathinfo($fileNameadd, PATHINFO_EXTENSION);
            $exte = strtolower($exte);

            $filepathadd = ADD_PROOF_IMAGE_PATH . $fileNameadd;

            if (!$fileTmpLocadd) { // if file not chosen
                $message = "<div class='alert alert-error'><button type='button' class='close' data-dismiss='alert'>&times;</button><strong>Error!</strong> ERROR: Please browse for a file before clicking the upload button.<div class='alert alert-success' style='display:none'>
                                    <button type='button' class='close' data-dismiss='alert'>&times;</button>
                                    <strong>Error!</strong> ERROR: Please browse for a file before clicking the upload button.
                            </div></div>";
                exit;
            } else if (!preg_match("/.(gif|jpg|png|jpeg|JPG|PNG|GIF|JPEG)$/i", $fileNameadd)) {
                // This condition is only if you wish to allow uploading of specific file types    
                $message = "<div class='alert alert-error'><button type='button' class='close' data-dismiss='alert'>&times;</button><strong>Error!</strong> ERROR:Your image was not .gif, .jpg, .png !<div class='alert alert-success' style='display:none'>
                                    <button type='button' class='close' data-dismiss='alert'>&times;</button>
                                    <strong>Error!</strong> ERROR: Your image was not .gif, .jpg, .png !
                            </div></div>";
                unlink($fileTmpLocadd);
                exit;
            }

            $tempdata = explode(".", $_FILES["tAddressProof"]["name"]);
            $newfilenameadd = rand(1, 99999) . time() . '.' . $exte;

            move_uploaded_file($fileTmpLocadd, ADD_PROOF_IMAGE_PATH . $newfilenameadd);
            chmod(ADD_PROOF_IMAGE_PATH . $newfilenameadd, 0777);

            $resizeObj = new resize(ADD_PROOF_IMAGE_PATH . $newfilenameadd);
            $resizeObj->resizeImage(100, 100, 'auto');
            $resizeObj->saveImage(ADD_PROOF_IMAGE_THUMB_PATH . $newfilenameadd, 100);
            chmod(ADD_PROOF_IMAGE_THUMB_PATH . $newfilenameadd, 0777);
            $filepathadd = ADD_PROOF_IMAGE_THUMB_PATH . $newfilenameadd;


            if (!file_exists($filepathadd)) {
                $message = "<div class='alert alert-error'><button type='button' class='close' data-dismiss='alert'>&times;</button><strong>Error!</strong> ERROR: File not uploaded! <div class='alert alert-success' style='display:none'>
                                    <button type='button' class='close' data-dismiss='alert'>&times;</button>
                                    <strong>Error!</strong> ERROR: File not uploaded!
                            </div></div>";
                unlink($fileTmpLocadd);
                exit;
            }
        }
    } 
//    else {
//        $msgdata = '<div class="alert alert-danger alert-dismissable">
//						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
//						<i class="zmdi zmdi-block pr-15 pull-left"></i><p class="pull-left">Opps! Somthing went wrong.Please upload image file only.</p>
//						<div class="clearfix"></div>
//					</div>';
//        unlink($fileTmpLocadd);
//    }
  
    //if ($newfilename != '' && $newfilenameadd != '') {

        $updatedata = editContact($newfilename, $newfilenameadd, $contactid);

        if ($updatedata) {
            $message = '<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="zmdi zmdi-check pr-15 pull-left"></i><p class="pull-left">Success! Contact has been updated successfully.</p> 
						<div class="clearfix"></div>
					</div>';
        } else {
            $message = '<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="zmdi zmdi-block pr-15 pull-left"></i><p class="pull-left">Opps! Somthing went wrong.Please try again.</p>
						<div class="clearfix"></div>
					</div>';
        }
    //}
}

$allcountry = getAllCountry();
include('header.php');
include('left_sidebar.php');
?>

<div class="page-wrapper">
    <div class="container-fluid pt-25">	
        <div class="col-md-12">
            <?php echo $msgdata; ?>
            <?php echo $message; ?>
        </div>

        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark">Edit Contact</h5>
            </div>
        </div>
        <!-- /Title -->

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark">Edit Contact Form</h6>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-md-8">                                   
                                    <div class="form-wrap">
                                        <form data-toggle="validator" name="addcontactform" id="addcontactform" method="post" role="form" enctype="multipart/form-data">
                                            <?php
                                            if (isset($_REQUEST['cid']) && $_REQUEST['cid'] != '') {
                                                $contactdata = getContact($_REQUEST['cid']);
                                                $userlist = getuserlistfromtype($contactdata['vContactType']);

                                                if (empty($contactdata)) {
                                                    echo "<script>window.location='index.php'</script>";
                                                    exit;
                                                }
                                                ?>
                                                <input type="hidden" name="contactid" value="<?php echo $_REQUEST['cid']; ?>">
                                            <?php
                                            } else {
                                                echo "<script>window.location='index.php'</script>";
                                                exit;
                                            }
                                            ?>

                                            <div class="form-group">
                                                <label class="control-label mb-10">Contact Type</label>                                                
                                                <select class="selectpicker" data-style="form-control btn-default btn-outline" name="vContactType" id="vContactType" required>
                                                    <option value="" disabled selected>Select Contact Type</option>   
                                                    <option value="Agent" <?php echo (isset($contactdata['vContactType']) && $contactdata['vContactType'] == 'Agent') ? 'selected' : ''; ?>>Agent</option>   
                                                    <option value="Prospect" <?php echo (isset($contactdata['vContactType']) && $contactdata['vContactType'] == 'Prospect') ? 'selected' : ''; ?>>Prospect</option>                                                                                                                           
                                                    <option value="Owner" <?php echo (isset($contactdata['vContactType']) && $contactdata['vContactType'] == 'Owner') ? 'selected' : ''; ?>>Owner</option>                                                                                                                           
                                                    <option value="Agency" <?php echo (isset($contactdata['vContactType']) && $contactdata['vContactType'] == 'Agency') ? 'selected' : ''; ?>>Agency</option>                                                                                                                           
                                                    <option value="Syndic" <?php echo (isset($contactdata['vContactType']) && $contactdata['vContactType'] == 'Syndic') ? 'selected' : ''; ?>>Syndic</option>                                                                                                                           
                                                    <option value="Construction Company" <?php echo (isset($contactdata['vContactType']) && $contactdata['vContactType'] == 'Construction Company') ? 'selected' : ''; ?>>Construction Company</option>                                                                                                                                                                                                                                                                                                      
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label mb-10">User List</label>
                                                <select class="form-control select2" name="iUserID" id="userlist" required>
                                                    <option value="" disabled selected>Select User</option>
                                                    <?php
                                                    $selected = '';
                                                    if (mysqli_num_rows($userlist) > 0) {
                                                        while ($row = mysqli_fetch_array($userlist)) {
                                                            if ($contactdata['user_id'] == $row['user_id']) {
                                                                $selected = 'selected';
                                                            }
                                                            echo "<option value=" . $row['user_id'] . " $selected>" . $row['first_name'] . " " . $row['last_name'] . "</option>";
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label mb-10">Salutation</label>
                                                <select class="selectpicker" data-style="form-control btn-default btn-outline" name="vSalutation">
                                                    <option value="Mr" <?php echo (isset($contactdata['vSalutation']) && $contactdata['vSalutation'] == 'Mr') ? 'selected' : ''; ?>>Mr</option>   
                                                    <option value="Mrs" <?php echo (isset($contactdata['vSalutation']) && $contactdata['vSalutation'] == 'Mrs') ? 'selected' : ''; ?>>Mrs</option>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label mb-10">First Name</label>
                                                <input type="text" class="form-control" name="vFirstName" id="vFirstName" placeholder="First Name" value="<?php echo $contactdata['vFirstName']; ?>" required>
                                            </div>                                           
                                            <div class="form-group">
                                                <label class="control-label mb-10">Last Name</label>
                                                <input type="text" class="form-control" name="vLastName" id="vLastName" placeholder="Last Name" value="<?php echo $contactdata['vLastName']; ?>"  required>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label mb-10">Email</label>
                                                <input type="email" class="form-control" id="vEmail" name="vEmail" placeholder="Email" value="<?php echo $contactdata['vEmail']; ?>" data-error="Please enter valid email address" required>
                                                <div class="help-block with-errors"></div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label mb-10">Phone</label>
                                                <input type="text" placeholder="9999999999" data-mask="9999999999" class="form-control" name="vPhone" value="<?php echo $contactdata['vPhone']; ?>">                                                 
                                            </div>                                         

                                            <div class="form-group">
                                                <label class="control-label mb-10 text-left">Date of Birth</label>
                                                <div class='input-group date' id='datetimepickerfordob'>
                                                    <input type='text' class="form-control" name="dDateofbirth" value="<?php echo date("d/m/Y", strtotime($contactdata['dDateofbirth'])); ?>"  />
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label mb-10">Citizenship</label>
                                                <input type="text" class="form-control" name="vCitizenship" id="vCitizenship" value="<?php echo $contactdata['vCitizenship']; ?>" required>
                                            </div> 
                                            <div class="form-group">
                                                <label class="control-label mb-10 text-left">Address</label>
                                                <textarea class="form-control" rows="3" name="vAddress" id="vAddress"><?php echo $contactdata['vAddress']; ?></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label mb-10">City</label>
                                                <input type="text" class="form-control" name="vCity" id="vCity" placeholder="City Name" value="<?php echo $contactdata['vCity']; ?>" required>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label mb-10">Select Country</label>
                                                <select id="country" name="country_id" class=" form-control form-control" data-container="body" data-live-search="true">
                                                    <?php
                                                    if (isset($allcountry) && $allcountry != 0) {
                                                        while ($country = mysqli_fetch_assoc($allcountry)) {
                                                            if ($country['country_id'] == $contactdata['country_id']) {
                                                                ?>
                                                                <option value="<?= $country['country_id'] ?>"  data-code="<?= $country['iso_code'] ?>" selected><?= $country['country_nicename'] ?></option>
                                                            <?php } else {
                                                                ?>
                                                                <option value="<?= $country['country_id'] ?>"  data-code="<?= $country['iso_code'] ?>"><?= $country['country_nicename'] ?></option>
                                                                <?php
                                                            }
                                                        }
                                                    }
                                                    ?> 
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label mb-10">Language Spoken</label>
                                                <div class="checkbox checkbox-primary checkbox-circle">
                                                    <input id="englangcheckbox" type="checkbox" name="tLanguageSpoken[]" value="English" <?php
                                                    if (in_array("English", $contactdata['vLangaugeSpoken'])) {
                                                        echo "checked";
                                                    }
                                                    ?>>
                                                    <label for="englangcheckbox"> English </label>                                                    
                                                </div>
                                                <div class="checkbox checkbox-primary checkbox-circle">
                                                    <input id="hinlangcheckbox" type="checkbox" name="tLanguageSpoken[]" value="Hindi" <?php
                                                    if (in_array("Hindi", $contactdata['vLangaugeSpoken'])) {
                                                        echo "checked";
                                                    }
                                                    ?>>
                                                    <label for="hinlangcheckbox"> Hindi </label>                                                   
                                                </div>
                                                <div class="checkbox checkbox-primary checkbox-circle">
                                                    <input id="frlangcheckbox" type="checkbox" name="tLanguageSpoken[]" value="French" <?php
                                                    if (in_array("French", $contactdata['vLangaugeSpoken'])) {
                                                        echo "checked";
                                                    }
                                                    ?>>
                                                    <label for="frlangcheckbox"> French </label>                                                   
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label mb-10">Upload ID proof image</label>
                                                <div class="panel-wrapper collapse in">
                                                    <div class="panel-body">                                                       
                                                        <div class="mt-0">
                                                            <input type="file" id="tUploadID" class="dropify" name="tUploadID" data-max-file-size="2M" data-default-file="<?php if(isset($contactdata['tUploadID']) &&  $contactdata['tUploadID'] != ''){echo ID_PROOF_IMAGE_PATH . $contactdata['tUploadID'];} ?>" />
                                                        </div>	
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label mb-10">Upload Address proof image</label>
                                                <div class="panel-wrapper collapse in">
                                                    <div class="panel-body">                                                       
                                                        <div class="mt-0">
                                                            <input type="file" id="tAddressProof" class="dropify" name="tAddressProof" data-max-file-size="2M" data-default-file="<?php if(isset($contactdata['tAddressProof']) &&  $contactdata['tAddressProof'] != ''){echo ADD_PROOF_IMAGE_PATH . $contactdata['tAddressProof'];} ?>"/>
                                                        </div>	
                                                    </div>
                                                </div>
                                            </div>
                                                
                                                <div class="form-group">    
                                                    <label class="control-label mb-10">User Status</label>
                                                    <?php 
                                                    $checkeddata = '';
                                                    if($contactdata['eActive'] == 0){ 
                                                        $checkeddata = 'checked';
                                                    }
                                                    ?>
                                                    <input type="checkbox" <?=$checkeddata?> class="js-switch js-switch-1" data-color="#8BC34A" name="eActive"/>
                                                </div>

                                            <div class="form-group mb-0">
                                                <input type="submit" value="Submit" name="editcontactbtn" id="editcontactbtn" class="btn btn-success btn-anim">
                                            </div>                                            
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <?php include('footer.php'); ?>
        <script type="text/javascript">
            /* Switchery Init*/
            var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
            $('.js-switch-1').each(function () {
                new Switchery($(this)[0], $(this).data());
            });
            $(document).on('change', '#vContactType', function () {
                var contactType = this.value;
                var data = {
                    "contype": contactType
                };
                if (contactType == "Agent") {
                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: "getusertypelist.php",
                        data: data,
                        success: function (data) {
                            if (data != '') {
                                $("#userlist").html(data);
                            }
                        }
                    });
                } else {
                    $("#userlist").html('<option value="" disabled selected>Select User</option>');
                    return false;
                }
            });
        </script>