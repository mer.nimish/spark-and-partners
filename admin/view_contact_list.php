<?php
session_start();
require 'config/config.php';
require 'config/constant.php';
require 'model/model.php';
global $conn;


if (isset($_SESSION['first_name']) && isset($_SESSION['last_name']) && $_SESSION['add_contact'] == '1') {
    $first_name = $_SESSION['first_name'];
    $last_name = $_SESSION['last_name'];
} else {
    echo "<script>window.location='index.php'</script>";
    exit;
}
$viewcontacts = viewallcontacts();


include('header.php');
include('left_sidebar.php');
?>

<div class="page-wrapper">
    <div class="container-fluid pt-25"> 
        <div  class="pills-struct">            
            <div class="tab-content" id="myTabContent_6">
                <div id="contact_list" class="tab-pane fade active in" role="tabpanel">
                    <div class="row">
                        <div class="" style="margin-top:20px;margin-bottom:5px; margin-left:0% "></div>
                        <div class="col-sm-12">
                            <div class="panel panel-default card-view">
                                <div class="panel-heading">
                                    <div class="pull-left">
                                        <h6 class="panel-title txt-dark">List of Contacts</h6>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body">
                                        <div class="table-wrap">
                                            <div class="table-responsive">
                                                <table id="contact_list_tbl" class="table table-hover display  pb-30" >
                                                    <thead>
                                                        <tr>
                                                            <th width="10%">Name</th>
                                                            <th width="10%">Type</th>                                  
                                                            <th width="10%">Phone</th>
                                                            <th width="10%">Email</th>
                                                            <th width="10%">Number of Mandate</th>
                                                            <th width="10%">Deals</th>
                                                            <th width="10%">Commission Genrated</th>
                                                            <th width="10%">Agents Recruited</th>
                                                            <th width="10%">User Status</th>
                                                            <th width="10%">Action</th>
                                                        </tr>
                                                    </thead>                                                     
                                                    <tbody>
                                                        <?php
                                                        if (isset($viewcontacts) && $viewcontacts != 0) {
                                                            foreach ($viewcontacts as $val) {
                                                                ?>
                                                                <tr>
                                                                    <td><?php echo $val['vFirstName'] . ' ' . $val['vLastName']; ?></td>
                                                                    <td><?php echo $val['vContactType']; ?></td>   
                                                                    <td><?php echo $val['vPhone']; ?></td>
                                                                    <td><?php echo $val['vEmail']; ?></td>
                                                                    <td>
                                                                    <?php
                                                                    if($val['numberofmandate'] != '' && $val['vContactType'] == 'Agent'){
                                                                        echo $val['numberofmandate'];
                                                                    }else{
                                                                        echo "-";
                                                                    } 
                                                                    ?>
                                                                    </td>
                                                                    <td>
                                                                    <?php
                                                                    if($val['numberofdeals'] != ''  && $val['vContactType'] == 'Agent'){
                                                                        echo $val['numberofdeals'];
                                                                    }else{
                                                                        echo "-";
                                                                    } 
                                                                    ?>
                                                                   </td>
                                                                    <td>
                                                                    <?php 
                                                                    if($val['commission_amount'] != ''  && $val['vContactType'] == 'Agent'){
                                                                        echo $val['commission_amount'];
                                                                    }else{
                                                                        echo "-";
                                                                    }    
                                                                    ?>
                                                                    </td>
                                                                    <td>
                                                                    <?php 
                                                                    if($val['userrefer'] != ''  && $val['vContactType'] == 'Agent'){
                                                                        echo $val['userrefer'];
                                                                    }else{
                                                                        echo "-";
                                                                    }    
                                                                    ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php
                                                                        if ($val['eActive'] == '0') { ?>
                                                                            <span class="label label-success">Active</span>
                                                                        <?php } else { ?>
                                                                            <span class="label label-danger">Inactive</span>
                                                                        <?php }
                                                                        ?>
                                                                    </td>
                                                                    <td>                                                                        
                                                                        <a href="contact.php?ct_id=<?php echo $val['iContactID']; ?>"><button class="btn btn-primary btn-icon-anim btn-square"><i class="fa fa-desktop"></i></button>
                                                                        </a>
                                                                        <a href="edit_contact.php?cid=<?php echo $val['iContactID']; ?>" class="mr-25" data-toggle="tooltip" data-original-title="Edit"><button class="btn btn-default btn-icon-anim btn-square"> <i class="fa fa-pencil text-inverse m-r-10"></i></button> </a>  
                                                                    </td>

                                                                </tr>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </tbody>                                                     
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                

            </div>
        </div>        
        <script>
            // Main reference for data-table           
            $(document).ready(function () {
                var table = $('#contact_list_tbl').DataTable( );
                var $dt_head = $('#contact_list_tbl').find('thead').find('td');

                $dt_head.each(function () {
                    var title = $dt_head.eq($(this).index()).text();

                    if (title != 'Action') {
                        $(this).html($('<input/>', {'type': 'text', 'placeholder': 'Search ' + title}).addClass('form-control input-xs table-column-search width-full p-0'));
                    }
                });

                // Apply column filters at footer
                $("#contact_list_tbl  thead input").on('keyup change', function () {
                    table
                            .column($(this).parent().index() + ':visible')
                            .search(this.value)
                            .draw();
                });
            });

        </script>

        <style type="text/css">
            .dataTables_scroll{position:relative}
            .dataTables_scrollHead{margin-bottom:40px;}
            .dataTables_scrollFoot{position:absolute; top:38px}
        </style>
<?php include('footer.php'); ?>