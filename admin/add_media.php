<?php
session_start();
require 'config/config.php';
require 'config/resize-class.php';
require 'model/model.php';
global $conn;
if (isset($_SESSION['first_name']) && isset($_SESSION['last_name']) && $_SESSION['add_contact'] == '1') {
    $first_name = $_SESSION['first_name'];
    $last_name = $_SESSION['last_name'];
} else {
    echo "<script>window.location='index.php'</script>";
    exit;
}

if (!empty($_FILES)) {
    $msgdata = '';
    $fileName = '';
    $fileName = $_FILES['file']['name'];
    $fileTmpLoc = $_FILES['file']['tmp_name'];
    $newfilename = '';
    $check = getimagesize($_FILES["file"]["tmp_name"]);
    if ($check !== false) {
        if (!empty($_FILES['file']) && is_array($_FILES['file']) && $_FILES['file']['error'] == 0) {
            $ext = pathinfo($fileName, PATHINFO_EXTENSION);
            $ext = strtolower($ext);

            $filepath = MEDIA_IMAGE_PATH . $fileName;


            if (!$fileTmpLoc) { // if file not chosen
                $message = "<div class='alert alert-error'><button type='button' class='close' data-dismiss='alert'>&times;</button><strong>Error!</strong> ERROR: Please browse for a file before clicking the upload button.<div class='alert alert-success' style='display:none'>
                                    <button type='button' class='close' data-dismiss='alert'>&times;</button>
                                    <strong>Error!</strong> ERROR: Please browse for a file before clicking the upload button.
                            </div></div>";
                exit;
            } else if (!preg_match("/.(gif|jpg|png|jpeg|JPG|PNG|GIF|JPEG)$/i", $fileName)) {
                // This condition is only if you wish to allow uploading of specific file types    
                $message = "<div class='alert alert-error'><button type='button' class='close' data-dismiss='alert'>&times;</button><strong>Error!</strong> ERROR:Your image was not .gif, .jpg, .png !<div class='alert alert-success' style='display:none'>
                                    <button type='button' class='close' data-dismiss='alert'>&times;</button>
                                    <strong>Error!</strong> ERROR: Your image was not .gif, .jpg, .png !
                            </div></div>";
                unlink($fileTmpLoc);
                exit;
            }

            $temp = explode(".", $_FILES["tUploadID"]["name"]);
            $newfilename = strtoupper($fileName) . "-" . rand(1, 99999) . '.' . $ext;

            move_uploaded_file($fileTmpLoc, MEDIA_IMAGE_PATH . $newfilename);
            chmod(MEDIA_IMAGE_PATH . $newfilename, 0777);

            $resizeObj = new resize(MEDIA_IMAGE_PATH . $newfilename);
            $resizeObj->resizeImage(100, 100, 'auto');
            $resizeObj->saveImage(MEDIA_IMAGE_THUMB_PATH . $newfilename, 100);

            chmod(MEDIA_IMAGE_THUMB_PATH . $newfilename, 0777);

            $filepath = MEDIA_IMAGE_THUMB_PATH . $newfilename;
            if (!file_exists($filepath)) {
                $message = "<div class='alert alert-error'><button type='button' class='close' data-dismiss='alert'>&times;</button><strong>Error!</strong> ERROR: File not uploaded! <div class='alert alert-success' style='display:none'>
                                    <button type='button' class='close' data-dismiss='alert'>&times;</button>
                                    <strong>Error!</strong> ERROR: File not uploaded!
                            </div></div>";
                unlink($fileTmpLoc);
                exit;
            }
        }
    } else {
        $msgdata = '<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="zmdi zmdi-block pr-15 pull-left"></i><p class="pull-left">Opps! Somthing went wrong.Please upload image file only.</p>
						<div class="clearfix"></div>
					</div>';
        unlink($fileTmpLoc);
    }


    $last_inserted_id = '';
    //if ($newfilename != '' && $newfilenameadd != '') {
    echo "<pre>";
    print_r($newfilename);
    exit();
    $last_inserted_id = addContact($newfilename);

    if ($last_inserted_id > 0 && $last_inserted_id != '') {
        $message = '<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="zmdi zmdi-check pr-15 pull-left"></i><p class="pull-left">Success! Contact has been added successfully.</p> 
						<div class="clearfix"></div>
					</div>';
    } else {
        $message = '<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="zmdi zmdi-block pr-15 pull-left"></i><p class="pull-left">Opps! Somthing went wrong.Please try again.</p>
						<div class="clearfix"></div>
					</div>';
    }
    //}
}

if(isset($_REQUEST['path']) && $_REQUEST['path'] != ''){
    if($_REQUEST['path'] == "Test"){
        $activeclass = 'active';
    }
}else{
    
}

$allcountry = getAllCountry();
include('header.php');
include('left_sidebar.php');


?>

<div class="page-wrapper">
    <div class="container-fluid pt-25">	
        <div class="col-md-12">
            <?php echo $msgdata; ?>
            <?php echo $message; ?>
        </div>

        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark mypictures">My Pictures</h5>
            </div>
            <div class="button-list uploadmediaimage" id="uploadmediaimage">
                <button class="btn btn-primary btn-icon left-icon"> 
                    <i class="fa fa-image"></i> 
                    <span><a href="#imguploadmodal" data-toggle="modal" class="upload-image" data-backdrop="static" data-keyboard="false">New Images</a></span>
                </button>
                <button class="btn btn-default btn-icon left-icon"> 
                    <i class="fa fa-folder"></i> 
                    <span><a href="#foldernameModal" data-toggle="modal" class="folder-name" data-backdrop="static" data-keyboard="false">New Folder</a></span>
                </button>                              
            </div>
        </div>
        <!-- /Title -->

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark"><i class="fa fa-folder-open"></i> <span>ROOT</span></h6>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-9 col-md-9 col-sm-6 col-xs-12">
                                    <div class="filterdata">
                                        <div class="col-lg-7 col-md-6 col-sm-12 search-form">
                                            <form id="search_form" role="search" class="top-nav-search pull-left">                                               
                                                <input type="text" name="example-input1-group2" class="form-control imagesearch" placeholder="Filter">                                                
                                            </form>
                                        </div>
                                        <div class="col-lg-5 col-md-6 col-sm-12 sorting-img">
                                            <ul role="tablist" class="nav nav-pills" id="myTabs_6">
                                                <li class="active" role="presentation">
                                                    <span>Sort by</span>
                                                </li>
                                                <li role="presentation" class="">
                                                    <a aria-expanded="true"  data-toggle="tab" role="tab" id="home_tab_6" href="#home_6">Name</a>
                                                </li>
                                                <li role="presentation" class="">
                                                    <a aria-expanded="true"  data-toggle="tab" role="tab" id="home_tab_6" href="#home_6">Date</a>
                                                </li>                                                
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="imagelist">
                                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6">
                                            <div class="panel panel-default card-view pa-0">
                                                <div class="panel-wrapper collapse in">
                                                    <div class="panel-body pa-0">
                                                        <article class="col-item">
                                                            <div class="photo">
                                                                <div class="options">
                                                                    <a href="#" class="font-18 txt-grey mr-10 pull-left"><i class="zmdi zmdi-edit"></i></a>
                                                                    <a href="javascript:void(0);" class="font-18 txt-grey pull-left sa-warning"><i class="zmdi zmdi-close"></i></a>
                                                                </div>

                                                                <a href="javascript:void(0);"> 
                                                                    <img src="../img/chair.jpg" class="img-responsive mediaimg"  alt="Product Image" /> 
                                                                </a>
                                                            </div>
                                                          
                                                        </article>
                                                    </div>
                                                </div>	
                                            </div>	
                                        </div>
                                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6">
                                            <div class="panel panel-default card-view pa-0">
                                                <div class="panel-wrapper collapse in">
                                                    <div class="panel-body pa-0">
                                                        <article class="col-item">
                                                            <div class="photo">
                                                                <div class="options">
                                                                    <a href="add-products.html" class="font-18 txt-grey mr-10 pull-left"><i class="zmdi zmdi-edit"></i></a>
                                                                    <a href="javascript:void(0);" class="font-18 txt-grey pull-left sa-warning"><i class="zmdi zmdi-close"></i></a>
                                                                </div>

                                                                <a href="javascript:void(0);"> 
                                                                    <img src="../img/chair.jpg" class="img-responsive mediaimg"  alt="Product Image" /> 
                                                                </a>
                                                            </div>
                                                          </article>
                                                    </div>
                                                </div>	
                                            </div>	
                                        </div>
                                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6">
                                            <div class="panel panel-default card-view pa-0">
                                                <div class="panel-wrapper collapse in">
                                                    <div class="panel-body pa-0">
                                                        <article class="col-item">
                                                            <div class="photo">
                                                                <div class="options">
                                                                    <a href="add-products.html" class="font-18 txt-grey mr-10 pull-left"><i class="zmdi zmdi-edit"></i></a>
                                                                    <a href="javascript:void(0);" class="font-18 txt-grey pull-left sa-warning"><i class="zmdi zmdi-close"></i></a>
                                                                </div>

                                                                <a href="javascript:void(0);"> 
                                                                    <img src="../img/chair.jpg" class="img-responsive mediaimg"  alt="Product Image" /> 
                                                                </a>
                                                            </div>
                                                        </article>
                                                    </div>
                                                </div>	
                                            </div>	
                                        </div>
                                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6">
                                            <div class="panel panel-default card-view pa-0">
                                                <div class="panel-wrapper collapse in">
                                                    <div class="panel-body pa-0">
                                                        <article class="col-item">
                                                            <div class="photo">
                                                                <div class="options">
                                                                    <a href="add-products.html" class="font-18 txt-grey mr-10 pull-left"><i class="zmdi zmdi-edit"></i></a>
                                                                    <a href="javascript:void(0);" class="font-18 txt-grey pull-left sa-warning"><i class="zmdi zmdi-close"></i></a>
                                                                </div>

                                                                <a href="javascript:void(0);"> 
                                                                    <img src="../img/chair.jpg" class="img-responsive mediaimg"  alt="Product Image" /> 
                                                                </a>
                                                            </div>
                                                          </article>
                                                    </div>
                                                </div>	
                                            </div>	
                                        </div>
                                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6">
                                            <div class="panel panel-default card-view pa-0">
                                                <div class="panel-wrapper collapse in">
                                                    <div class="panel-body pa-0">
                                                        <article class="col-item">
                                                            <div class="photo">
                                                                <div class="options">
                                                                    <a href="add-products.html" class="font-18 txt-grey mr-10 pull-left"><i class="zmdi zmdi-edit"></i></a>
                                                                    <a href="javascript:void(0);" class="font-18 txt-grey pull-left sa-warning"><i class="zmdi zmdi-close"></i></a>
                                                                </div>

                                                                <a href="javascript:void(0);"> 
                                                                    <img src="../img/chair.jpg" class="img-responsive mediaimg"  alt="Product Image" /> 
                                                                </a>
                                                            </div>
                                                             </article>
                                                    </div>
                                                </div>	
                                            </div>	
                                        </div>
                                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6">
                                            <div class="panel panel-default card-view pa-0">
                                                <div class="panel-wrapper collapse in">
                                                    <div class="panel-body pa-0">
                                                        <article class="col-item">
                                                            <div class="photo">
                                                                <div class="options">
                                                                    <a href="add-products.html" class="font-18 txt-grey mr-10 pull-left"><i class="zmdi zmdi-edit"></i></a>
                                                                    <a href="javascript:void(0);" class="font-18 txt-grey pull-left sa-warning"><i class="zmdi zmdi-close"></i></a>
                                                                </div>

                                                                <a href="javascript:void(0);"> 
                                                                    <img src="../img/chair.jpg" class="img-responsive mediaimg"  alt="Product Image" /> 
                                                                </a>
                                                            </div>                                                            
                                                        </article>
                                                    </div>
                                                </div>	
                                            </div>	
                                        </div>
                                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6">
                                            <div class="panel panel-default card-view pa-0">
                                                <div class="panel-wrapper collapse in">
                                                    <div class="panel-body pa-0">
                                                        <article class="col-item">
                                                            <div class="photo">
                                                                <div class="options">
                                                                    <a href="add-products.html" class="font-18 txt-grey mr-10 pull-left"><i class="zmdi zmdi-edit"></i></a>
                                                                    <a href="javascript:void(0);" class="font-18 txt-grey pull-left sa-warning"><i class="zmdi zmdi-close"></i></a>
                                                                </div>

                                                                <a href="javascript:void(0);"> 
                                                                    <img src="../img/chair.jpg" class="img-responsive mediaimg"  alt="Product Image" /> 
                                                                </a>
                                                            </div>                                                           
                                                        </article>
                                                    </div>
                                                </div>	
                                            </div>	
                                        </div>
                                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6">
                                            <div class="panel panel-default card-view pa-0">
                                                <div class="panel-wrapper collapse in">
                                                    <div class="panel-body pa-0">
                                                        <article class="col-item">
                                                            <div class="photo">
                                                                <div class="options">
                                                                    <a href="add-products.html" class="font-18 txt-grey mr-10 pull-left"><i class="zmdi zmdi-edit"></i></a>
                                                                    <a href="javascript:void(0);" class="font-18 txt-grey pull-left sa-warning"><i class="zmdi zmdi-close"></i></a>
                                                                </div>

                                                                <a href="javascript:void(0);"> 
                                                                    <img src="../img/chair.jpg" class="img-responsive mediaimg"  alt="Product Image" /> 
                                                                </a>
                                                            </div>                                                           
                                                        </article>
                                                    </div>
                                                </div>	
                                            </div>	
                                        </div>
                                    </div>      
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                    <div class="row">
                                        <ul class="folderpath">
                                            <a href="?path=" class="folderlist">
                                                <li class="<?php echo $activeclass;?>">
                                                    <div class="pull-left">
                                                        <i class="zmdi zmdi-folder mr-5"></i><span class="right-nav-text">Root</span>
                                                    </div>
                                                    <div class="pull-right">
                                                        <span class="label label-default">16</span>
                                                    </div>
                                                    <div class="clearfix"></div>                                               
                                                </li>
                                            </a>
                                            <a href="?path=Test" class="folderlist">
                                                <li>                                                
                                                    <div class="pull-left">
                                                        <i class="zmdi zmdi-folder mr-5"></i>
                                                        <span class="right-nav-text">Test</span>
                                                    </div>
                                                    <div class="pull-right">
                                                        <span class="label label-default">2</span>
                                                    </div>
                                                    <div class="clearfix"></div>                                                
                                                </li>
                                            </a>
                                        </ul>
                                    </div>
                                </div>   
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="imguploadmodal" class="modal fade imguploadmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h5 class="modal-title" id="myModalLabel">New Images</h5>
                    </div>
                    <div class="modal-body">
                        <div class="panel panel-default card-view panel-refresh">
                            <div class="refresh-container">
                                <div class="la-anim-1"></div>
                            </div>
                            <div class="inform_title">
                                <div class="pull-left">
                                    <h6 class="panel-title">NOVELTY!</h6>
                                </div>
                                <div class="pull-right">                                                                       
                                    <a class="pull-left inline-block close-panel" href="#" data-effect="fadeOut">
                                        <i class="zmdi zmdi-close"></i>
                                    </a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="collapse_1" class="panel-wrapper collapse in">
                                <div class="imguploadinfo">
                                    <p>You can now use more formats directly from this tool.<br> 
                                        JPEG, PNG, JPG ... files are directly accepted </p>
                                </div>
                            </div>
                        </div>
                        <div class="form-wrap">
                            <div class="form-group">
                                <label class="control-label mb-10">Folder</label>
                                <select class="selectpicker" data-style="form-control btn-default btn-outline" name="foldernamelist" id="foldernamelist">
                                    <option value="Root">Root</option>
                                    <option value="Test">Test</option>
                                </select>
                            </div>
                            <form action="ajax/upload.php" method="post" class="dropzone" name="uploadmultipleimgs">
                                <input type="hidden" name="foldername" class="foldername" value="Root">
                                <div class="form-group">
                                    <div class="fallback">
                                        <input name="file" type="file" multiple />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>                                       
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <div id="foldernameModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h5 class="modal-title" id="folderModalLabel">New folder name<br/>(letters, numbers, '-', and '_' only)</h5>
                    </div>
                    <div class="form-wrap">                            
                        <form action="ajax/createfolder.php" method="post" class="foldernameclass" name="foldernameform">     
                            <div class="modal-body">
                                <div class="form-group">
                                    <input type="text" name="createfoldername" class="form-control" id="createfoldername">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button class="btn" data-dismiss="modal" aria-hidden="true" style="color:#212121;">Cancel</button>
                                <button class="btn btn-primary" data-dismiss="modal" id="create_folder">OK</button>
                            </div> 
                        </form>
                    </div>    
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <style type="text/css">
            .error{
                color: red !important;
            }
        </style>
        <?php include('footer.php'); ?>

        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>       
        <script type="text/javascript">
            window.onscroll = function () {
                scrollFunction()
            };

            function scrollFunction() {
                if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
                    document.getElementById("uploadmediaimage").style.position = "fixed";
                } else {
                    document.getElementById("uploadmediaimage").style.position = "unset";
                }
            }

            $(document).on('change', '#foldernamelist', function () {
                var foldername = $('#foldernamelist option:selected').val();
                $(".foldername").val(foldername);
            });
//            $(document).on('change', '#tAddressProof', function (e) {
//                var filename = e.target.files[0].name;
//                var fExtension;
//                fExtension = filename.replace(/^.*\./, '');
//                if (fExtension != 'gif' && fExtension != 'jpg' && fExtension != 'png' && fExtension != 'jpeg') {
//                    alert('Image file type must be PNG, JPG, JPEG or GIF');
//                    $(".dropify-clear").trigger("click");
//                    return false;
//                }
//            });
        </script>
        <script>
            $(document).on('click', '.close', function () {
                Dropzone.forElement(".dropzone").removeAllFiles(true);
            });
            $(document).on('click', '.upload-image', function () {
                $('#imguploadmodal').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                //Dropzone.forElement(".dropzone").removeAllFiles(true);
            });

            $(document).on('click', '#create_folder', function ()
            {
                var foldername = $("#createfoldername").val();
                var data = {
                    "foldername": foldername
                };
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "ajax/createfolder.php",
                    data: data,
                    success: function (data) {

                    }
                });
            });
            
            $(document).on('click', '.folderlist', function(){
                $("li").removeClass("active");
            });
        </script>
