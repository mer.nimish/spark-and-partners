<?php 
session_start();
require 'config/config.php';
require 'model/model.php';
global $conn;


if (isset($_SESSION['first_name']) && isset($_SESSION['last_name'])) {
    $first_name = $_SESSION['first_name'];
    $last_name = $_SESSION['last_name'];
} else {
    echo "<script>window.location='index.php'</script>";
    exit;
}
$allbuilding = allBuilding();
$mybuilding = myBuilding();

include('header.php');
include('left_sidebar.php');

?>
 
    <div class="page-wrapper">
            <div class="container-fluid pt-25"> 
              <div  class="pills-struct">
                <ul role="tablist" class="nav nav-pills" id="myTabs_6">
                  <li class="active" role="presentation"><a aria-expanded="true"  data-toggle="tab" role="tab" id="home_tab_6" href="#shared_building">Shared Buildings</a></li>
                  <li role="presentation" class=""><a  data-toggle="tab" id="profile_tab_6" role="tab" href="#my_building" aria-expanded="false">My Buildings</a></li>
                  <li role="presentation" class="">
                  <a href="add_building.php" class="btn text-white btn-primary left-label"> <span class="btn-label"><i class="fa fa-plus"></i> </span><span class="btn-text">New Building</span></a>
                  </li>
                  
                </ul>
                <div class="tab-content" id="myTabContent_6">
                  <div  id="shared_building" class="tab-pane fade active in" role="tabpanel">
                   <div class="row">
                     <div class="" style="margin-top:20px;margin-bottom:5px; margin-left:0% "></div>
                       <div class="col-sm-12">
                          <div class="panel panel-default card-view">
                             <div class="panel-heading">
                                <div class="pull-left">
                                   <h6 class="panel-title txt-dark">List of shared buildings</h6>
                                </div>
                                <div class="clearfix"></div>
                             </div>
                             <div class="panel-wrapper collapse in">
                                <div class="panel-body">
                                   <div class="table-wrap">
                                      <div class="table-responsive">
                                         <table id="shared_building_tbl" class="table table-hover display  pb-30" >
                                            <thead>
                                               <tr>
                                                  <th width="40%" >Name</th>
                                                  <th width="20%">District</th>
                                                  <th width="30%">Address</th>
                                                  <th width="10%">Action</th>
                                                  
                                               </tr>
                                            </thead>
                                            <thead id="table-tfoot">
                                              <tr class="table-header">
                                                 <td width="40%" >Name</td>
                                                  <td width="20%">District</td>
                                                  <td width="30%">Address</td>
                                                  <td></td>
                                              </tr>
                                            </thead>
                                            <tbody>
                                               <?php
                                               if(isset($allbuilding) && $allbuilding != 0){
                                                 foreach($allbuilding as $building) 
                                                  
                                                    {?>
                                                  <tr>
                                                    <td><?php echo $building['building_name'];?></td>
                                                    <td><?php echo $building['district'];?></td>
                                                    <td><?php echo $building['address'];?></td>
                                                    <td>
                                                      <a href="building.php?building_id=<?php echo $building['building_id'];?>"><button class="btn btn-primary btn-icon-anim btn-square"><i class="fa fa-desktop"></i></button>
                                                      </a>
                                                    </td>
                                        
                                                  </tr>
                                               <?php }
                                                }
                                                ?>
                                            </tbody>
                                            <tfoot>
                                               <tr>
                                                  <th >Name</th>
                                                  <th >District</th>
                                                  <th >Address</th>
                                                  <th >Action</th>
                                               </tr>
                                            </tfoot>
                                         </table>
                                      </div>
                                   </div>
                                </div>
                             </div>
                          </div>
                       </div>
                    </div>
                  </div>
                  <div  id="my_building" class="tab-pane fade" role="tabpanel">
                    <div class="row">
                      <div class="response_message" style="margin-top:20px;margin-bottom:5px; margin-left:0% "></div>
                       <div class="col-sm-12">
                          <div class="panel panel-default card-view">
                             <div class="panel-heading">
                                <div class="pull-left">
                                   <h6 class="panel-title txt-dark">List of my buildings</h6>
                                </div>
                                <div class="clearfix"></div>
                             </div>
                             <div class="panel-wrapper collapse in">
                                <div class="panel-body">
                                   <div class="table-wrap">
                                      <div class="table-responsive">
                                         <table id="my_building_tbl" class="table table-hover display  pb-30" >
                                            <thead>
                                              
                                               <tr>
                                                  <th width="40%" >Name</th>
                                                  <th width="20%">District</th>
                                                  <th width="30%">Address</th>
                                                  <th width="10%">Action</th>
                                                  
                                               </tr>
                                            </thead>
                                            <thead id="table-tfoot">
                                              <tr class="table-header">
                                                 <td width="40%" >Name</td>
                                                  <td width="20%">District</td>
                                                  <td width="30%">Address</td>
                                                 <td></td>
                                              </tr>
                                            </thead>
                                            
                                            <tbody>
                                             
                                               <?php
                                               if(isset($mybuilding) && $mybuilding != 0){
                                                 foreach($mybuilding as $building1) 
                                                  
                                                    {?>
                                                  <tr id="row_<?=$building1["building_id"]?>">
                                                    <td><a href="building.php?building_id=<?php echo $building1['building_id'];?>"><?php echo $building1['building_name'];?></a></td>
                                                    <?php $district_name =  districtName($building1['district']);?>
                                                    <td><?php echo $district_name['district_name'];?></td>
                                                    <td><?php echo $building1['address'];?></td>
                                                    <td class="text-nowrap">
                                                     <a href="building.php?building_id=<?php echo $building1['building_id'];?>"><button class="btn btn-primary btn-icon-anim btn-square"><i class="fa fa-desktop"></i></button></a>
                                                     <a href="edit_building.php?building_id=<?php echo $building1['building_id'];?>"><button class="btn btn-default btn-icon-anim btn-square"><i class="fa fa-pencil"></i></button></a>
                                                     <a href="#myModal" data-toggle="modal" data-original-title="Remove" building_id="<?php echo $building1['building_id']; ?>" class="removebuilding"><button class="btn btn-danger btn-icon-anim btn-square"><i class="fa fa-trash"></i></button></a>
                                                     </td>
                                        
                                                  </tr>
                                               <?php }
                                                }
                                                ?>
                                           </tbody>
                                            
                                            <tfoot>
                                               <tr>
                                                  <th >Name</th>
                                                  <th >District</th>
                                                  <th >Address</th>
                                                  <th >Action</th>
                                               </tr>
                                            </tfoot>
                                         </table>
                                         
                                      </div>
                                   </div>
                                </div>
                             </div>
                          </div>
                       </div>
                    </div>
                  </div>
                 
                </div>
              </div>

              <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h5 class="modal-title" id="myModalLabel">Confirm</h5>
                    </div>
                    <div class="modal-body">
                        <h5 class="mb-15">Are you sure want to delete this building?</h5>                        
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">No</button>
                        <button class="btn btn-primary" data-dismiss="modal" building_id="" id="building_delete_yes">Yes</button>
                    </div>                    
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
<script>
    // Main reference for data-table
    //var $data_table = $( '#data-table' );
   $(document).ready(function() {
    // Initialize DataTable
    var table = $( '#my_building_tbl' ).DataTable( );
    var $dt_head = $( '#my_building_tbl' )  .find( 'thead' ).find( 'td' );
 
    $dt_head.each( function () {
        var title = $dt_head.eq( $( this ).index()).text();
 
        if( title != 'Action' ) {
            $( this ).html($('<input/>', { 'type': 'text', 'placeholder': 'Search ' + title }).addClass('form-control input-xs table-column-search width-full p-0'));
        }
    });
 
    // Apply column filters at footer
    $("#my_building_tbl  thead input").on( 'keyup change', function () {
        table
            .column( $(this).parent().index()+':visible' )
            .search( this.value )
            .draw();
    } );
    

  
    } );

  
   $(document).ready(function () {
    /* shared building Search **/
    var table = $( '#shared_building_tbl' ).DataTable( );
    var $dt_head = $( '#shared_building_tbl' )  .find( 'thead' ).find( 'td' );
 
    $dt_head.each( function () {
        var title = $dt_head.eq( $( this ).index()).text();
 
        if( title != 'Action' ) {
            $( this ).html($('<input/>', { 'type': 'text', 'placeholder': 'Search ' + title }).addClass('form-control input-xs table-column-search width-full p-0'));
        }
    });
 
    // Apply column filters at footer
    $("#shared_building_tbl  thead input").on( 'keyup change', function () {
        table
            .column( $(this).parent().index()+':visible' )
            .search( this.value )
            .draw();
    } );

    /** Delete a building*/
    $(document).delegate('.removebuilding', 'click', function () {
        $('#building_delete_yes').attr('building_id', $(this).attr('building_id'));
    });

    $('#building_delete_yes').on('click', function ()
    {
        var building_id = $(this).attr('building_id');
        var data = {
            "action": "delete_building",
            "building_id": building_id
        };
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "ajax/delete_building.php", //Relative or absolute path to response.php file
            data: data,
            success: function (data) {
                $(".response_message").html(
                        data["message"]
                        );
                if (data["success"] === 'true') {
                    $('#row_' + building_id).remove();
                }
            }
        });
    });
});


</script>

<style type="text/css">
  .dataTables_scroll{position:relative}
.dataTables_scrollHead{margin-bottom:40px;}
.dataTables_scrollFoot{position:absolute; top:38px}
</style>
<?php include('footer.php');?>