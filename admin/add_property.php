<?php 
session_start();
require 'config/config.php';
require 'model/model.php';
global $conn;


if (isset($_SESSION['first_name']) && isset($_SESSION['last_name'])) {
    $first_name = $_SESSION['first_name'];
    $last_name = $_SESSION['last_name'];
} else {
    echo "<script>window.location='index.php'</script>";
    exit;
}
$allcountry = getAllCountry();

if(isset($_REQUEST['addpropertybtn'])) {


    $last_inserted_id = addProperty($_REQUEST);
    if($last_inserted_id !== '' && isset($last_inserted_id)){
        header('location: edit_property.php?propertyid='.$last_inserted_id.'');

    }else{
        $errorOtherMsg = 'Opps! Somthing went wrong.';
        $requestdata = $_REQUEST;
    }

}
include('header.php');
include('left_sidebar.php');

?>

<div class="page-wrapper">
    <div class="container-fluid pt-25">	
        <div class="col-md-12">

            <?php if(isset($errorOtherMsg)){ ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <i class="zmdi zmdi-block pr-15 pull-left"></i><p class="pull-left"><?=$errorOtherMsg?></p>
                <div class="clearfix"></div>
            </div>
            <?php } ?>
        </div>

        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark">New property assistant</h5>
            </div>
        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default card-view">

                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">    
                            <form method="post" name="addproperty">
                                <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCGmRiNH9b_Ts8meG7fgcH2rhUYy7-FCqY&libraries=places&language=en"></script>

                                <div id="step1" class="step bg-current">
                                    <h4><span class="circle"></span> CHOOSE THE<strong> PROPERTY LOCATION</strong></h4>
                                    <div class="row" id="localisation">
                                        <div class="col-sm-4 sector-country">
                                            <div class="form-group">
                                                <div class="form-group-line">
                                                    <label class="control-label" for="country">Country</label>
                                                    <div class="input-elt input-elt-select ">
                                                        <select id="country" name="country" class=" form-control form-control" data-container="body" data-live-search="true">
                                                           <?php 
                                                           if(isset($allcountry) && $allcountry != 0){
                                                            while ($country = mysqli_fetch_assoc($allcountry)) { ?>
                                                            <option value="<?=$country['country_id']?>"  data-code="<?=$country['iso_code']?>"><?=$country['country_nicename']?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?> 
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="user_id" value="<?php echo $_SESSION['user_id'];?>">


                                    <script>
                                        $(function () {
                                            $("#country").change(function () {
                                                $("#manual").prop("checked", false);
                                                $("#address, #city, #codepostal, #region").val("");
                                                $(".sector-city").addClass("hidden");
                                                if ($(this).val()==141) {
                                                    $(".sector-building").removeClass("hidden");
                                                    $(".sector-address").addClass("hidden");
                                                    setupBuildings($(this).val());
                                                }
                                                else if ($(this).val()>0) {
                                                    $(".sector-building").addClass("hidden");
                                                    $(".sector-address").removeClass("hidden");
                                                }
                                                else {
                                                    $(".sector-building").addClass("hidden");
                                                    //$(".sector-address").addClass("hidden");
                                                }
                                                setupAddress();
                                                checkStep();
                                                if (typeof makeReference != "undefined") makeReference();
                                            }).change();
                                        });
                                    </script>
                                </div>
                                <div class="col-sm-8 sector-address">
                                    <div class="form-group" data-rel="tooltip" title="Private information">
                                        <div class="form-group-line">
                                            <label class="control-label" for="address">Address</label>
                                            <div class="input-elt ">
                                                <div class="input-group">
                                                    <input type="text" id="address" name="address" value="" class="private form-control" placeholder="Enter an address">
                                                    <div class="input-group-addon"><i class="fa fa-lock"></i></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                   <!--  <div class="form-group sector-manual hidden">
                                        <div class="form-group-line">
                                            <label class="control-label" for="manual">Address not found - manual</label>
                                            <div class="input-elt input-checkbox ">
                                                <div class="input-group">
                                                    <input type="checkbox" id="manual" name="manual" class=" form-control" value="1"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->
                                </div>
                                <input type="hidden" name="latitude" id="latitude">
                                <input type="hidden" name="longitude" id="longitude">

                                <div class="sector-building hidden">
                                    <script>

                                        function setupBuildings(pays)
                                        {
                                            console.log("setupBuildings("+pays+")");
                                            var $select = $("#building");
                                            if (pays==141) {
                                                var val = $select.val();
                                                if ($select.data("pays")==pays) {
                                                    $(".sector-building").removeClass("hidden");
                                                }
                                                else {
                                                    $select.data("pays", null).find("option").remove();
                                                    $.ajax({
                                                        method: 'post',
                                                        url: 'ajax/getbuilding.php',
                                                        data: {id: pays}
                                                    }) .done(function(data){
                                                        $select.data("pays", );
//                                $select.append("<option value=''></option>");

if (data.length>0) {

    var cpt= 0,curval=false;
    $.each( JSON.parse(data), function( k, v ) {
        cpt++;
        curval = k.building_id;
        console.log(v.address);
        $('#building_address').val(v.address);
        $select.append("<option value='"+(v.building_id>0? v.building_id:"")+"' data-district='"+(v.district)+"'>"+ v.building_name+"</option>");

    });
    $select.val(val).change();
}
else {
                                    //$select.append("<option value='no'>Aucun imeuble</option>");
                                }

                                $select.append('<option disabled="disabled" data-divider="true">-------------------</option>');
                                $select.append("<option value='other' data-subtext='district uniquement'>Other (Specify only the district)</option>");
                                $select.append("<option value='new' data-icon='glyphicon-plus'>New building</option>");
                                $select.selectpicker("refresh");
                                $(".sector-building").removeClass("hidden");
                            });
                                                }
                                            }
                                            else {
                                                $(".sector-building").addClass("hidden");
                                            }
                                        }
                                        function setupdistricts (ville)
                                        {
                                            console.log("setupdistricts("+ville+")");
                                            var $select = $("#district");

                                            if (ville>0) {
                                                var val = $select.val();
                //                         if ($select.data("ville")==ville) {
                //                             console.log("  liste déjà à jour");
                // //                            $(".sector-building").removeClass("hidden");
                //                         }
                //                      else {
                                        console.log("  màj liste");
                                        $select.data("ville", null).find("option").remove();
                                        $select.selectpicker("refresh");
                                        $.ajax({
                                            method: 'post',
                                            url: 'ajax/getdistrict.php',
                                            data: {id: ville}
                                        }) .done(function(data) {
                                            $select.data("ville", ville).find("option").remove();
                                        //  $select.append("<option value=''></option>");
                                        $select.append("<option value='-1'>Other district</option>");
                                        if (data.length>0) {
                                            var cpt= 0,curval=false;
                                            $.each( JSON.parse(data), function( k, v ) {
                                                cpt++;
                                                curval = k.district_id;
                                                $select.append("<option value='"+(v.district_id>0? v.district_id:"")+"'>"+ v.district_name+"</option>");
                                            });
                                            $select.val(val);
                                            console.log("  liste màj ("+data.length+" valeurs)");
                                        }
                                        else {
                                            $select.append("<option value='-1'>Other district</option>");
                                        }
                                        if (ville!=45 && !$("#city").attr("data-code")) {
                                            $select.append("<option value='new' data-icon='glyphicon-plus'>New district</option>");
                                        }

                                        $select.selectpicker("refresh");
                                //$(".sector-building").removeClass("hidden");
                            });
                        //}
                    }
                    else {
                        //$(".sector-building").addClass("hidden");
                    }
                }
            </script>
            <div class="col-sm-4 ">
                <div class="form-group group-building">
                    <div class="form-group-line"><label class="control-label" for="building">building</label>

                        <div class="input-elt input-elt-select has-addons">
                            <div class="input-group">
                                <select id="building" name="building" data-live-search="data-live-search" class=" form-control form-control" data-container="body" title="Choose Building...">                    
                                </select>
                                <div class="input-group-addon addon-building-info"><i class="infosbuilding fa fa-info-circle" title="Détails de l'building" data-rel="tooltip"></i></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group new-building">
                    <div class="form-group-line"><label class="control-label" for="building_other">New
                        building</label>
                        <div class="input-elt has-addons">
                            <div class="input-group"><input type="text" id="building_other"
                                name="building_other" class=" form-control"/>

                                <div class="input-group-addon">
                                    <div title="Entrez ici le nom d'un building pour le créer" data-rel="tooltip"><i
                                        class="fa fa-info"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" value="" name="building_address" id="building_address">
                </div>

                <script>
                    $(function () {
                    // $(".infosbuilding").parent().css("cursor", "pointer").click(function () {
                    //     if ($("#building").val()) {
                    //         openPopup("/building/42".replace("42", $("#building").val()));
                    //     }
                    // });
                    $("#building").change(function () {
                        //alert("hi");
                        var val = $(this).val();
                        if ($("#country").val()==141) {
                            console.log("#building change (" + val + ")");
                            setupdistricts(45);
                            // alert(val);
                            if (val == "new") {
                                $("#city, #codepostal, #region").val("monaco");
                                $(".sector-city").removeClass("hidden");
                                $(".no_building").addClass("hidden");
                                $(".new-building").removeClass("hidden");
                                $(".sector-district").removeClass("hidden");

                                $("#district").attr("disabled", false).selectpicker("refresh");
                            }
                            else if (val == "other") {
                                $("#city, #codepostal, #region").val("monaco");
                                $(".sector-city").removeClass("hidden");
                                // district uniquement
                                $(".no_building").addClass("hidden");
                                $(".sector-quartier").removeClass("hidden");
                                $(".new-building").addClass("hidden");
                                
                                $("#district").attr("disabled", false).selectpicker("refresh");
                            }
                            else if (val > 0) {
                                $("#city, #codepostal, #region").val("monaco");
                                $(".sector-city").removeClass("hidden");
                                //$(".no_building").addClass("hidden");
                                $(".new-building").addClass("hidden");
                                //if () {
                                    //console.log(district_id);
                                    district_id = $(this).find("option:selected").attr("data-district");
                                    alert("Building selectd");
                                    $('#district').val(district_id).selectpicker("refresh");
                                    //$('#district').attr("disabled","disabled");
                                    //$("#district").selectpicker("refresh");
                                    //$("#district").change();
                                    $(".sector-district").removeClass("hidden");
                                    //}
                                }
                                else {
                                    $(".sector-city").addClass("hidden");

                                //$(".no_building").removeClass("hidden");
                                $(".new-building").addClass("hidden");
                                $(".sector-district").addClass("hidden");
                            }
                            checkStep();
                        }
                    }).change();
                    $("#building_other").change(function () {
                        checkStep();
                    });
                    $("#district").change(function () {
                        checkStep();
                    });
                    if (typeof makeReference != "undefined") makeReference();
                });
            </script>
            <div class="col-sm-4 sector-district hidden">
                <div class="form-group group-district">
                    <div class="form-group-line"><label class="control-label" for="district">district</label>

                        <select id="district" name="district" data-none-selected-text="Autre/Non défini" class=" form-control form-control" data-live-search="true" data-container="body"></select>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="sector-city hidden">
            <div class="clearfix"></div>
            <script>
                var autocomplete, autocompleteLsr;
                function setupAddress ()
                {
                    var autocompleteRestrict = {'country': $("#country option:selected").attr("data-code")};
                    if (!autocomplete) {
                        autocomplete = new google.maps.places.Autocomplete(
                            document.getElementById('address'),
                            {
                                types: ['geocode'],
                                componentRestrictions: autocompleteRestrict
                            }
                            );

                        autocompleteLsr = autocomplete.addListener('place_changed', handleAddress);
                    }
                    else {
                        autocomplete.componentRestrictions = autocompleteRestrict;
                    }
                }
                $("#manual").change(function () {
                    if ($(this).prop("checked")) {
                        $("#city, #codepostal, #region").val("");
                        $(".sector-city").addClass("hidden");
                    }
                    checkStep();
                });
                $("#address").on("focus keyup", function () {
                    var $manual = $(".pac-item-manual");
                    if ($(this).val().length>4) {
                        $(".sector-manual").removeClass("hidden");
                    }
                    else {
                        $(".sector-manual").addClass("hidden");
                    }
                    if (!$manual.length) {
                        var $manual = $('<div class="pac-item pac-item-manual"><span class="pac-icon pac-icon-marker"></span><span class="pac-item-query">Address manual</a></span></div>');
                        $manual.on("mousedown", function () {
                            $("#manual").prop("checked", true).checkboxradio().change();
                            $("#address").blur();
                        });
                    }
                    window.setTimeout(function () {
                        if (!$(".pac-container .pac-item-manual").length) $(".pac-container").append($manual);
                        $(".pac-container").css("display", "block");
                    }, 500);
                });
                function handleAddress () {
                    var place = autocomplete.getPlace();
                    //console.log(place);
                    $("#city, #codepostal, #region").val("");
                    if (!place) {
                        $(".sector-city").addClass("hidden");
                        $(".sector-manual").removeClass("hidden");
                        return;
                    }
                    $(".sector-manual").addClass("hidden");
                    $("#manual").prop("checked", false).checkboxradio();
                    $(".sector-city").removeClass("hidden");

                    for (var i = 0; i < place.address_components.length; i++) {
                        var addressType = place.address_components[i].types[0];
                        var val = place.address_components[i]["long_name"];
                        var longitude=place.geometry.location.lng();
                        var latitude=place.geometry.location.lat();
                        $('#latitude').val(latitude);
                        $('#longitude').val(longitude);
                        if (addressType=="locality" || (addressType=="postal_town" && !$("#city").val())) {
                            $("#city").val(val);
                        }
                        else if (addressType=="postal_code") {
                            $("#codepostal").val(val);
                        }
                        else if (addressType=="administrative_area_level_1") {
                            $("#administrative1").val(val);
                            if ($("#region").val()) $("#region").val($("#region").val()+" - "+val);
                            else $("#region").val(val);
                        }
                        else if (addressType=="administrative_area_level_2") {
                            $("#administrative2").val(val);
                            if ($("#region").val()) $("#region").val($("#region").val()+" - "+val);
                            else $("#region").val(val);
                        }
                        else {

                        }
                    }
                    
                    checkStep();
                }
            </script>
            <div class="col-sm-4">
                <div class="group-city">
                    <div class="form-group  ">
                        <div class="form-group-line">
                            <label class="control-label" for="city">city</label>
                            <div class="input-elt ">
                                <div class="input-group">
                                    <input type="text" id="city" name="city" class="form-control" value="" readonly  />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="group-codepostal">
                    <div class="form-group  ">
                        <div class="form-group-line">
                            <label class="control-label" for="codepostal">Code postal</label>
                            <div class="input-elt ">
                                <div class="input-group">
                                    <input type="text" id="codepostal" name="codepostal" class="form-control" value="" readonly  />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="group-region">
                    <div class="form-group  ">
                        <div class="form-group-line">
                            <label class="control-label" for="region">Region</label>
                            <div class="input-elt ">
                                <div class="input-group">
                                    <input type="text" id="region" name="region" class="form-control" value="" readonly />
                                    <input type="hidden" id="administrative1" name="administrative1" value="" />
                                    <input type="hidden" id="administrative2" name="administrative2" value="" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="step2" class="step hidden">
    <h4><span class="circle"></span> CHOOSE YOUR<strong> TYPE OF TRANSACTION</strong></h4>
    
    <div class="row">
        <div class="col-sm-6">  
            <label class="btn-block">
                <input type="radio" name="typetransaction" id="typetransaction_Sale" autocomplete="off" value="1" required > Good at <strong>Sale</strong>
            </label>
        </div>
        <div class="col-sm-6">
            <label class="btn-block">
                <input type="radio" name="typetransaction" id="typetransaction_Location" autocomplete="off" value="5" required > Well at <strong>Location</strong>
            </label>
        </div>
    </div>
    <script>
      $( function() {
        $('input[type="radio"]').checkboxradio();
    } );
</script> 
<script>
    $(function () {
        $('input[name="typetransaction"]').change(function () {
            if ($(this).is(":checked")) {
                $("#sector-typeoffre [data-typetransaction]").addClass("hidden");
                var val = $(this).val();
                $("#sector-typeoffre").removeClass("hidden");
                $("#sector-typeoffre [data-typetransaction='"+val+"']").removeClass("hidden");

                if (!$("#sector-typeoffre [data-typetransaction='"+val+"'] input[type=radio]:checked").length) {
                    $("#sector-typeoffre [data-typetransaction='"+val+"'] input[type=radio]").first().attr("checked", "checked").change().checkboxradio();
                }
                checkAuto();
                checkStep();
                if (typeof makeReference != "undefined") makeReference();
            }
        });
        $("input[name=typetransaction]:checked").change();
    });
</script>

<div id="sector-typeoffre" class="hidden">
    <h5>SPECIFY YOUR TYPE OF TRANSACTION</h5>

    <div class="row smallinput">
        <div class="col-sm-3 hidden" data-typetransaction="1">
            <label>
                <input type="radio" name="typeoffre" id="typeoffre_1" autocomplete="off" value="1" >
                <strong>Sale</strong>
            </label>
        </div>
        <div class="col-sm-3 hidden" data-typetransaction="1">
            <div style="display: table;width: 100%;">
                <div class="sector_viager">
                    <label>
                        <input type="radio" name="typeoffre" id="typeoffre_2" autocomplete="off" value="2" >
                        lifetime
                    </label>
                </div>
                <div class="sector_type_viager hidden">
                    <select name="typeViager" id="typeViager" class="form-control selectpicker">
                        <option value="Free life" >Free life</option>
                        <option value="Life annuity" >Life annuity</option>
                    </select>
                </div>
            </div>  
        </div>
        <div class="col-sm-3 hidden" data-typetransaction="1">
            <label>
                <input type="radio" name="typeoffre" id="typeoffre_3" autocomplete="off" value="3" >
                New program
            </label>

        </div>
        <div class="col-sm-3 hidden" data-typetransaction="1">
            <label>
                <input type="radio" name="typeoffre" id="typeoffre_4" autocomplete="off" value="4" >
                Proffesional<strong> sale</strong>
            </label>

        </div>
        <div class="col-sm-3 hidden" data-typetransaction="5">
            <label>
                <input type="radio" name="typeoffre" id="typeoffre_5" autocomplete="off" value="5" >
                <strong>Location</strong>
            </label>

        </div>
        <div class="col-sm-3 hidden" data-typetransaction="5">
            <label>
                <input type="radio" name="typeoffre" id="typeoffre_6" autocomplete="off" value="6" >
                <strong>Location</strong> furnished
            </label>

        </div>
        <div class="col-sm-3 hidden" data-typetransaction="5">
            <label>
                <input type="radio" name="typeoffre" id="typeoffre_7" autocomplete="off" value="7" >
                <strong>Location</strong> seasonal
            </label>

        </div>
        <div class="col-sm-3 hidden" data-typetransaction="5">
            <label>
                <input type="radio" name="typeoffre" id="typeoffre_8" autocomplete="off" value="8" >
                <strong>Location</strong> professional
            </label>
        </div>

        <script>
            $(function () {
                $("input[name=typeoffre]").change(function () {
                    if ($(this).is(':checked')) {
                        var typeoffre = $(this).val();
                        if (typeoffre==2 && $(this).is(":checked")) {
                            $(".sector_type_viager").removeClass("hidden").find("select").attr("required", "required");
                        }
                        else {
                            $(".sector_type_viager").addClass("hidden").find("select").attr("required", false);
                        }
                        $("[data-forTypesOffres]").each(function () {
                            if ($(this).attr("data-forTypesOffres").indexOf(","+typeoffre+",")>=0) {
                                $(this).removeClass("hidden");
                            }
                            else {
                                $(this).addClass("hidden").find("input[type=radio]:checked").attr("checked", false).change().checkboxradio();
                            }
                        });
                        if (typeof makeReference != "undefined") makeReference();
                    }
                    checkAuto();
                    checkStep();
                });
                $("input[name=typeoffre]:checked").change();
            });
        </script>
    </div>
</div>
</div>                            
<div id="step3" class="step hidden">
    <h4><span class="circle"></span> CHOOSE YOUR<strong>PROPERTY TYPE</strong></h4>
    <div class="row">
        <div class="col-sm-4 hidden" data-forTypesOffres=",1,2,6,5,7,3,">
            <label class="btn-block">
                <input type="radio" name="typeproperty" autocomplete="off" value="1" required > Appartement
            </label>
        </div>
        <div class="col-sm-4 hidden" data-forTypesOffres=",1,2,6,5,7,3,">
            <label class="btn-block">
                <input type="radio" name="typeproperty" autocomplete="off" value="7" required > House
            </label>
        </div>
        <div class="col-sm-4 hidden" data-forTypesOffres=",1,2,5,">
            <label class="btn-block">
                <input type="radio" name="typeproperty" autocomplete="off" value="12" required > Ground
            </label>
        </div>
        <div class="col-sm-4 hidden" data-forTypesOffres=",1,2,3,5,">
            <label class="btn-block">
                <input type="radio" name="typeproperty" autocomplete="off" value="11" required > Parking / Garage / Box
            </label>
        </div>
        <div class="col-sm-4 hidden" data-forTypesOffres=",8,4,">
            <label class="btn-block">
                <input type="radio" name="typeproperty" autocomplete="off" value="19" required > Office
            </label>
        </div>
        <div class="col-sm-4 hidden" data-forTypesOffres=",1,2,3,5,">
            <label class="btn-block">
                <input type="radio" name="typeproperty" autocomplete="off" value="20" required > Cellar
            </label>
        </div>
        <div class="col-sm-4 hidden" data-forTypesOffres=",8,4,">
            <label class="btn-block">
                <input type="radio" name="typeproperty" autocomplete="off" value="21" required > Local
            </label>
        </div>
        <div class="col-sm-4 hidden" data-forTypesOffres=",4,">
            <label class="btn-block">
                <input type="radio" name="typeproperty" autocomplete="off" value="25" required > Commercial property
            </label>
        </div>
        <div class="col-sm-4 hidden" data-forTypesOffres=",4,">
            <label class="btn-block">
                <input type="radio" name="typeproperty" autocomplete="off" value="27" required > Commercial premises walls
            </label>
        </div>
        <div class="col-sm-4 hidden" data-forTypesOffres=",4,">
            <label class="btn-block">
                <input type="radio" name="typeproperty" autocomplete="off" value="26" required > Assignment of right to the lease
            </label>
        </div>
        <div class="col-sm-4 hidden" data-forTypesOffres=",1,4,5,7,8,6,">
            <label class="btn-block">
                <input type="radio" name="typeproperty" autocomplete="off" value="28" required > Other Property
            </label>
        </div>
    </div>
    <script>
        $(function () {
            $('input[name="typeproperty"]').change(function () {
                if ($(this).is(":checked")) {
                    $("#sector-subtypeproperty [data-typeproperty]").addClass("hidden");
                    var val = $(this).val();
                    $("#sector-subtypeproperty").removeClass("hidden");
                    $("#sector-subtypeproperty [data-typeproperty='"+val+"']").removeClass("hidden");

                    if (!$("#sector-subtypeproperty [data-typeproperty='"+val+"'] input[type=radio]").length) {
                        $("#sector-subtypeproperty").addClass("hidden");
                    }
                    else if (!$("#sector-subtypeproperty [data-typeproperty='"+val+"'] input[type=radio]:checked").length) {
                        $("#sector-subtypeproperty [data-typeproperty='"+val+"'] input[type=radio]").first().attr("checked", "checked").change().checkboxradio();
                    }
                    checkAuto();
                    checkStep();
                    if (typeof makeReference != "undefined") makeReference();
                }
                else {
                    if ($("input[name=typeproperty]:checked").length==0) {
                        $("#sector-subtypeproperty [data-typeproperty]").addClass("hidden").find("input[type=radio]").first().change();
                        $("#sector-subtypeproperty").addClass("hidden");
                        checkAuto();
                        checkStep();
                    }
                }
            });
            $("input[name=typeproperty]:checked").change();
        });
    </script>

    <div id="sector-subtypeproperty" class="hidden">
        <h5>SPECIFY YOUR TYPE OF PROPERTY</h5>

        <div class="row smallinput">
            <div class="col-sm-3 hidden" data-typeproperty="5">
                <label>
                    <input type="radio" name="subtypeproperty" id="subtypeproperty19" autocomplete="off" value="19" >
                    Office
                </label>
            </div>
            <div class="col-sm-3 hidden" data-typeproperty="20">
                <label>
                    <input type="radio" name="subtypeproperty" id="subtypeproperty20" autocomplete="off" value="20" >
                    Cellar
                </label>
            </div>
            <div class="col-sm-3 hidden" data-typeproperty="9">
                <label>
                    <input type="radio" name="subtypeproperty" id="subtypeproperty21" autocomplete="off" value="21" >
                    Local
                </label>
            </div>
            <div class="col-sm-3 hidden" data-typeproperty="11">
                <label>
                    <input type="radio" name="subtypeproperty" id="subtypeproperty22" autocomplete="off" value="22" >
                    Parking
                </label>
            </div>
            <div class="col-sm-3 hidden" data-typeproperty="11">
                <label>
                    <input type="radio" name="subtypeproperty" id="subtypeproperty23" autocomplete="off" value="23" >
                    Garage
                </label>
            </div>
            <div class="col-sm-3 hidden" data-typeproperty="11">
                <label>
                    <input type="radio" name="subtypeproperty" id="subtypeproperty24" autocomplete="off" value="24" >
                    Box
                </label>
            </div>
            <div class="col-sm-3 hidden" data-typeproperty="18">
                <label>
                    <input type="radio" name="subtypeproperty" id="subtypeproperty25" autocomplete="off" value="25" >
                    Commercial property
                </label>
            </div>
            <div class="col-sm-3 hidden" data-typeproperty="22">
                <label>
                    <input type="radio" name="subtypeproperty" id="subtypeproperty26" autocomplete="off" value="26" >
                    Assignments of right to the lease
                </label>
            </div>
            <div class="col-sm-3 hidden" data-typeproperty="24">
                <label>
                    <input type="radio" name="subtypeproperty" id="subtypeproperty27" autocomplete="off" value="27" >
                    Commercial premises walls
                </label>
            </div>
            <div class="col-sm-3 hidden" data-typeproperty="28">
                <label>
                    <input type="radio" name="subtypeproperty" id="subtypeproperty28" autocomplete="off" value="28" >
                    Other Property
                </label>
            </div>
            <div class="col-sm-3 hidden" data-typeproperty="28">
                <label>
                    <input type="radio" name="subtypeproperty" id="subtypeproperty29" autocomplete="off" value="29" >
                    Building
                </label>
            </div>
            <div class="col-sm-3 hidden" data-typeproperty="1">
                <label>
                    <input type="radio" name="subtypeproperty" id="subtypeproperty1" autocomplete="off" value="1" >
                    Appartement
                </label>
            </div>
            <div class="col-sm-3 hidden" data-typeproperty="1">
                <label>
                    <input type="radio" name="subtypeproperty" id="subtypeproperty2" autocomplete="off" value="2" >
                    Loft
                </label>
            </div>
            <div class="col-sm-3 hidden" data-typeproperty="1">
                <label>
                    <input type="radio" name="subtypeproperty" id="subtypeproperty3" autocomplete="off" value="3" >
                    Penthouse/Roof
                </label>
            </div>
            <div class="col-sm-3 hidden" data-typeproperty="1">
                <label>
                    <input type="radio" name="subtypeproperty" id="subtypeproperty4" autocomplete="off" value="4" >
                    Duplex
                </label>
            </div>
            <div class="col-sm-3 hidden" data-typeproperty="1">
                <label>
                    <input type="radio" name="subtypeproperty" id="subtypeproperty5" autocomplete="off" value="5" >
                    Ground floor
                </label>
            </div>
            <div class="col-sm-3 hidden" data-typeproperty="1">
                <label>
                    <input type="radio" name="subtypeproperty" id="subtypeproperty6" autocomplete="off" value="6" >
                    Room of service
                </label>
            </div>
            <div class="col-sm-3 hidden" data-typeproperty="12">
                <label>
                    <input type="radio" name="subtypeproperty" id="subtypeproperty17" autocomplete="off" value="17" >
                    Building plot
                </label>
            </div>
            <div class="col-sm-3 hidden" data-typeproperty="12">
                <label>
                    <input type="radio" name="subtypeproperty" id="subtypeproperty18" autocomplete="off" value="18" >
                    Non constructible land
                </label>
            </div>
            <div class="col-sm-3 hidden" data-typeproperty="7">
                <label>
                    <input type="radio" name="subtypeproperty" id="subtypeproperty7" autocomplete="off" value="7" >
                    House
                </label>
            </div>
            <div class="col-sm-3 hidden" data-typeproperty="7">
                <label>
                    <input type="radio" name="subtypeproperty" id="subtypeproperty15" autocomplete="off" value="15" >
                    Villa
                </label>
            </div>
            <div class="col-sm-3 hidden" data-typeproperty="7">
                <label>
                    <input type="radio" name="subtypeproperty" id="subtypeproperty8" autocomplete="off" value="8" >
                    Bastide
                </label>
            </div>
            <div class="col-sm-3 hidden" data-typeproperty="7">
                <label>
                    <input type="radio" name="subtypeproperty" id="subtypeproperty9" autocomplete="off" value="9" >
                    Castle
                </label>
            </div>
            <div class="col-sm-3 hidden" data-typeproperty="7">
                <label>
                    <input type="radio" name="subtypeproperty" id="subtypeproperty10" autocomplete="off" value="10" >
                    Closed
                </label>
            </div>
            <div class="col-sm-3 hidden" data-typeproperty="7">
                <label>
                    <input type="radio" name="subtypeproperty" id="subtypeproperty12" autocomplete="off" value="12" >
                    Ground
                </label>
            </div>
            <div class="col-sm-3 hidden" data-typeproperty="7">
                <label>
                    <input type="radio" name="subtypeproperty" id="subtypeproperty13" autocomplete="off" value="13" >
                    Property
                </label>
            </div>
            <div class="col-sm-3 hidden" data-typeproperty="7">
                <label>
                    <input type="radio" name="subtypeproperty" id="subtypeproperty14" autocomplete="off" value="14" >
                    Ground villa
                </label>
            </div>
            <div class="col-sm-3 hidden" data-typeproperty="7">
                <label>
                    <input type="radio" name="subtypeproperty" id="subtypeproperty16" autocomplete="off" value="16" >
                    Chalet
                </label>
            </div>
        </div>
    </div>
    <script>
        $(function () {
            $('input[name="subtypeproperty"]').change(function () {
                if ($(this).is(":checked")) {
                    if (typeof makeReference != "undefined") makeReference();
                }
            });
        });
    </script>
</div>                            
<div id="step4" class="step hidden">
    <div>
        <h4><span class="circle"></span> SPECIFY <strong>ADDITIONAL INFORMATION</strong></h4>
        <div class="row smallinput">

            <div class="col-sm-4 col-md-3 hidden" data-display-selector="[name='typeproperty'][value='1']:checked,[name='typeproperty'][value='5']:checked,[name='typeproperty'][value='7']:checked">
                <div class="form-group">
                    <div class="form-group-line">
                        <label class="control-label" for="numrooms">Num rooms</label>
                        <div class="input-elt input-elt-select ">
                            <select class="form-control" name="numrooms" id="numrooms" data-container="body" title="Number of rooms...">
                                <option value="" selected>Other / Not defined</option>
                                <option value="1" >Studio</option>
                                <option value="2" >2 rooms</option>
                                <option value="3" >3 rooms</option>
                                <option value="4" >4 rooms</option>
                                <option value="5" >5 rooms</option>
                                <option value="10" >+5 rooms</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-4 col-md-3 hidden" data-display-selector="[name='typeproperty'][value='1']:checked,[name='typeproperty'][value='7']:checked">

                <div class="form-group">
                    <div class="form-group-line">
                        <label class="control-label" for="numbedrooms">Num bedrooms</label>
                        <div class="input-elt input-elt-select ">
                            <select class="form-control" name="numbedrooms" id="numbedrooms" data-container="body" title="Number of bedrooms...">
                                <option value="" selected>None / Not defined</option>
                                <option value="1" >1 bedroom</option>
                                <option value="2" >2 bedrooms</option>
                                <option value="3" >3 bedrooms</option>
                                <option value="4" >4 bedrooms</option>
                                <option value="5" >5 bedrooms</option>
                                <option value="10" >+5 bedrooms</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-4 col-md-3 hidden" data-display-selector="[name='typeproperty'][value='1']:checked,[name='typeproperty'][value='7']:checked">
                <div class="form-group">
                    <div class="form-group-line">
                        <label class="control-label" for="numsbathrooms">Num bathrooms</label>
                        <div class="input-elt input-elt-select ">
                            <select class="form-control" name="numsbathrooms" id="numsbathrooms" data-container="body" title="Number of bathrooms...">
                                <option value="" selected>None / Not defined</option>
                                <option value="1" >1 bathroom</option>
                                <option value="2" >2 bathrooms</option>
                                <option value="3" >3 bathrooms</option>
                                <option value="4" >4 bathrooms</option>
                                <option value="5" >5 bathrooms</option>
                                <option value="10" >+5 bathrooms</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-4 col-md-3 hidden" data-display-selector="[name='typeproperty'][value='1']:checked,[name='typeproperty'][value='5']:checked,[name='typeproperty'][value='7']:checked,[name='typeproperty'][value='11']:checked">
                <div class="form-group">
                    <div class="form-group-line">
                        <label class="control-label" for="numparking">Num parkings</label>
                        <div class="input-elt input-elt-select ">
                            <select class="form-control" name="numparking" id="numparking" data-container="body" title="Number of parkings...">
                                <option value="" selected>None / Not defined</option>
                                <option value="1" >1 parking</option>
                                <option value="2" >2 parkings</option>
                                <option value="3" >3 parkings</option>
                                <option value="4" >4 parkings</option>
                                <option value="5" >5 parkings</option>
                                <option value="10" >+5 parkings</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-4 col-md-3 hidden" data-display-selector="[name='typeproperty'][value='1']:checked,[name='typeproperty'][value='5']:checked,[name='typeproperty'][value='7']:checked,[name='typeproperty'][value='11']:checked">
                <div class="form-group">

                    <div class="form-group-line">
                        <label class="control-label" for="numbox">Num box</label>
                        <div class="input-elt input-elt-select ">
                            <select class="form-control" name="numbox" id="numbox" data-container="body" title="Number of box...">
                                <option value="" selected>None / Not defined</option>
                                <option value="1" >1 garage</option>
                                <option value="2" >2 boxs</option>
                                <option value="3" >3 boxs</option>
                                <option value="4" >4 boxs</option>
                                <option value="5" >5 boxs</option>
                                <option value="10" >+5 boxs</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-4 col-md-3 hidden" data-display-selector="[name='typeproperty'][value='1']:checked,[name='typeproperty'][value='5']:checked,[name='typeproperty'][value='7']:checked,[name='typeproperty'][value='6']:checked">
                <div class="form-group">
                    <div class="form-group-line">
                        <label class="control-label" for="numcaves">Num cellars</label>
                        <div class="input-elt input-elt-select ">
                            <select class="form-control" name="numcaves" id="numcaves" data-container="body" title="Number of cellars...">
                                <option value="" selected>None / Not defined</option>
                                <option value="1" >1 cellar</option>
                                <option value="2" >2 cellars</option>
                                <option value="3" >3 cellars</option>
                                <option value="4" >4 cellars</option>
                                <option value="5" >5 cellars</option>
                                <option value="10" >+5 cellars</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(function () {
                $("#step4").on("checkAutoChanged", function () {
                    if ($("#step4 [data-display-selector]").not(".hidden").length>0) {

                    }
                    else {
                        $("#step4").addClass("hidden");
                    }
                });
                $("#numrooms").change(function () {
                    if (typeof makeReference != "undefined") makeReference();
                });
            });
        </script>
    </div>
</div>                            
<div id="step5" class="step hidden">
    <h4><span class="circle"></span> REFERENCE</h4>
    <div class="form-group  ">
        <div class="form-group-line">
            <div class="input-elt ">
                <div class="input-group">
                    <input id="reference" name="reference" required="required" class=" form-control" value="" type="text">
                </div>
            </div>
        </div>
    </div>

    <div id="help_5_1" class="help help5 hidden">
        <div class="popover fade bottom in" role="tooltip">
            <div class="arrow" style="left: 50%;"></div>
            <div class="popover-content"><p><span class="glyphicon glyphicon-info-sign"></span> Specify the product reference to create the property</p></div>
        </div>
    </div>

    <script>
        $(function () {
            $('#reference').change(function () {
                checkStep();
            }).keyup(function () {
                checkStep();
            }).change();
        });

    </script>
</div>
<div id="step6" class="step hidden">
    <p class="text-center">
        <input type="submit" value="Create the property" name="addpropertybtn" class="btn btn-primary" />
    </p>
</div>
</form>
<script>

    var checkStepTimer = false;
    function checkStep ()
    {
        if (checkStepTimer) window.clearTimeout(checkStepTimer);
        checkStepTimer = window.setTimeout("_checkStep()", 250);
    }
    function _checkStep ()
    {


        var country = $("#country").val();
        var building = $("#building").val();
        var city = $("#city").val();
        var district = $("#district").val();


        $(".help1").addClass("hidden");

        var step2=false;
        $("#help1_citynew").addClass("hidden");
        if (country==1) {
            if (building=="other" || building=="other") {
                step2 = district>0;
                $("#help1_building").toggleClass("hidden", step2);
            }
            else if (building=="new") {
                step2 = $("#building_other").val().length>0 && district>0;
                $("#help1_buildingnew").toggleClass("hidden", step2);
            }
            else {
                step2=building>0;
                $("#help1_building").toggleClass("hidden", step2);
            }
        }
        else if (country>0) {
            if ($("#manual").prop("checked")) {
                step2=true;
            }
            else if (city.length>0) {
                if (district=="new") {
                    step2=$("#district_autre").val().length>0;

                    $("#help1_districtnew").toggleClass("hidden", step2);
                }
                else if (district>0) {
                    step2=true;
                }
                else {
                    step2=true;
                }
            }
            else {
                step2=false;
                $("#help1_city").toggleClass("hidden", step2);
            }
        }
    //                            alert("endstep2");

    var typetransaction = $("input[name='typetransaction']:checked").val();
    var step3 = step2 && $("[data-typetransaction='"+typetransaction+"'] input[name='typeoffre']:checked").val()>0;

    var step4 = step3 && $("input[name='typeproperty']:checked").val()>0;

    var step5 = step4 && true;
    var step6 = step5 && $("#reference").val().length>1;


    $("#step1").toggleClass("validate", step2);
    $("#step2").toggleClass("hidden", !step2);
    $("#step2").toggleClass("validate", step3);

    $("#step3").toggleClass("hidden", !step3);
    $("#step3").toggleClass("validate", step4);

    $("#step4").toggleClass("hidden", !step4);
    $("#step4").toggleClass("validate", step4);

    $("#step5").toggleClass("hidden", !step5);
    $("#step5").toggleClass("validate", step6);
    $("#help_5_1").toggleClass("hidden", step6);


    $("#step6").toggleClass("hidden", !step6);

    $(".step").removeClass("bg-current");
    if (!step2) $("#step1").addClass("bg-current");
    else if (!step3) $("#step2").addClass("bg-current");
    else if (!step4) $("#step3").addClass("bg-current");
    else if (!step5) $("#step4").addClass("bg-current");
    else if (!step6) $("#step5").addClass("bg-current");
    _checkAuto();
}

var checkAutoTimer = 0;
function checkAuto ()
{
    //                            if (checkAutoTimer) window.clearTimeout(checkAutoTimer);
    //                            checkAutoTimer = window.setTimeout("_checkAuto()", 250);
}
function _checkAuto ()
{
    $("[data-display-selector]").each(function () {
        var selector = $(this).data("display-selector");

        var selectors = selector.split("&&");
        var toShow = true;
        for (var k in selectors) {

            selector = selectors[k];

            toShow = toShow && ($(selector).length>0);
            if (!toShow) break;
        }

        $(this).toggleClass("hidden", !toShow).trigger("checkAutoChanged");
    });
}
</script>
</div>
</div>
</div>
</div>
</div>
<?php include('footer.php');?>