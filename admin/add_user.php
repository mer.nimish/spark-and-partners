<?php
session_start();
require 'config/config.php';
require 'model/model.php';
global $conn;


if (isset($_POST['createuserbtn'])) {

    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $email = $_POST['email'];
    $password = password_hash($_POST['password'], PASSWORD_BCRYPT);
    $user_type = $_POST['user_type_id'];
    $user_refer_id = $_POST['user_refer_id'];

    $last_inserted_id = '';
    $last_inserted_id = addUser($first_name, $last_name, $email, $password, $user_type, $user_refer_id);

    if ($last_inserted_id > 0 && $last_inserted_id != '') {
        $message = '<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="zmdi zmdi-check pr-15 pull-left"></i><p class="pull-left">Yay! Your form has been sent successfully.</p> 
						<div class="clearfix"></div>
					</div>';
    } else {
        $message = '<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="zmdi zmdi-block pr-15 pull-left"></i><p class="pull-left">Opps! Somthing went wrong.</p>
						<div class="clearfix"></div>
					</div>';
    }
}
$allusertypeslist = getusertypes();
$allreferuserlist = getreferuserslist();
include('header.php');
include('left_sidebar.php');
?>

<div class="page-wrapper">
    <div class="container-fluid pt-25">	
        <div class="col-md-12">
            <?php if (isset($successMsg)) { ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <i class="zmdi zmdi-check pr-15 pull-left"></i><p class="pull-left"><?= $successMsg ?></p>
                    <div class="clearfix"></div>
                </div>
            <?php } ?>
            <?php if (isset($errorEmailMsg)) { ?>
                <div class="alert alert-warning alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <i class="zmdi zmdi-alert-circle-o pr-15 pull-left"></i><p class="pull-left"><?= $errorEmailMsg ?></p>
                    <div class="clearfix"></div>
                </div>
            <?php } ?>
            <?php if (isset($errorOtherMsg)) { ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <i class="zmdi zmdi-block pr-15 pull-left"></i><p class="pull-left"><?= $errorOtherMsg ?></p>
                    <div class="clearfix"></div>
                </div>
            <?php } ?>
        </div>

        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark">Create New User</h5>
            </div>
        </div>
        <!-- /Title -->

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark">User Form</h6>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-md-8">
                                    <?php echo $message; ?>
                                    <div class="form-wrap">
                                        <form data-toggle="validator" name="createuserform" id="createuserform" method="post" action="#" role="form">
                                            <div class="form-group">
                                                <label for="first_name" class="control-label mb-10">First Name</label>
                                                <input type="text" class="form-control" name="first_name" id="first_name" placeholder="First Name" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="LastName" class="control-label mb-10">Last Name</label>
                                                <input type="text" class="form-control" name="last_name" id="LastName" placeholder="Last Name" required>
                                            </div>

                                            <div class="form-group">
                                                <label for="email" class="control-label mb-10">Email</label>
                                                <input type="email" class="form-control" id="email" name="email" placeholder="Email" data-error="Bruh, that email address is invalid" required>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="password" class="control-label mb-10">Password</label>
                                                <div class="row">
                                                    <div class="form-group col-sm-12">
                                                        <input type="password" data-minlength="6" class="form-control" id="password" name="password" placeholder="Password" required>
                                                        <div class="help-block">Minimum of 6 characters</div>
                                                    </div>                                                    
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label mb-10">Select User Type</label>
                                                <select class="form-control select2" name="user_type_id" id="user_type" onchange="usertype(this);">
                                                    <?php
                                                    if (isset($allusertypeslist) && $allusertypeslist != 0) {
                                                    ?>
                                                      <option value="" selected disabled>Select User Type</option>     
                                                    <?php    
                                                        while ($usertypes = mysqli_fetch_assoc($allusertypeslist)) {
                                                            ?>
                                                            <option value="<?= $usertypes['user_type_id']; ?>"><?= $usertypes['user_type']; ?></option>                                                                
                                                            <?php
                                                        }
                                                    }
                                                    ?> 
                                                </select>
                                            </div>
                                            <div class="form-group referlist">
                                                <label class="control-label mb-10">Referred by</label>
                                                <select class="selectpicker userreferlist" data-style="form-control selectuserlist btn-default btn-outline" name="user_refer_id" disabled="">
                                                    <?php
                                                    if (isset($allreferuserlist) && $allreferuserlist != 0) {
                                                    ?>
                                                    <option value="" selected>Select Agent/Consultant</option>    
                                                    <?php   
                                                        while ($userlist = mysqli_fetch_assoc($allreferuserlist)) { 
                                                    ?>        
                                                            <option value="<?= $userlist['user_id']; ?>"><?= $userlist['first_name'].' '.$userlist['last_name']; ?></option>                                                                
                                                            <?php
                                                        }
                                                    }
                                                    ?> 
                                                </select>
                                            </div>
                                            <div class="form-group mb-0">
                                                <input type="submit" value="Submit" name="createuserbtn" id="createuserbtn" class="btn btn-success btn-anim">
                                            </div>                                            
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <?php include('footer.php'); ?>
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
        <script type="text/javascript">
            $('form[id="createuserform"]').validate({
                rules: {
                    email: {                                               
                        remote: {
                            url: "ajax/checkemail.php",
                            type: "post"
                        }
                    }
                },
                messages: {
                    email: {                                               
                        remote: "Email already registered please use other!!"
                    }
                },
                submitHandler: function (form) {
                    form.submit();
                }
            });

           function usertype(usertypetext){
                var selectedText = usertypetext.options[usertypetext.selectedIndex].innerHTML; 
                if(selectedText != 'Agent'){
                    $('.referlist').css("display","none");  
                }else{                    
                    $(".userreferlist").removeAttr("disabled");
                    $(".userreferlist").removeClass("disabled");
                    $(".selectuserlist").removeClass("disabled");
                    $('.referlist').css("display","block");                    
                }
           }            
        </script>