<?php 
session_start();
require 'config/config.php';
require 'model/model.php';
global $conn;

$allcountry = getAllCountry();
if (isset($_SESSION['first_name']) && isset($_SESSION['last_name'])) {
    $first_name = $_SESSION['first_name'];
    $last_name = $_SESSION['last_name'];
} else {
    echo "<script>window.location='index.php'</script>";
    exit;
}
$allproperty = allProperty();
//$mybuilding = myBuilding();

include('header.php');
include('left_sidebar.php');

?>
 
<div class="page-wrapper">
  <div class="container-fluid pt-25"> 
    <div  class="pills-struct">
      <ul role="tablist" class="nav nav-pills" id="myTabs_6">
        <li role="presentation"><a aria-expanded="true" role="tab" id="all_tab_1" href="view_public_all_property.php">All</a></li>
        <li role="presentation" class=""><a  id="my_sale_tab" role="tab" href="view_public_sale_property.php" aria-expanded="false">My Sale</a></li>
        <li role="presentation" class="active"><a  id="my_rent_tab" role="tab" href="view_public_rent_property.php" aria-expanded="false">My Rent</a></li>
        <li role="presentation" class="active">
        <a href="add_property.php" class="btn text-white btn-primary left-label"> <span class="btn-label"><i class="fa fa-plus"></i> </span><span class="btn-text">New Property</span></a>
        </li>
        <li role="presentation" class="" style="float: right;">
          <form method="POST" name="tempselectionform" class="temporary_selection" id="temporary_selection" action="view_temp_selection.php" target="_blank" >
            <span class="addselectiondata">
              <input type="hidden" name="publicval" value="1">
            </span>           

          <a href="javascript:{}" onclick="document.getElementById('temporary_selection').submit();" class="btn btn-default left-label"> 
            <span class="btn-label"><i class="fa fa-folder-open"></i> </span>
                <span class="btn-text">Temporary Selection <span class="label label-default pull-right" id="tempselection" dataval="0">0</span>
            </span>
          </a>
          </form>
        </li>
        
      </ul>
      <div class="tab-content" id="myTabContent_6">
        <div  id="all_tab" class="tab-pane fade active in" role="tabpanel">
         <div class="row">
           <div class="response_message" style="margin-top:20px;margin-bottom:5px; margin-left:0% "></div>
             <div class="col-sm-12">
                <div class="panel panel-default card-view">
                   <div class="panel-heading">
                      <div class="pull-left">
                         <h6 class="panel-title txt-dark">SEARCH IN MY AGENCY'S PROPERTY</h6>
                      </div>
                      <div class="clearfix"></div>
                   </div>

                   <div class="panel-wrapper collapse in">
                      <div class="panel-body">
                        <div id="searchEngine" class="collapse collapse-search m-t-10 in">
                          <form class="search_property" id="search_property" method="post" action="">
                            <div class="row">
                              <div class="col col-xs-12 col-sm-6 col-lg-4">
                                <div class="form-group mb-0">
                                  <label class="control-label mb-10">State of publication </label>
                                    <select multiple="multiple" class="selectpicker" name="statepublication[]" id="statepublication">
                                    <optgroup label="All Property">
                                      <option value="1" data-icon="fa fa-fw fa-file" >Draft</option>
                                      <option value="2" class="offmarket" data-icon="fa fa-fw fa-low-vision">Off Market</option>
                                      <option value="3" class="public" selected="selected" data-icon="fa fa-fw fa-globe">Public</option>
                                      <option value="4" class="archive" data-icon="fa fa-fw fa-archive">Archive</option>
                                    </optgroup>
                                  </select>
                                </div>
                              </div>
                              <div class="col col-xs-12 col-sm-6 col-lg-4">
                                <div class="form-group">
                                  <label class="control-label mb-10">Transaction Type</label>
                                  <select class="selectpicker" data-style="form-control btn-default btn-outline" name="transectiontype" id="transectiontype">
                                    <optgroup label="">
                                      <option value="">All</option>
                                    </optgroup>
                                    <optgroup label="Sale">
                                      <option value="1"   data-type_transaction="Sale">Sale</option>
                                      <option value="3"  data-type_transaction="Sale">New program</option>
                                      <option value="4"  data-type_transaction="Sale">Sale professional</option>
                                      <option value="2"  data-type_transaction="Sale">Life Time</option>
                                    </optgroup>
                                    <optgroup label="Location">
                                      <option value="5"  data-type_transaction="Location">Location</option>
                                      <option value="6"  data-type_transaction="Location">Location furnished</option>
                                      <option value="7"  data-type_transaction="Location">Location seasonal</option>
                                      <option value="8"  data-type_transaction="Location">Location professional</option>
                                    </optgroup>
                                  </select>
                                </div>
                              </div>  
                              <div class="col col-xs-12 col-sm-6 col-lg-4">
                                <div class="form-group mb-0">
                                  <label class="control-label mb-10">Property type</label>
                                  <select multiple name="propertytype" id="propertytype" data-placeholder="-" class="form-control ">
                                    <optgroup label="">
                                      <option value="">All</option>
                                    </optgroup>
                                    <optgroup label="Appartement">
                                      <option value="1" >Appartement</option>
                                      <option value="2" >Loft</option>
                                      <option value="3" >Penthouse/Roof</option>
                                      <option value="4" >Duplex</option>
                                      <option value="5" >Ground floor</option>
                                      <option value="6" >Room of service</option>
                                   </optgroup>
                                   <optgroup label="House">
                                      <option value="7" >House</option>
                                      <option value="15" >Villa</option>
                                      <option value="8" >Bastide</option>
                                      <option value="9" >castle</option>
                                      <option value="10" >Closed</option>
                                      <option value="12" >Mas</option>
                                      <option value="14" >Ground villa</option>
                                      <option value="13" >Property</option>
                                      <option value="16" >Chalet</option>
                                   </optgroup>
                                   <optgroup label="Building">
                                      <option value="17" >Building plot</option>
                                      <option value="18" >Non constructible land</option>
                                   </optgroup>
                                   <optgroup label="Parking / Garage / Box">
                                      <option value="22" >Parking</option>
                                      <option value="23" >Garage</option>
                                      <option value="24" >Box</option>
                                   </optgroup>
                                   <optgroup label="Office">
                                      <option value="19" >Office</option>
                                   </optgroup>
                                   <optgroup label="Cellar">
                                      <option value="20" >Cellar</option>
                                   </optgroup>
                                   <optgroup label="Local">
                                      <option value="21" >Local</option>
                                   </optgroup>
                                   <optgroup label="Commercial property">
                                      <option value="25" >Commercial property</option>
                                   </optgroup>
                                   <optgroup label="Commercial premises walls">
                                      <option value="27" >Commercial premises walls</option>
                                   </optgroup>
                                   <optgroup label="Assignments of right to the lease">
                                      <option value="26">Assignments of right to the lease</option>
                                   </optgroup>
                                   <optgroup label="Other property">
                                      <option value="28" >Other property</option>
                                      <option value="29" >Building</option>
                                   </optgroup>
                                  </select>
                                </div>
                               </div> 

                            </div>
                            <div class="row">
                              <div class="col col-xs-12 col-sm-6 col-lg-4">
                                <div class="form-group mb-0">
                                  <label class="control-label mb-10">Reference</label>
                                  <input type="text" id="reference" name="reference" class="form-control">
                                </div>
                               </div>
                               <div class="col col-xs-12 col-sm-6 col-lg-4">
                                <div class="form-group">
                                  <label for="" class="control-label mb-10">Country</label>
                                    <select class="form-control selectpicker" id="country" name="country" data-live-search="true">
                                      <option value="">All</option>
                                      <?php 
                                      if(isset($allcountry) && $allcountry != 0){
                                        while ($country = mysqli_fetch_assoc($allcountry)) { ?>
                                          <option value="<?=$country['country_id']?>"><?=$country['country_nicename']?></option>
                                      <?php
                                        }
                                       }?> 
                                    </select>
                                  
                                </div>
                              </div>
                              <div class="col col-xs-12 col-sm-6 col-lg-4">
                                <div class="form-group mb-0">
                                  <label class="control-label mb-10">No. Rooms </label>
                                    <select multiple title="All(8)" name="nubrooms" id="nubrooms" class="selectpicker">
                                      <option value="1" >Studio</option>
                                      <option value="2" >2 rooms</option>
                                      <option value="3" >3 rooms</option>
                                      <option value="4" >4 rooms</option>
                                      <option value="5" >5 rooms</option>
                                      <option value="10" >+5 rooms</option>
                                    
                                  </select>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-lg-4">
                                <div class="form-group mb-0">
                                  <label class="control-label mb-10 text-left">On-line</label>
                                  <input class="form-control input-daterange-datepicker" name="onlinedate" id="onlinedate" type="text" name="daterange" value="" />
                                </div>
                              </div>
                               <div class="col col-xs-12 col-sm-6 col-lg-4">
                                <div class="form-group">
                                  <div class="col-sm-6">
                                    <label class="control-label mb-10 text-left">Min Area</label>
                                    <input type="text" name="minarea" class="form-control" id="minarea" placeholder="min. (m²)">
                                  </div>
                                  <div class="col-sm-6">
                                    <label class="control-label mb-10 text-left">Max Area</label>
                                    <input type="text" name="maxarea" class="form-control" id="maxarea" placeholder="max. (m²)">
                                  </div>
                                </div>
                              </div>
                              <div class="col col-xs-12 col-sm-6 col-lg-4">
                                <div class="form-group mb-0">
                                  <label class="control-label mb-10">Other criteria</label>
                                    <select multiple title="-" name="othercriteria" id="othercriteria" class="selectpicker">
                                      <option value="mixeduse" >Mixed Use</option>
                                     <!--  <option value="terrace" >With terrace</option> -->
                                      <option value="parking" >With Parking</option>
                                      <option value="furniture" >Furniture</option>
                                      <!-- <option value="rentalpossible" >Student rental possible</option> -->
                                      <option value="interesting" >Interesting view</option>                             
                                      <!-- <optgroup label="UNDER LAWS:">
                                        <option value="underlaws" >Under laws</option>
                                        <option value="notunderlaws" >Not under laws</option>   
                                      </optgroup> -->
                                  </select>
                                </div>
                              </div>
                            </div>
                          </form>
                        </div>

                        <h6 class="property_title"><i class="fa fa-fw fa-th-list color-theme"></i>MY PROPERTY</h6>
                        <div class="row mt-20">
                          <div class="col-md-1 export-btn">                            
                            <div id="buttons"></div>
                          </div>
                          <div class="col-md-3 cstm-tbl"><span>Customize the table</span><a href="#customize_modal" data-toggle="modal"><span><i class="fa fa-fw fa-sliders"></i></span></a></div>
                          
                        </div>
                         <div class="table-wrap">
                            <div class="table-responsive">
                               <table id="all_tbl" class="display mb-30" >
                                  <thead>
                                     <tr>
                                        <th>UPD</th>
                                        <th>REF</th>
                                        <th>DESIGNATION</th>
                                        <th>LOCATION</th>
                                        <th>PROPERTY TYPE</th>
                                        <th>TRANSECTION TYPE</th>
                                        <th>PRICE/RENT</th>
                                        <th>DIFFUSION</th>
                                        <th>ROOMS</th>
                                        <th>PRICE/M<sup>2</sup></th>
                                        <th>INFOS</th>
                                        <th>CREATED DATE</th>
                                        <th>DATE ON-LINE</th>
                                        <th>FLOOR</th>
                                        <th>NO. OF IMAGES</th>
                                        <th>TOTAL AREA</th>
                                        <th>RETROCESSION</th>
                                        <th>ACTION</th>
                                                                                          
                                     </tr>
                                  </thead>
                                  <!-- Body part is called form ajx file (ajaxfile.php)    -->
                                 </table>
                            </div>
                         </div>
                      </div>
                   </div>
                </div>
             </div>
          </div>
        </div>
        
      </div>
    </div>
  <!-- Modal for delete option -->
    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                      <h5 class="modal-title" id="myModalLabel">CONFIRM</h5>
                  </div>
                  <div class="modal-body">
                      <h5 class="mb-15"></h5>                        
                      <p class="archievstatus"></p>
                      <p class="agentlist"></p>
                      <p class="agentcommission"></p>                       
                  </div>
                  <div class="modal-footer">
                      <button class="btn" data-dismiss="modal" aria-hidden="true">No</button>
                      <button class="btn btn-primary yes" data-dismiss="modal" property_id="" id="">Yes</button>
                  </div>                    
              </div>
              <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
      </div>

      <!-- Customize the table modal-->
      <div id="customize_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                      <!-- <h5 class="modal-title" id="myModalLabel">CONFIRM</h5> -->
                  </div>
                  <div class="modal-body">
                       <div>
                        <label class="btn-block" for="updated_at">
                           <input type="checkbox" id="updated_at" checked data-column="0" class="form-control toggle-vis columns"> Updated at
                        </label>
                        <label class="btn-block" for="ref">
                           <input type="checkbox" checked id="ref" data-column="1" class="toggle-vis form-control columns"> Referance
                        </label>
                        <label class="btn-block" for="designation">
                           <input type="checkbox" checked id="designation" data-column="2" class="toggle-vis form-control columns"> Designation
                        </label>
                        <label class="btn-block" for="location_check">
                           <input type="checkbox" checked id="location_check" data-column="3" class="toggle-vis form-control columns"> Location
                        </label>
                        <label class="btn-block" for="property_type_check">
                           <input type="checkbox" checked id="property_type_check" data-column="4" class="toggle-vis form-control columns"> Property Type
                        </label>
                        <label class="btn-block" for="transaction_type_check">
                           <input type="checkbox" checked id="transaction_type_check" data-column="5" class="toggle-vis form-control columns"> Transection Type
                        </label>
                        <label class="btn-block" for="price_check">
                           <input type="checkbox" checked id="price_check" data-column="6" class="toggle-vis form-control columns"> price
                        </label>
                        <label class="btn-block" for="difusion_check">
                           <input type="checkbox" checked id="difusion_check" data-column="7" class="toggle-vis form-control columns">Diffusion
                        </label>
                        <label class="btn-block" for="num_rooms_check">
                           <input type="checkbox" checked id="num_rooms_check" data-column="8" class="toggle-vis form-control columns"> No. of rooms
                        </label>
                        <label class="btn-block" for="price_m_check">
                           <input type="checkbox" checked  id="price_m_check" data-column="9" class="toggle-vis form-control columns"> Price/m2
                        </label>
                        <label class="btn-block" for="info_check">
                           <input type="checkbox"  id="info_check" data-column="10" class="toggle-vis form-control columns"> Infos
                        </label>
                         
                        <label class="btn-block" for="creat_date_check">
                           <input type="checkbox"  id="creat_date_check" data-column="11" class="toggle-vis form-control columns"> Created at
                        </label>
                        <label class="btn-block" for="online_date_check">
                           <input type="checkbox"  id="online_date_check" data-column="12" class="toggle-vis form-control columns"> On-line date
                        </label>
                        <label class="btn-block" for="floor_check">
                           <input type="checkbox"  id="floor_check" data-column="13" class="toggle-vis form-control columns"> Floor
                        </label>
                        <label class="btn-block" for="images_check">
                           <input type="checkbox"  id="images_check" data-column="14" class="toggle-vis form-control columns"> No. of images
                        </label>
                        <label class="btn-block" for="totalarea_check">
                           <input type="checkbox"  id="totalarea_check" data-column="15" class="toggle-vis form-control columns"> Total Area
                        </label>
                        <label class="btn-block" for="retro_check">
                           <input type="checkbox"  id="retro_check" data-column="16 " class="toggle-vis form-control columns"> retrocession
                        </label>
                        </div>                       
                  </div>
                                    
              </div>
              <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
      </div>

<script>
  
   $(document).ready(function() {
  
      var dataTable = $('#all_tbl').DataTable({
          sDom: '<lf<t>ip>',
          'processing': true,
          'serverSide': true,
          //'stateSave': true,
          'serverMethod': 'post',
          'searching': false, // Remove default Search Control
         
          'ajax': {
              'url':'ajax/ajaxpublicrent.php',
              'data': function(data){
                  // Read values
                  
                  var transectiontype = $('#transectiontype').val();
                  var propertytype = [];
                  var propertytype = $('#propertytype').val();
                  var statepublication = [];
                  var statepublication = $('#statepublication').val();
                  var reference = $('#reference').val();
                  var country = $('#country').val();
                  var nubrooms = [];
                  var nubrooms = $('#nubrooms').val();
                  var startdate = $('input[name=daterangepicker_start]').val();
                  var enddate = $('input[name=daterangepicker_end]').val();
                  var minarea = $('#minarea').val();
                  var maxarea = $('#maxarea').val();
                  var othercriteria = [];
                  var othercriteria = $('#othercriteria').val();
                 
                  // Append to data
                  data.transectionType = transectiontype;
                  data.proType = propertytype;
                  data.statePublication = statepublication;
                  data.reference = reference;
                  data.country = country;
                  data.nubRooms = nubrooms;
                  data.startDate = startdate;
                  data.endDate = enddate;
                  data.minArea = minarea;
                  data.maxArea = maxarea;
                  data.otherCriteria = othercriteria;
              }
          },
              'columns': [
              { data: 'updated_at' },
              { data: 'reference' },
              { data: 'property_title' },
              { data: 'building_id'},
              { data: 'property_type' },
              { data: 'transaction_type' },              
              { data: 'price' },
              { data: 'diffusion' },
              { data: 'num_rooms' },
              { data: 'monthly_rent' }, // actully it is a price/m2.
              { "visible": false,data: 'property_title' },
              { "visible": false,data: 'created_at' },
              { "visible": false,data: 'release_date' },
              { "visible": false,data: 'floor' },
              { "visible": false,data: 'photo_id' },
              { "visible": false,data: 'total_area' },
              { "visible": false,data: 'retrocession' },
              { data: 'action' },

          ]
          
      });
      new $.fn.dataTable.Buttons( dataTable, {
           buttons: [
          {
            extend: 'excel',
            text: 'Export',
            className: 'exportExcel',
            filename: 'publicrentproperty',
            exportOptions: {
              modifier: {
                page: 'all'
              }
            }
          }]
      } ).container().appendTo($('#buttons'));
      $(document).on('change', '.toggle-vis:checkbox', function() {
        //alert("hi");
        var column = dataTable.column( $(this).attr('data-column') ); // Get the column API object
        column.visible( ! column.visible() );  // Toggle the visibility
       });  

      $('#searchEngine input[type=text]').keyup(function(){
          dataTable.draw();
      });

      $('#searchEngine select').change(function(){
          dataTable.draw();
      });
      $(document).on('click', '.applyBtn', function(event) {
         dataTable.draw(); 
      });
  
    // $( '#my_sale_tbl' ).DataTable( );
    // $( '#my_rent_tbl' ).DataTable( );
   
 
  
    /** Delete a building*/
    $(document).delegate('.ajax', 'click', function () {
        $('.agentlist').html('');
        $('.agentcommission').html('');
        $('.yes').attr('id',$(this).attr('data-id'));
        $('.yes').attr('property_id', $(this).attr('property_id'));
        var propstatus = $(this).attr('data-id');
        if(propstatus == 'property_archive'){
          $('.archievstatus').html('<select name="property_status" class="property_status form-control select2"><option value="" selected disabled>Please select status</option><option value="SOLD">Sold</option><option value="NO LONGER AVAILABLE">No Longer Available</option></select>');
        }
        $('.modal-body h5').text($(this).attr('data-confirm'));
    });

    $(document).on('click', '#property_offmarket', function(event) {
        event.preventDefault();
        var property_id = $(this).attr('property_id');
        var data = {
            "action": "offmarket_property",
            "property_id": property_id
        };
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "ajax/offmarket_property.php", //Relative or absolute path to response.php file
            data: data,
            success: function (data) {
                $(".response_message").html(
                        data["message"]
                        );
                if (data["success"] === 'true') {
                    $('#row_' + property_id).remove();
                }
            }
        });
    });

    $(document).on('click', '#property_public', function(event) {
        event.preventDefault();
        var property_id = $(this).attr('property_id');
        var data = {
            "action": "public_property",
            "property_id": property_id
        };
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "ajax/public_property.php", //Relative or absolute path to response.php file
            data: data,
            success: function (data) {
                $(".response_message").html(
                        data["message"]
                        );
                if (data["success"] === 'true') {
                    $('#row_' + property_id).remove();
                }
            }
        });
    });
    /*$(document).on('click', '#property_archive', function(event) {
        event.preventDefault();
        var property_id = $(this).attr('property_id');
        var data = {
            "action": "archive_property",
            "property_id": property_id
        };
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "ajax/archive_property.php", //Relative or absolute path to response.php file
            data: data,
            success: function (data) {
                $(".response_message").html(
                        data["message"]
                        );
                if (data["success"] === 'true') {
                    $('#row_' + property_id).remove();
                }
            }
        });
    });*/

    $(document).on('click', '#property_delete', function(event) {
        event.preventDefault();
        var property_id = $(this).attr('property_id');
        var data = {
            "action": "delete_property",
            "property_id": property_id
        };
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "ajax/delete_property.php", //Relative or absolute path to response.php file
            data: data,
            success: function (data) {
                $(".response_message").html(
                        data["message"]
                        );
                if (data["success"] === 'true') {
                    $('#row_' + property_id).remove();
                }
            }
        });
    });
    
    //$("#formsearchproduit").serialize()
});


</script>

<script src="dist/js/custom.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    property_selection();
    property_sold();
    property_status(); 
    property_sold_commission();   
});
</script>
<script type="text/javascript">
    jQuery(function ($) {
      $.getScript('https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js', function () {
          $("input[type='checkbox']").checkboxradio();
        });
      });
    
 </script>

<style type="text/css">
  .multiselect {
  text-align: left;
}
.multiselect b.caret {
  position: absolute;
  top: 20px;
  right: 13px;
} 
.multiselect-container{
  width: 100%;
}
</style>
<?php include('footer.php');?>