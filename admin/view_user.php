<?php
session_start();
require 'config/config.php';
require 'model/model.php';
global $conn;

include('header.php');
include('left_sidebar.php');
$userslist = getuserslist();
?>

<div class="page-wrapper">
    <div class="container-fluid pt-25">	        
        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark">View User List</h5>
            </div>
        </div>
        <!-- /Title -->

        <div class="row">
            <div class="col-sm-12">
                <div class="response_message" style="margin-top:20px;margin-bottom:5px; margin-left:0% "></div>
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark">Users Data</h6>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="table-wrap">
                                <div class="">
                                    <table id="myTable1" class="table table-hover display pb-30" >
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>User Type</th>
                                                <th>Created Date</th>
                                                <th>Action</th>                                                
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php
                                            if (!empty($userslist)) {
                                                while ($alluserlist = mysqli_fetch_assoc($userslist)) {
                                                    ?>
                                                    <tr id="row_<?=$alluserlist["user_id"]?>">
                                                        <td><?= $alluserlist['first_name'] . ' ' . $alluserlist['last_name'] ?></td>
                                                        <td><?= $alluserlist['email'] ?></td>
                                                        <td><?= $alluserlist['user_type'] ?></td>
                                                        <td><?= date("d/m/Y", strtotime($alluserlist['created_at'])); ?></td>                                                        
                                                        <td class="text-nowrap">
                                                            <?php if ($alluserlist['user_type'] != 'Super Admin') { ?>
                                                                <!--<a href="#" class="mr-25" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>--> 
                                                                <a href="#myModal" data-toggle="modal" data-original-title="Remove" user_id="<?php echo $alluserlist['user_id']; ?>" class="removeuser"> <i class="fa fa-close text-danger"></i> </a> 
                                                           <?php } ?>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </tbody>                                       
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>	
            </div>
        </div>
        <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h5 class="modal-title" id="myModalLabel">Confirm</h5>
                    </div>
                    <div class="modal-body">
                        <h5 class="mb-15">Are you sure want to delete this user?</h5>                        
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">No</button>
                        <button class="btn btn-primary" data-dismiss="modal" user_id="" id="delete_yes">Yes</button>
                    </div>                    
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>


        <?php include('footer.php'); ?>
        <script>
            $(document).ready(function () {

                $(document).delegate('.removeuser', 'click', function () {
                    $('#delete_yes').attr('user_id', $(this).attr('user_id'));
                });

                $('#delete_yes').on('click', function ()
                {
                    var user_id = $(this).attr('user_id');
                    var data = {
                        "action": "delete_user",
                        "user_id": user_id
                    };
                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: "ajax/delete_user.php", //Relative or absolute path to response.php file
                        data: data,
                        success: function (data) {
                            $(".response_message").html(
                                    data["message"]
                                    );
                            if (data["success"] === 'true') {
                                $('#row_' + user_id).remove();
                            }
                        }
                    });
                });
            });
        </script> 