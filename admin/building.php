<?php 
session_start();
require 'config/config.php';
require 'model/model.php';
global $conn;


if (isset($_SESSION['first_name']) && isset($_SESSION['last_name'])) {
	$first_name = $_SESSION['first_name'];
	$last_name = $_SESSION['last_name'];
} else {
	echo "<script>window.location='index.php'</script>";
	exit;
} 

$singel_building = singelBuilding($_REQUEST['building_id']);
$rent_building = getrentpropertyofbuilding($_REQUEST['building_id']);
$sale_building = getsalepropertyofbuilding($_REQUEST['building_id']);

include('header.php');
include('left_sidebar.php');

?>

<div class="page-wrapper">
	<div class="container-fluid pt-25">	
		
		<!-- Title -->
		<div class="row heading-bg">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				<h5 class="txt-dark"><?php echo $singel_building['building_name'];?></h5>
			</div>
		</div>
		<!-- /Title -->
		
		<!-- Row -->
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default card-view building">
					
					<div class="panel-wrapper collapse in">
						<div class="panel-body">    
							
							<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCGmRiNH9b_Ts8meG7fgcH2rhUYy7-FCqY&libraries=places&language=en"></script>

							<div class="row">
								<div class="col-md-6">
									<h5><i class="fa fa-building"></i> Building details <small>#<?php echo $singel_building['building_id'];?></small></h5>
									<div class="building-group">
										<div class="building-detail">
											<div class="item">Country</div>
											<?php $country_name =  countryName($singel_building['country']);?>
											<div class="value"><?php echo $country_name['country_nicename'];?></div>
										</div>
									</div>
									<div class="building-group">
										<div class="building-detail">
											<div class="item">City</div>
											<?php $city_name =  cityName($singel_building['city']);?>
											<div class="value"><?php echo $city_name['city_name'];?></div>
										</div>
									</div>
									<div class="building-group">
										<div class="building-detail">
											<div class="item">District</div>
											<?php $district_name =  districtName($singel_building['district']);?>
											<div class="value"><?php echo $district_name['district_name'];?></div>
										</div>
									</div>
									<div class="building-group">
										<div class="building-detail">
											<div class="item">Address</div>
											<div class="value" id="address"><?php echo $singel_building['address'];?></div>
										</div>
									</div>
									<hr>
									<h5><i class="fa fa-search"></i> Properties in this building</h5>
									<div class="btn-group btn-group-sm btn-group-justified buildingsaleandrentdata">
										<a href="building_sale_property.php?bid=<?=$_REQUEST['building_id']?>" target="_blank" class="btn sale" data-popup="1">Sales <span class="badge"><?=$sale_building?></span></a>
										<a href="building_rent_property.php?bid=<?=$_REQUEST['building_id']?>" target="_blank" class="btn rent popup-xl" data-popup="1">Rentals <span class="badge"><?=$rent_building?></span></a>
									</div>
								</div>


								<div class="col-md-6">
									<div class="row">
										<div class="padding-0">
											<div id="map" style="width: 100%; height: 300px;"></div> 
										</div>
									</div>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group  ">
												<div class="form-group-line">
													<label class="control-label" for="latitude">Latitude</label>
													<div class="input-elt ">
														<div class="input-group"><input type="text" id="latitude" name="latitude" readonly="readonly" readonly="readonly" value="<?php echo $singel_building['latitude'];?>" class=" form-control" /></div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group  ">
												<div class="form-group-line">
													<label class="control-label" for="longitude">Longitude</label>
													<div class="input-elt ">
														<div class="input-group"><input type="text" id="longitude" name="longitude" readonly="readonly" value="<?php echo $singel_building['longitude'];?>" readonly="readonly" class=" form-control" /></div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<hr>
							<a href="/image/newmult/?immeuble=773" class="btn btn-sm btn-primary popup-sxl pull-right" data-toggle="modal" data-target="#modal"><i class="fa fa-plus"></i> Add Image(s)...</a>
							<h5><i class="fa fa-image"></i> Pictures <span class="badge">0</span></h5> 
							<div class="clearfix"></div>
							<div class="alert alert-warning fade in m-t-0">
								<p>
									No picture for this building
								</p>
							</div>                    
						</div>
					</div>
				</div>
			</div>
		</div>

		<script type="text/javascript">
			
			function initialize() {

				var lat = $('#latitude').val();
				var lang = $('#longitude').val();
				var address = $('#address').text();

				if(lat == '' && lang == ''){
					var latlng = new google.maps.LatLng(43.7313393,7.4140294);
				}
				else{
					var latlng = new google.maps.LatLng(lat,lang);
				}
				var map = new google.maps.Map(document.getElementById('map'), {
					center: latlng,
					zoom: 13
				});
				var marker = new google.maps.Marker({
					map: map,
					position: latlng,
					icon: "/sparkandpartners/img/placeholder.png",
					draggable: false,
					anchorPoint: new google.maps.Point(0, -29)
				});
				var infowindow = new google.maps.InfoWindow();   
				google.maps.event.addListener(marker, 'click', function() {
					var iwContent = "<div id='iw_container'>" +
					"<div class='iw_title'><b>Location</b> : "+address+" </div></div>";
          // including content to the infowindow
          infowindow.setContent(iwContent);
          // opening the infowindow in the current map and at the current marker location
          infowindow.open(map, marker);
      });
			}
			google.maps.event.addDomListener(window, 'load', initialize);
			jQuery(function ($) {
				
				$("input[type='checkbox']").checkboxradio();
				
			});
		</script>

		<?php include('footer.php');?>