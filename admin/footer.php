<!-- Footer -->
			<footer class="footer container-fluid pl-30 pr-30">
				<div class="row">
					<div class="col-sm-12">
						<p>2018 &copy; Spark And Partners.</p>
					</div>
				</div>
			</footer>
			<!-- /Footer -->
			
		</div>
        <!-- /Main Content -->

    </div>
    <!-- /#wrapper -->
	
	<!-- JavaScript -->
	
    <!-- jQuery -->
    <script src="../vendors/bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script>


    <!-- Bootstrap Core JavaScript -->
    <script src="../vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    
    <!-- Dropzone JavaScript -->
<script src="../vendors/bower_components/dropzone/dist/dropzone.js"></script>

<!-- Dropzone Init JavaScript -->
<script src="dist/js/dropzone-data.js"></script>

    <!-- bootstrap-datepicker -->
   <!--  <script src="datepicker/bootstrap-datepicker.js"></script> -->
   <!-- Select2 JavaScript -->
	<script src="../vendors/bower_components/select2/dist/js/select2.full.min.js"></script>
	<script type="text/javascript" src="dist/js/bootstrap-multiselect.js"></script>
	<!-- Data table JavaScript -->
	<script src="dist/js/dataTables-data.js"></script>              
    <script src="../vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="dist/js/responsive-datatable-data.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
	<script src="../vendors/bower_components/jszip/dist/jszip.min.js"></script>
 
	<script src="../vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
	
	<!-- Slimscroll JavaScript -->
	<script src="dist/js/jquery.slimscroll.js"></script>

	<!-- Bootstrap Daterangepicker JavaScript -->
	<script src="../vendors/bower_components/dropify/dist/js/dropify.min.js"></script>

	<!-- Moment JavaScript -->
	<script type="text/javascript" src="../vendors/bower_components/moment/min/moment-with-locales.min.js"></script>
	
	<!-- Switchery JavaScript -->
    <script src="../vendors/bower_components/switchery/dist/switchery.min.js"></script>

	<!-- Progressbar Animation JavaScript -->
	<script src="../vendors/bower_components/waypoints/lib/jquery.waypoints.min.js"></script>
	<script src="../vendors/bower_components/jquery.counterup/jquery.counterup.min.js"></script>
	
	<!-- Fancy Dropdown JS -->
	<script src="dist/js/dropdown-bootstrap-extended.js"></script>

	<!-- Form Flie Upload Data JavaScript -->
	<script src="dist/js/form-file-upload-data.js"></script>

    <!-- Bootstrap Select JavaScript -->
    <script src="../vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

	<!-- Sparkline JavaScript -->
	<script src="../vendors/jquery.sparkline/dist/jquery.sparkline.min.js"></script>
	
	<!-- Owl JavaScript -->
	<script src="../vendors/bower_components/owl.carousel/dist/owl.carousel.min.js"></script>
	
	<!-- Switchery JavaScript -->
	<script src="../vendors/bower_components/switchery/dist/switchery.min.js"></script>
	
	<!-- EChartJS JavaScript -->
	<script src="../vendors/bower_components/echarts/dist/echarts-en.min.js"></script>
	<script src="../vendors/echarts-liquidfill.min.js"></script>
	
	<!-- Toast JavaScript -->
	<script src="../vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>

    <script src="../vendors/bower_components/bootstrap-validator/dist/validator.min.js"></script>

    <script type="text/javascript" src="../vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

    <!-- Bootstrap Daterangepicker JavaScript -->
	<script src="../vendors/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
	<!-- Form Picker Init JavaScript -->
	<script src="dist/js/form-picker-data.js"></script>

	<!-- Init JavaScript -->
	<script src="dist/js/init.js"></script>
	<script src="dist/js/dashboard-data.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
    	$('#propertytype').multiselect({
    		enableFiltering: true,
    		buttonWidth: '100%'
    		// nonSelectedText:'-'
    	});
	});

	function setupCity ()
        {
            if (!$("#country").val()) {
                $("#city").closest(".form-group").hide();
                $("#city").val("").selectpicker("render").change();
            }
            else {
                //if ($("#city").attr("data-pays") == $("#country").val()) {
                  //alert("hi");
                      var id = $("#country").val();
                        $.ajax({
                          method: 'post',
                          url: 'ajax/getcity.php',
                          data: {id: id}
                        }) .done(function(data){
              //console.log(data);
                        var val = $("#city").val();
                        $("#city").attr("data-pays",$("#country").val()).find("option").remove();

                        if (!data) {
                            $("#city").closest(".form-group").hide();
                        }
                        else {
                            var cpt= 0,curval=false;
                            $.each( JSON.parse(data), function( k, v ) {
                                cpt++;
                                var curval= v.city_id;
                                $("#city").append("<option value='"+(v.city_id>0?v.city_id:"")+"'>"+ v.city_name+"</option>");
                            });
                            if ($("#localisation #country").val() != 141) {
                                $("#city").append("<option value=''>Other</option>");
                                $("#city").append("<option value='' class='new'>[+] New city</option>");
                            }
                            else if (!val && cpt==1 && curval) {
                                val = curval;
                            }
                            $("#city").selectpicker("refresh");
                            $("#city").closest(".form-group").show();
                        }
                        $("#city").val(val?val:"").selectpicker("render").change();
                    });
                // }
                // else {
                //     //debugloc&&alert('setupVilles : liste déjà à jour');
                //     $("#city").closest(".form-group").show();
                //     $("#city").change();
                // }
            }
        }
        function setupDistrict()
        {
            //debugloc&&alert("setupQuartiers : "+$("#city").val());
            if (!$("#city").val()) {
                //debugloc&&alert('setupQuartiers : no parent');
                $("#district").closest(".form-group").hide();
                $("#district").val("").selectpicker("render").change();
            }
            else {
                //if ($("#district").attr("data-ville") != $("#city").val()) {
                    var id = $('#city').val();
                    $.ajax({
                        method: 'post',
                        url: 'ajax/getdistrict.php',
                        data: {id: id}
                      }) .done(function(data) {
                                  var val = $("#district").val();
                        $("#district").attr("data-ville",$("#city").val()).find("option").remove();

                        if (!data) {
                            //debugloc&&alert('no quartier...');
                            $("#district").closest(".form-group").hide();
                        }
                        else {
                            var cpt= 0,curval=false;
                            $.each( JSON.parse(data), function( k, v ) {
                                cpt++;
                                curval=v.district_id;
                                $("#district").append("<option value='"+(v.district_id>0?v.district_id:"")+"'>"+ v.district_name+"</option>");
                            });

                            if ($("#localisation #country").val() != 1) {
                                $("#district").append("<option value=''>Other</option>");
                                if ($("#localisation #city").val() > 0) {
                                    $("#district").append("<option value='' class='new'>[+] New district</option>");
                                }
                            }
                            else if (!val && cpt==1 && curval) {
                                val = curval;
                            }
                            $("#district").selectpicker("refresh");
                            $("#district").closest(".form-group").show();
                        }
                        if ($("#localisation #country").val() != 1) {
                            $("#district").append("<option value=''>Autre</option>");
                        }

                        $("#district").val(val?val:"").selectpicker("render").change();
                    });
               // }
                // else {
                //     debugloc&&alert('setupQuartiers : liste déjà à jour');
                //     $("#district").closest(".form-group").show();
                //     $("#district").change();
                // }
            }
        }
	</script>
</body>

</html>
