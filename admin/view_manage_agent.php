<?php
session_start();
require 'config/config.php';
require 'config/constant.php';
require 'model/model.php';
global $conn;


if (isset($_SESSION['first_name']) && isset($_SESSION['last_name']) && $_SESSION['add_contact'] == '1') {
    $first_name = $_SESSION['first_name'];
    $last_name = $_SESSION['last_name'];
} else {
    echo "<script>window.location='index.php'</script>";
    exit;
}
$viewagents = viewallagents();


include('header.php');
include('left_sidebar.php');
?>

<div class="page-wrapper">
    <div class="container-fluid pt-25"> 
        <div  class="pills-struct">            
            <div class="tab-content" id="myTabContent_6">
                <div id="contact_list" class="tab-pane fade active in" role="tabpanel">
                    <div class="row">
                        <div class="response_message" style="margin-top:20px;margin-bottom:5px; margin-left:0% "></div>
                        <div class="col-sm-12">
                            <div class="panel panel-default card-view">
                                <div class="panel-heading">
                                    <div class="pull-left">
                                        <h6 class="panel-title txt-dark">Manage Agents</h6>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body">
                                        <div class="table-wrap">
                                            <div class="table-responsive">
                                                <table id="agent_list_tbl" class="table table-hover display  pb-30" >
                                                    <thead>
                                                        <tr>
                                                            <th width="20%">Agent Name</th>
                                                            <th width="20%">Created On</th>                                  
                                                            <th width="10%">#Mandate</th>
                                                            <th width="10%">#Deals</th>                                 
                                                            <th width="20%">Commission Genrated</th>
                                                            <th width="10%">Agents Recruited</th>                       
                                                            <th width="10%">Action</th>
                                                        </tr>
                                                    </thead>                                                     
                                                    <tbody>
                                                        <?php
                                                        if (isset($viewagents) && $viewagents != 0) {
                                                            foreach ($viewagents as $val) {
                                                                ?>
                                                                <tr id="row_<?=$val['user_id']?>">
                                                                    <td><?php echo ucfirst($val['first_name']) . ' ' . ucfirst($val['last_name']); ?></td>                                                           
                                                                    <td>
                                                                    <?php 
                                                                    $dt = new DateTime($val['created_at']);
                                                                    $date = $dt->format('m/d/Y');
                                                                    echo $date; 
                                                                    ?>
                                                                    </td>
                                                                    <td>
                                                                    <?php
                                                                    if($val['numberofmandate'] != ''){
                                                                        echo $val['numberofmandate'];
                                                                    }else{
                                                                        echo "-";
                                                                    } 
                                                                    ?>
                                                                    </td>
                                                                    <td>
                                                                    <?php
                                                                    if($val['numberofdeals'] != ''){
                                                                        echo $val['numberofdeals'];
                                                                    }else{
                                                                        echo "-";
                                                                    } 
                                                                    ?>
                                                                   </td>
                                                                    <td>
                                                                    <?php 
                                                                    if($val['commission_amount'] != ''){
                                                                        echo $val['commission_amount'];
                                                                    }else{
                                                                        echo "-";
                                                                    }    
                                                                    ?>
                                                                    </td>
                                                                    <td>
                                                                    <?php 
                                                                    if($val['userrefer'] != ''){
                                                                        echo $val['userrefer'];
                                                                    }else{
                                                                        echo "-";
                                                                    }    
                                                                    ?>
                                                                    </td>    
                                                                    <td>                                                            
                                                                        <a href="#" class="" data-toggle="tooltip" data-original-title="Edit"><button class="btn btn-default btn-icon-anim btn-square"> <i class="fa fa-pencil text-inverse m-r-10"></i></button> </a>

                                                                         <a href="#myModal" data-toggle="modal" data-original-title="Remove" user_id="<?php echo $val['user_id']; ?>" class="mr-25 removeuser"><button class="btn btn-default btn-icon-anim btn-square"> <i class="fa fa-close text-danger text-inverse m-r-10"></i></button></a>  
                                                                    </td>

                                                                </tr>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </tbody>                                                     
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                

            </div>
        </div>
        
        <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h5 class="modal-title" id="myModalLabel">Confirm</h5>
                    </div>
                    <div class="modal-body">
                        <h5 class="mb-15">Are you sure want to delete this Agent?</h5>                        
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">No</button>
                        <button class="btn btn-primary" data-dismiss="modal" user_id="" id="delete_yes">Yes</button>
                    </div>                    
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <script>
            // Main reference for data-table           
            $(document).ready(function () {
                var table = $('#agent_list_tbl').DataTable( );
                var $dt_head = $('#agent_list_tbl').find('thead').find('td');

                $dt_head.each(function () {
                    var title = $dt_head.eq($(this).index()).text();

                    if (title != 'Action') {
                        $(this).html($('<input/>', {'type': 'text', 'placeholder': 'Search ' + title}).addClass('form-control input-xs table-column-search width-full p-0'));
                    }
                });

                // Apply column filters at footer
                $("#agent_list_tbl  thead input").on('keyup change', function () {
                    table
                            .column($(this).parent().index() + ':visible')
                            .search(this.value)
                            .draw();
                });
            });

        </script>

        <style type="text/css">
            .dataTables_scroll{position:relative}
            .dataTables_scrollHead{margin-bottom:40px;}
            .dataTables_scrollFoot{position:absolute; top:38px}
        </style>
<?php include('footer.php'); ?>
<script>
            $(document).ready(function () {

                $(document).delegate('.removeuser', 'click', function () {
                    $('#delete_yes').attr('user_id', $(this).attr('user_id'));
                });

                $('#delete_yes').on('click', function ()
                {
                    var user_id = $(this).attr('user_id');
                    var data = {
                        "action": "delete_user",
                        "user_id": user_id
                    };
                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: "ajax/delete_user.php", //Relative or absolute path to response.php file
                        data: data,
                        success: function (data) {
                            $(".response_message").html(
                                    data["message"]
                                    );
                            if (data["success"] === 'true') {
                                $('#row_' + user_id).remove();
                            }
                        }
                    });
                });
            });
        </script> 