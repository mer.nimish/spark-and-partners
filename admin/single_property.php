<?php 
session_start();
require 'config/config.php';
require 'model/model.php';
global $conn;


if (isset($_SESSION['first_name']) && isset($_SESSION['last_name'])) {
    $first_name = $_SESSION['first_name'];
    $last_name = $_SESSION['last_name'];
} else {
    echo "<script>window.location='index.php'</script>";
    exit;
} 

$single_property = singleProperty($_REQUEST['propertyid']);

include('header.php');
include('left_sidebar.php');

?>
 
<div class="page-wrapper">
  <div class="container-fluid pt-25">	
    <div  class="pills-struct">            
			<ul role="tablist" class="nav nav-pills" id="myTabs_6">
	      <li class="active" role="presentation"><a aria-expanded="true"  data-toggle="tab" role="tab" id="general_tab" href="#general">General</a></li>
	      <li role="presentation" class=""><a  data-toggle="tab" id="photo_id" role="tab" href="#photos" aria-expanded="false">Photos</a></li>
	     </ul>
    <div class="panel panel-default card-view mt-20 ml-0 mr-0">
		  <div class="panel-wrapper collapse in">
		    <div class="panel-body">
			    <div class="tab-content" id="myTabContent_6">
			        <div id="general" class="tab-pane fade active in" role="tabpanel">
			         
			         		<div class="alert alert-info padding-sm" role="alert">
						        <div class="row">
						          <div class="col-sm-4">
						            Created on : <?php echo $single_property['created_at']; ?>
						          </div>
						          <div class="col-sm-4">
						            Updated on : <?php echo $single_property['updated_at']; ?>
						          </div>
						        </div>
						      </div>

						      <div class="col-md-12">
						      	<h4><?php echo $single_property['property_title'];?></h4>
						      	<p><?php echo $single_property['property_description'];?></p>
						      </div>
						      <div class="col-md-12 mt-15">
						      	<h5>INFORMATION ON THE ANNOUNCEMENT</h5>
						      </div>
						      <div class="row">
						      	<div class="col-md-12 mt-15">
							      	<div class="col-sm-6 col-print-6 col-md-4 col-lg-4">
	                      <div class="display-group">
	            						<div class="display-group-line">
	                          <div class="item">Referance</div>
	                          <div class="value"><?php echo $single_property['reference'];?></div>
	            						</div>
	        							</div>
	                     	<div class="display-group">
	    						        <div class="display-group-line">
	                          <div class="item">Type of property</div>
	                          <div class="value"><?php echo propertyType($single_property['property_type']); ?></div>
							            </div>
	        							</div>
	                    </div>
	                    <div class="col-sm-6 col-print-6 col-md-4 col-lg-4">
	                      <div class="display-group">
	            						<div class="display-group-line">
	                          <div class="item">Transaction Type</div>
	                          <div class="value"><?php echo transactionType($single_property['transaction_type']);?></div>
	            						</div>
	        							</div>
	                     	<div class="display-group">
	    						        <div class="display-group-line">
	                          <div class="item">Price</div>
	                          <div class="value"><?php echo $single_property['price']; ?></div>
							            </div>
	        							</div>
	                    </div>
                  	</div>
                  	<div class="col-md-12">
                  	<hr>
                  	</div>
                  	<div class="col-md-12 mt-15">
                  	<div class="col-sm-6 col-print-6 col-lg-4">
										   <h5>
										      Location
										      <a href="/produit/maplatlng/43.74854500/7.43224000/0/" title="" class="popup-xl pull-right btn btn-sm btn-default m-t--5 hasTooltip" data-rel="tooltip" data-popup="1" data-original-title="Voir la localisation"><i class="fa fa-map"></i></a>
										   </h5>
										   <div class="display-group">
										      <div class="display-group-line">
										         <div class="item">Country</div>
										         <div class="value"><?php $country = countryName($single_property['country']); echo $country['country_nicename'];?></div>
										      </div>
										   </div>
										   <div class="display-group">
										      <div class="display-group-line">
										         <div class="item">Quartier</div>
										         <div class="value"><?php if(!empty($single_property['district'])){ echo $single_property['district'];} else{ echo '-';}?></div>
										      </div>
										   </div>
										   <div class="display-group">
										      <div class="display-group-line">
										         <div class="item">Building</div>
										         <div class="value"><a href="/immeuble/27" class="popup-xl" data-popup="1"><?php if(!empty($single_property['building_id'])){ echo buildingName($single_property['building_id']);} else{ echo '-';}?></a></div>
										      </div>
										   </div>
										</div>
										<div class="col-sm-6 col-print-6 col-lg-4">
										   <h5>CHARACTERISTICS</h5>
										   <div class="display-group">
										      <div class="display-group-line">
										         <div class="item">No. of rooms</div>
										         <div class="value"><?php echo $single_property['num_rooms'];?></div>
										      </div>
										   </div>
										   <div class="display-group">
										      <div class="display-group-line">
										         <div class="item">No. of parking</div>
										         <div class="value"><?php if(!empty($single_property['num_parking'])){ echo $single_property['district'];} else{ echo '-';}?></div>
										      </div>
										   </div>
										   
										</div>
										<div class="col-sm-6 col-print-6 col-lg-4">
										   <h5>SURFACES</h5>
										   <div class="display-group">
										      <div class="display-group-line">
										         <div class="item">Living Area</div>
										         <div class="value"><?php echo $single_property['living_area'];?></div>
										      </div>
										   </div>
										   <div class="display-group">
										      <div class="display-group-line">
										         <div class="item">Total Area</div>
										         <div class="value"><?php if(!empty($single_property['total_area'])){ echo $single_property['total_area'];} else{ echo '-';}?></div>
										      </div>
										   </div>
										   
										</div>
										</div>
										<div class="col-md-12 mt-20">
					      		<h5>DESCRIPTION</h5>
					      		<p><?php echo $single_property['property_excerpt'];?></p>
					      		</div>
						      </div>
						    </div>
			       <div id="photos" class="tab-pane fade active in" role="tabpanel">
			       </div>

			    </div>
			  </div>
			</div>
		</div>
    </div>

    <script type="text/javascript">
      
    function initialize() {

      var lat = $('#latitude').val();
      var lang = $('#longitude').val();
      var address = $('#address').text();

      if(lat == '' && lang == ''){
        var latlng = new google.maps.LatLng(43.7313393,7.4140294);
      }
      else{
       var latlng = new google.maps.LatLng(lat,lang);
     }
        var map = new google.maps.Map(document.getElementById('map'), {
          center: latlng,
          zoom: 13
        });
        var marker = new google.maps.Marker({
          map: map,
          position: latlng,
          icon: "/sparkandpartners/img/placeholder.png",
          draggable: false,
          anchorPoint: new google.maps.Point(0, -29)
       });
        var infowindow = new google.maps.InfoWindow();   
        google.maps.event.addListener(marker, 'click', function() {
          var iwContent = "<div id='iw_container'>" +
          "<div class='iw_title'><b>Location</b> : "+address+" </div></div>";
          // including content to the infowindow
          infowindow.setContent(iwContent);
          // opening the infowindow in the current map and at the current marker location
          infowindow.open(map, marker);
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
     jQuery(function ($) {
      
          $("input[type='checkbox']").checkboxradio();
      
      });
    </script>

<?php include('footer.php');?>