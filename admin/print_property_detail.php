<?php 
require 'config/config.php';
require 'model/model.php';
global $conn;

if(isset($_REQUEST['dpid']) && $_REQUEST['dpid'] != ''){
	$query = "SELECT tp.*, tb.building_name, td.district_name from tbl_property as tp LEFT JOIN tbl_building as tb ON tb.building_id = tp.building_id LEFT JOIN tbl_district as td ON td.district_id = tp.district WHERE tp.is_delete = '0' AND tp.is_draft = '1' AND tp.property_id =".$_REQUEST['dpid']."";
}elseif (isset($_REQUEST['dspid']) && $_REQUEST['dspid'] != '') {
	$query = "SELECT tp.*, tb.building_name, td.district_name from tbl_property as tp LEFT JOIN tbl_building as tb ON tb.building_id = tp.building_id LEFT JOIN tbl_district as td ON td.district_id = tp.district WHERE tp.transaction_type = '1' AND tp.is_delete = '0' AND tp.is_draft = '1' AND tp.property_id =".$_REQUEST['dspid']."";
}elseif (isset($_REQUEST['drpid']) && $_REQUEST['drpid'] != '') {
	$query = "SELECT tp.*, tb.building_name, td.district_name from tbl_property as tp LEFT JOIN tbl_building as tb ON tb.building_id = tp.building_id LEFT JOIN tbl_district as td ON td.district_id = tp.district WHERE tp.transaction_type = '5' AND tp.is_delete = '0' AND tp.is_draft = '1' AND tp.property_id =".$_REQUEST['drpid']."";
}

elseif (isset($_REQUEST['offid']) && $_REQUEST['offid'] != '') {
	$query = "SELECT tp.*, tb.building_name, td.district_name from tbl_property as tp LEFT JOIN tbl_building as tb ON tb.building_id = tp.building_id LEFT JOIN tbl_district as td ON td.district_id = tp.district WHERE tp.is_delete = '0' AND tp.is_offmarket = '1' AND tp.property_id =".$_REQUEST['offid']."";
}elseif (isset($_REQUEST['offmsid']) && $_REQUEST['offmsid'] != '') {
    $query = "SELECT tp.*, tb.building_name, td.district_name from tbl_property as tp LEFT JOIN tbl_building as tb ON tb.building_id = tp.building_id LEFT JOIN tbl_district as td ON td.district_id = tp.district WHERE tp.transaction_type = '1' AND tp.is_delete = '0' AND tp.is_offmarket = '1' AND tp.property_id =".$_REQUEST['offmsid']."";
}elseif (isset($_REQUEST['offmrid']) && $_REQUEST['offmrid'] != '') {
    $query = "SELECT tp.*, tb.building_name, td.district_name from tbl_property as tp LEFT JOIN tbl_building as tb ON tb.building_id = tp.building_id LEFT JOIN tbl_district as td ON td.district_id = tp.district WHERE tp.transaction_type = '5' AND tp.is_delete = '0' AND tp.is_offmarket = '1' AND tp.property_id =".$_REQUEST['offmrid']."";
}

elseif (isset($_REQUEST['plid']) && $_REQUEST['plid'] != '') {
	$query = "SELECT tp.*, tb.building_name, td.district_name from tbl_property as tp LEFT JOIN tbl_building as tb ON tb.building_id = tp.building_id LEFT JOIN tbl_district as td ON td.district_id = tp.district WHERE tp.is_delete = '0' AND tp.is_public = '1' AND tp.property_id =".$_REQUEST['plid']."";
}elseif (isset($_REQUEST['plsid']) && $_REQUEST['plsid'] != '') {
    $query = "SELECT tp.*, tb.building_name, td.district_name from tbl_property as tp LEFT JOIN tbl_building as tb ON tb.building_id = tp.building_id LEFT JOIN tbl_district as td ON td.district_id = tp.district WHERE tp.transaction_type = '1' AND tp.is_delete = '0' AND tp.is_public = '1' AND tp.property_id =".$_REQUEST['plsid']."";
}elseif (isset($_REQUEST['plrid']) && $_REQUEST['plrid'] != '') {
    $query = "SELECT tp.*, tb.building_name, td.district_name from tbl_property as tp LEFT JOIN tbl_building as tb ON tb.building_id = tp.building_id LEFT JOIN tbl_district as td ON td.district_id = tp.district WHERE tp.transaction_type = '5' AND tp.is_delete = '0' AND tp.is_public = '1' AND tp.property_id =".$_REQUEST['plrid']."";
}

elseif (isset($_REQUEST['arid']) && $_REQUEST['arid'] != '') {
	$query = "SELECT tp.*, tb.building_name, td.district_name from tbl_property as tp LEFT JOIN tbl_building as tb ON tb.building_id = tp.building_id LEFT JOIN tbl_district as td ON td.district_id = tp.district WHERE tp.is_delete = '0' AND tp.is_archive = '1' AND tp.property_id =".$_REQUEST['arid']."";
}elseif (isset($_REQUEST['arsid']) && $_REQUEST['arsid'] != '') {
    $query = "SELECT tp.*, tb.building_name, td.district_name from tbl_property as tp LEFT JOIN tbl_building as tb ON tb.building_id = tp.building_id LEFT JOIN tbl_district as td ON td.district_id = tp.district WHERE tp.transaction_type = '1' AND tp.is_delete = '0' AND tp.is_archive = '1' AND tp.property_id =".$_REQUEST['arsid']."";
}elseif (isset($_REQUEST['arcrid']) && $_REQUEST['arcrid'] != '') {
    $query = "SELECT tp.*, tb.building_name, td.district_name from tbl_property as tp LEFT JOIN tbl_building as tb ON tb.building_id = tp.building_id LEFT JOIN tbl_district as td ON td.district_id = tp.district WHERE tp.transaction_type = '5' AND tp.is_delete = '0' AND tp.is_archive = '1' AND tp.property_id =".$_REQUEST['arcrid']."";
}else{
    echo "<script>window.location='dashboard.php'</script>";
    exit;
}

$res = mysqli_query($conn, $query);
if (mysqli_num_rows($res) > 0) {     
    $data = mysqli_fetch_assoc($res);

    $html = '<body>
            <div class="spark_main_container">
            	<div class="topsection">
            		<p>'.$data['property_title'].'</p>
            		<p>'.$data['city'].'</p>
            		<img src="../img/prop-1.jpg" alt="Property1" width="320px" style="border: 3px solid #000;">
            	</div>';

        if($data['property_description'] != ''){   	
     		$html .= '<div class="description">
        		<span>DESCRIPTION<span>
        		<p>'.$data['property_description'].'</p>
        	</div>';
        }

          $html .= '<div class="areadetails">
            		<span>DETAILS<span>
            		<hr class="detail_border">
            		<div class="col-md-3 areadiv">';
    	if($data['total_area'] != ''){
    		$html .= '<p class="arealabel">Total Area:</p>
    	        	<p class="areaval">'.$data['total_area'].' m<sup>2</sup></p>';
    	}
    	if($data['terrace_area'] != ''){
    		$html .= '<p class="arealabel">Terrace Area:</p>
            		<p class="areaval">'.$data['terrace_area'].' m<sup>2</sup></p>';
    	}	
    		$html .= '</div><div class="col-md-3 areadiv">';

    	if($data['num_rooms'] != ''){
    		$html .= '<p class="arealabel">Type:</p>
            			<p class="areaval">'.$data['num_rooms'].' Rooms</p>';
    	}
    	if($data['num_cellars'] != ''){
    		$html .= '<p class="arealabel">Cellar:</p>
            			<p class="areaval">'.$data['num_cellars'].'</p>';
    	}	
    		$html .= '</div><div class="col-md-3 areadiv">';
        
       	if($data['num_bedrooms'] != ''){
        	$html .= '<p class="arealabel">Bedroom:</p>
            			<p class="areaval">'.$data['num_bedrooms'].' </p>';
        } 
        if($data['building_name'] != ''){
        	$html .= '<p class="arealabel">Building:</p>
            			<p class="areaval">'.$data['building_name'].'</p>';
        }    			        			        		
            $html .= '</div><div class="col-md-3 areadiv">';			
         
        if($data['num_bathrooms'] != ''){
        	$html .= '<p class="arealabel">Bathroom:</p>
            			<p class="areaval">'.$data['num_bathrooms'].'</p>';
        } 

        if($data['district_name'] != ''){
        	$html .= '<p class="arealabel">Situation:</p>
            			<p class="areaval">'.$data['district_name'].'</p>';
        }   			        					
            $html .= '</div>
            	   </div><hr class="price_border">';

        if($data['price'] != ''){
            $html .= '<div class="asked_price">
                    <p>ASKED PRICE: <span>'.$data['price'].'</span> EUROS</p>
                </div>';
        }    	        
            $html .= '<div class="benefits">
            		<span>BENEFITS</span>
            		<div class="benefits_div">
            			<ul>
    	        			<li>Centrally Located</li>	        			
            			</ul>	
            		</div>
            		<div class="benefits_div">
            			<ul>	        			
    	        			<li>Supermarket Nearby</li>	        			
            			</ul>	
            		</div>
            		<div class="benefits_div">
            			<ul>	        			
    	        			<li>Fully Renovated</li>
            			</ul>	
            		</div>        				         
            	</div>
            	<div class="prop_images">
    	        	<div class="property_images" style="width: 70%;margin: 0 auto;padding: 30px 0px 20px 0px;">
    	        		<img src="../img/prop-dt-1.jpg" style="border: 2px solid #000;">
    	        	</div>
    	           <div class="property_images" style="width: 70%;margin: 0 auto;padding: 20px 0px;">
    	        		<img src="../img/prop-dt-2.jpg" style="border: 2px solid #000;">
    	        	</div>
    	        	<div class="property_images" style="width: 70%;margin: 0 auto;padding: 20px 0px;">
    	        		<img src="../img/prop-dt-3.jpg" style="border: 2px solid #000;">
    	        	</div>
    	        </div>	
            </div>
        </body>'; 

//==============================================================
//==============================================================
//==============================================================
include("../mpdf/mpdf.php");

//$mpdf=new mPDF('en-GB-x','A4','','',0,0,0,0,0,3); 
$mpdf=new mPDF('en-GB-x','A4','','',0,0,33,32,0,-5); 

$mpdf->SetTitle('Spark and Partners 	- '.$data['reference']);

$mpdf->SetHTMLHeader('<div class="header" style="float: left; width: 100%;padding: 10px 20px 0px 20px;">
                        <div class="logo" style="text-align:center;">
                            <img src="../img/logopdf.jpg" />
                        </div>                        
                    </div>
                    <div class="headerborder">
                        <hr class="header_border">
                    </div>');

$mpdf->SetHTMLFooter('<div class="headerborder">
                        <hr class="header_border">
                    </div>
                    <div class="footer" style="float: left;width: 100%;padding: 0px 20px 40px 20px;">
                       <p>SPARK & PARTNERS SARL – 57 rue Grimaldi 98000, MONACO</p>
                       <p>E: <a href="mailto:hello@spark-estate.com">hello@spark-estate.com</a> - T: <a href="tel:+37797777000">+377 97 77 70 00</a></p>
                       <p>Société au Capital de 150.000 euros - RC I: 15S06776 – TVA: FR 88 000117654</p>
                    </div>');

$mpdf->SetDisplayMode('fullpage');

$mpdf->list_indent_first_level = 0;	// 1 or 0 - whether to indent the first level of a list

// LOAD a stylesheet
$stylesheetfont = file_get_contents('dist/css/font-awesome.min.css');
$mpdf->WriteHTML($stylesheetfont,1);
$stylesheetbootstrap = file_get_contents('dist/css/bootstrap.min.css');
$mpdf->WriteHTML($stylesheetbootstrap,1);
$pdf_style = file_get_contents('dist/css/pdf_style.css');
$mpdf->WriteHTML($pdf_style,1);

$mpdf->WriteHTML($html,2);

$mpdf->Output($data['reference'].' - sparkandpartners.pdf','I');

exit;
} else{
    echo "<script>window.location='dashboard.php'</script>";
    exit;
}
//==============================================================
//==============================================================
//==============================================================


?>