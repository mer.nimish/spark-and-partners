<?php 

$draftproperties = getdraftproperties();
$offmarketproperties = getoffmarketproperties();
$publicproperties = getpublicproperties();
$archiveproperties = getarchiveproperties();

?>
<div class="fixed-sidebar-left">
    <ul class="nav navbar-nav side-nav nicescroll-bar">
        <li class="navigation-header">
            <span>Main</span>
            <i class="zmdi zmdi-more"></i>
        </li>
        <li>
            <a class="active" href="javascript:void(0);" data-toggle="collapse" data-target="#dashboard_dr"><div class="pull-left"><i class="zmdi zmdi-landscape mr-20"></i><span class="right-nav-text">My Agency Properties</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
            <ul id="dashboard_dr" class="collapse collapse-level-1">
                <li>
                    <a href="add_property.php">New Property</a>
                </li>
                <li>
                    <a href="view_draft_all_property.php">Draft <span class="label label-danger pull-right"><?=$draftproperties?></span> </a>
                </li>
                 <li>
                    <a href="view_offmarket_all_property.php">Off Market <span class="label label-warning pull-right"><?=$offmarketproperties?></span></a>
                </li>
                <li>
                    <a href="view_public_all_property.php">Publics <span class="label label-success pull-right"><?=$publicproperties?></span></a>
                </li>
                <li>
                    <a href="view_archive_all_property.php">Archive <span class="label label-primary pull-right"><?=$archiveproperties?></span></a>
                </li>
            </ul>
        </li>

        <?php  if($_SESSION['user_type_id'] == '1') { ?>
        <li>
            <a href="javascript:void(0);" data-toggle="collapse" data-target="#user_dr"><div class="pull-left"><i class="fa fa-user mr-20"></i><span class="right-nav-text">Users</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
            <ul id="user_dr" class="collapse collapse-level-1">
                <li>
                    <a href="add_user.php">Create User</a>
                </li>
                <li>
                    <a href="view_user.php">View User</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="javascript:void(0);" data-toggle="collapse" data-target="#user_type"><div class="pull-left"><i class="fa fa-tasks mr-20"></i><span class="right-nav-text">User Types</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
            <ul id="user_type" class="collapse collapse-level-1">
                <li>
                    <a href="add_user_type.php">Add User Type</a>
                </li>
                <li>
                    <a href="view_user_type.php">View User Type</a>
                </li>
            </ul>
        </li>
        <?php } ?>
        <li>
             <a href="javascript:void(0);" data-toggle="collapse" data-target="#tools"><div class="pull-left"><i class="fa fa-building mr-20"></i><span class="right-nav-text">Tools</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
            <ul id="tools" class="collapse collapse-level-1">
                <li>
                    <a href="view_building.php">Buildings</a>
                </li>
                <li>
                    <a href="add_media.php">
                        Photos & Videos
                    </a>
                </li>
               <!--  <li>
                    <a href="view_user_type.php">View User Type</a>
                </li> -->
            </ul>
        </li>
         <?php  if($_SESSION['add_contact'] == '1') { ?>
        <li>
            <a href="javascript:void(0);" data-toggle="collapse" data-target="#contacts">
                <div class="pull-left"><i class="fa fa-user mr-20"></i><span class="right-nav-text">Contacts</span></div>
                <div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div>
                <div class="clearfix"></div>
            </a>
            <ul id="contacts" class="collapse collapse-level-1">
                <li>
                    <a href="add_contact.php">Add Contact</a>
                </li>
                <li>
                    <a href="view_contact_list.php">View Contact List</a>
                </li>
            </ul>
        </li>
         <?php } ?>
    </ul>
</div>