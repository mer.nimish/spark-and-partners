

<?php 
  session_start();
  require 'config/config.php';
  require 'model/model.php';
  global $conn;
  
  
  if (isset($_SESSION['first_name']) && isset($_SESSION['last_name'])) {
      $first_name = $_SESSION['first_name'];
      $last_name = $_SESSION['last_name'];
  } else {
      echo "<script>window.location='index.php'</script>";
      exit;
  }
  $allcountry = getAllCountry();
  
  if(isset($_REQUEST['property_save'])) {
      $last_inserted_id = updatePropertyGeneral($_REQUEST);
     
      if($last_inserted_id == '1'){
              $successMsg = 'Property Updated successfully.';
          }else{
              $errorOtherMsg = 'Opps! Somthing went wrong.';
              $requestdata = $_REQUEST;
          }
  
  }
  $property_byid = getPropertyByID($_REQUEST['propertyid']);
  include('header.php');
  include('left_sidebar.php');
  
  ?>
 
<div class="page-wrapper">
<div class="container-fluid pt-25">
<div class="col-md-12">
  <?php if(isset($successMsg)){ ?>
  <div class="alert alert-success alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <i class="zmdi zmdi-check pr-15 pull-left"></i>
    <p class="pull-left"><?=$successMsg?></p>
    <div class="clearfix"></div>
  </div>
  <?php } ?>
  <?php if(isset($errorEmailMsg)){ ?>
  <div class="alert alert-warning alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <i class="zmdi zmdi-alert-circle-o pr-15 pull-left"></i>
    <p class="pull-left"><?=$errorEmailMsg?></p>
    <div class="clearfix"></div>
  </div>
  <?php } ?>
  <?php if(isset($errorOtherMsg)){ ?>
  <div class="alert alert-danger alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <i class="zmdi zmdi-block pr-15 pull-left"></i>
    <p class="pull-left"><?=$errorOtherMsg?></p>
    <div class="clearfix"></div>
  </div>
  <?php } ?>
</div>
<!-- Title -->
<div class="row heading-bg">
  <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
    <h5 class="txt-dark">New property assistant</h5>
  </div>
</div>
<!-- /Title -->
<!-- Row -->
<div class="row">
<div class="col-md-12">
  <div class="panel panel-default card-view">
    <div class="panel-wrapper collapse in">
      <div class="panel-body">
        <div class="noprint content-nav">
          <div class="nav-btns">
            <div class="nav-btns-left pull-left" style="margin-right: 245.5px;">
              <div class="btn-group btn-group-reduce pills-struct">
                <ul role="tablist" class="nav nav-pills" id="myTabs_6">
                <li class="active" role="presentation"><a href="#form_property_general" class="btn btn-default active" data-toggle="tab" role="tab">General</a></li>
                <li class="" role="presentation"><a href="#form_property_localisation" class="form_property_localisation btn btn-default" data-toggle="tab">Location</a></li>
                <li class="" role="presentation"><a href="#form_property_textes" class="btn btn-default" data-toggle="tab">Texts</a></li>
                <li class="" role="presentation"><a href="#form_property_images" class="btn btn-default" data-toggle="tab">Pictures </a></li>
              </ul>
              </div>
            </div>
          </div>
        </div>
        

<form name="property" method="post" id="formproperty" class="form-dual" name="test">
 
  <div class="tab-content edit-tab-content">
  <!--   <div class="tab-pane active" id="form_loading">
      <p class="m-t-5">
        <i class="fa fa-spinner fa-spin"></i> Loading...
      </p>
    </div> -->
    <div class="tab-pane fade active in" id="form_property_general">
      <div class="alert alert-info padding-sm" role="alert">
        <div class="row">
          <div class="col-sm-4">
            Created on : <?php echo $property_byid['created_at']; ?>

          </div>
        </div>
      </div>
      <div class="alert alert-danger hidden">
        <h4>Alur Law Information</h4>
        <p>
          Thank you for completing the information required by the Alur law for properties located in ...        
        </p>
        <p>
          <a href="#alur" class="btn btn-sm btn-default"><i class="fas fa-map-marker-alt"></i> Définir les informations</a>
        </p>
      </div>
      <div class="alert alert-danger hidden">
        <h4>Information on charges</h4>
        <p>
          Thank you to complete the information concerning rental charges for this property located in...        
        </p>
        <p>
          <a href="#prix" class="btn btn-sm btn-default"><i class="fas fa-map-marker-alt"></i> Définir les informations</a>
        </p>
      </div>
      <div class="alert alert-danger hidden">
        <h4>Tenant fees information (Alur Law)</h4>
        <p>
          Please fill in the information about the tenant's fees        
        </p>
        <p>
          <a href="#alur" class="btn btn-sm btn-default"><i class="fas fa-map-marker-alt"></i> Define information</a>
        </p>
      </div>
      <div class="form-group  ">
        <div class="form-group-line">
          <label class="control-label" for="property_privatenote">Private notes</label>
          <div class="input-elt has-addons">
            <div class="input-group">
              <textarea id="property_privatenote" name="property_privatenote" rows="6" class="private forced form-control" ></textarea>
              <div class="input-group-addon"><i class='fa fa-lock' title='Private information' data-rel='tooltip'></i></div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6 col-lg-4">
          <div class="form-group">
            <h4>Ad information</h4>
          </div>
          <div class="form-group  ">
            <div class="form-group-line">
              <label class="control-label required" for="property_reference">Reference</label>
              <div class="input-elt ">
                <div class="input-group"><input type="text" id="property_reference" name="reference" required="required" class=" form-control" value="<?php echo $property_byid['reference'];?>" /></div>
              </div>
            </div>
          </div>
          <!-- <div class="hidden">
            <div class="form-group  ">
              <div class="form-group-line">
                <label class="control-label required" for="property_type_transaction">Transaction type</label>
                <div class="input-elt input-elt-select ">
                  <div class="input-group">
                    <select id="property_type_transaction" name="property[type_transaction]" class=" form-control form-control" data-container="body">
                      <option value="Sale"  selected="selected">For sale</option>
                      <option value="Location" >Rental</option>
                      <option value="Location Saisonniere" >Location Saisonnière</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </div> -->
          <div class="row">
            <div class="col-xs-12 col-md-12">
              <div class="form-group  ">
                <div class="form-group-line">
                  <label class="control-label required" for="property_typeoffre">Transaction type</label>
                  <div class="input-elt input-elt-select ">
                    <div class="input-group">
                      <select id="property_typeoffre" name="property_typeoffre" class=" form-control form-control" data-container="body">
                          <?php $transection_type = $property_byid['sub_transaction_type'];?>
                          <optgroup label="Sale">
                          <option value="1"   data-type_transaction="Sale" <?php echo ($transection_type == 1) ? 'selected' : ''; ?>>Sale</option>
                          <option value="3"  data-type_transaction="Sale" <?php echo ($transection_type == 3) ? 'selected' : ''; ?>>New program</option>
                          <option value="4"  data-type_transaction="Sale" <?php echo ($transection_type == 4) ? 'selected' : ''; ?>>Sale professional</option>
                          <option value="2"  data-type_transaction="Sale" <?php echo ($transection_type == 2) ? 'selected' : ''; ?>>Life Time</option>
                        </optgroup>
                        <optgroup label="Location">
                          <option value="5"  data-type_transaction="Location" <?php echo ($transection_type == 5) ? 'selected' : ''; ?>>Location</option>
                          <option value="6"  data-type_transaction="Location" <?php echo ($transection_type == 6) ? 'selected' : ''; ?>>Location furnished</option>
                          <option value="7"  data-type_transaction="Location" <?php echo ($transection_type == 7) ? 'selected' : ''; ?>>Location seasonal</option>
                          <option value="8"  data-type_transaction="Location" <?php echo ($transection_type == 8) ? 'selected' : ''; ?>>Location professional</option>
                        </optgroup>
                                        
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="hidden col-xs-4 col-md-3" title="<strong>Devise du property</strong><br />Contactez-nous pour plus d&#039;informations" data-rel="tooltip">
              <div class="form-group  ">
                <div class="form-group-line">
                  <div class="input-elt input-elt-select ">
                    <div class="input-group">
                      <select id="property_monnaie" name="property[monnaie]" class=" form-control form-control" data-container="body">
                        <option value="EUR"  selected="selected">€</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
           <div class="sector_type_lifetime hidden" data-condition-selector="#property_typeoffre" data-condition-value="6">
            <div class="form-group  ">
              <div class="form-group-line">
                <label class="control-label" for="property_typelifetime">Life type</label>
                <div class="input-elt input-elt-select ">
                  <div class="input-group">
                    <select id="property_typelifetime" name="property_typelifetime" class=" form-control form-control" data-container="body">
                      <option value=""></option>
                      <option value="Free life" <?php echo ($transection_type == 'Free life') ? 'selected' : ''; ?> selected="selected">Free life</option>
                      <option value="Life annuity" <?php echo ($transection_type == 'Life annuity') ? 'selected' : ''; ?> >Life annuity</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="autohide location_rental hidden" data-condition-selector="#property_typeoffre" data-condition-value="8">
            <div class="autoshow checkbox-align" data-condition-selector="#property_type_transaction" data-condition-value="Location">
              <label class="btn-block" for="property_locationrent">
                  <input type="checkbox" id="property_locationrent" name="property_locationrent" class=" form-control" value="1" <?php if($property_byid['location_rental'] == 1) { echo "checked=checked";}?> > Location rental possible
              </label>  
            </div>
          </div>
          <a name="prix" id="prix" class="anchor"></a>
          <div class="form-group  ">
            <div class="form-group-line">
              <label class="control-label" for="property_prifix">Price/Rental</label>
              <div class="input-elt has-addons">
                <div class="input-group">
                  <input type="text" id="property_prifix" name="property_prifix" value="<?php echo $property_byid['price'];?>"  class="numberf form-control"  />
                  <div class="input-group-addon"><span class='monnaie'>€</span></div>
                  <div class="input-group-addon"><i class="switcher fa fa-unlock-alt c-gray-light action" title="Private information" data-icon-unchecked="fa-unlock-alt" data-icon-checked="fa-lock" data-checkbox="#property_prifixIsPrivate"></i></div>
                </div>
              </div>
            </div>
          </div>
          <!-- <div class="form-group  ">
            <div class="form-group-line">
              <label class="control-label" for="property_prifixIsPrivate">Price private</label>
              <div class="input-elt input-checkbox ">
                <div class="input-group"><input type="checkbox" id="property_prifixIsPrivate" name="property[prixIsPrivate]" class=" form-control" value="1" /></div>
              </div>
            </div>
          </div> -->
          <div class="sector_type_lifetime hidden" data-condition-selector="#property_typeoffre" data-condition-value="6">
            <div class="form-group  ">
              <div class="form-group-line">
                <label class="control-label" for="property_monthlyrent">Monthly Rent</label>
                <div class="input-elt has-addons">
                  <div class="input-group">
                    <input type="text" value="<?php echo $property_byid['monthly_rent'];?>" id="property_monthlyrent" name="property_monthlyrent"  class="numberf forced form-control" />
                    <div class="input-group-addon"><span class='monnaie'>€</span></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="autoshow" data-condition-selector="#property_type_transaction" data-condition-value="Sale">
            <div class="autoshow" data-condition-selector="#property_country" data-condition-value="1">
              <div class="form-group  ">
                <div class="form-group-line">
                  <div id="propertyextension_monaco_data">
                      <label class="btn-block" for="property_noretrocession">
                          <input type="checkbox" id="property_noretrocession" name="property_noretrocession" class=" form-control" value="1" <?php if($property_byid['no_retrocession'] == 1){ echo "checked=checked";
                          } ?>> No Retrocession
                      </label>
                    
                  </div >
                </div>
              </div>
            </div>
            <div class="form-group annual_rent">
              <div class="form-group-line">
                <label class="control-label" for="property_annualcost">Annual costs</label>
                <div class="input-elt has-addons">
                  <div class="input-group">
                    <input type="text" value="<?php echo $property_byid['annual_costs'];?>" id="property_annualcost" name="property_annualcost"  class="numberf form-control" />
                    <div class="input-group-addon"><span class='monnaie'>€</span></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="autoshow monthly_rent hidden" data-condition-selector="#property_type_transaction" data-condition-value="Location">
            <div class="form-group  ">
              <div class="form-group-line">
                <label class="control-label" for="property_monthlycharge">Monthly service charges</label>
                <div class="input-elt has-addons">
                  <div class="input-group">
                    <input type="text" id="property_monthlycharge" name="property_monthlycharge" value="<?php echo $property_byid['monthly_service_charge'];?>" class=" form-control" />
                    <div class="input-group-addon"><span class='monnaie'>€</span></div>
                    <div class="input-group-addon"><i class="switcher far fa-check-square c-gray-light action" title="Service charges included" data-checkbox="#property_isChargesIncluses"></i></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group  ">
            <div class="form-group-line">
              <label class="control-label" for="property_releasedate">Release date</label>
              <div class="input-elt ">
                <div class="input-group"><input type="text" value="<?php echo $property_byid['release_date'];?>" id="property_releasedate" name="property_releasedate" maxlength="255"  class=" form-control" /></div>
              </div>
            </div>
          </div>
          <div class="form-group  ">
            <div class="form-group-line">
              <label class="control-label required" for="property_soleagent">Sole agent</label>
              <div class="input-elt input-elt-select ">
                <div class="input-group">
                  <select id="property_soleagent" name="property_soleagent" class=" form-control form-control" data-container="body">
                    <option value="0" <?php echo ($property_byid['sole_agent'] == 0) ? 'selected' : ''; ?>>Not exclusive</option>
                    <option value="1" <?php echo ($property_byid['sole_agent'] == 1) ? 'selected' : ''; ?>>Sole agent</option>
                    <option value="2" <?php echo ($property_byid['sole_agent'] == 2) ? 'selected' : ''; ?>>Co-exclusive</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="autohide " data-condition-selector="#property_country" data-condition-value="1">
            <div class="form-group  ">
              <div class="form-group-line">
                <label class="control-label" for="property_collaboration">In collaboration with</label>
                <div class="input-elt ">
                  <div class="input-group"><input type="text" id="property_collaboration" name="property_collaboration" maxlength="255" class=" form-control" value="<?php echo $property_byid['collaboration_with'];?>" /></div>
                </div>
              </div>
            </div>
          </div>
          <!-- <datalist id="list_liberation">
            <option value="Now">
            <option value="Quickly">
            <option value="Shortly">
          </datalist> -->
          <!-- <div class="autoshow" data-condition-selector="#property_country" data-condition-value="1">
          <div class="form-group  "><div class="form-group-line"><label class="control-label" for="property_lois">Special Law</label><div class="input-elt has-addons"><div class="input-group"><input type="text" id="property_lois" name="property[lois]" maxlength="255"  list="list_lois" class=" form-control" /><div class="input-group-addon"><i class="fa fa-info" title="Laws applicable to a property in Monaco" data-rel="tooltip"></i></div></div></div></div></div>
          <datalist id="list_lois">
          <option value="Loi 887">
          <option value="Loi 1291">
          </datalist>
          </div> -->
        </div>
        <div class="col-sm-6 col-lg-4">
        <div class="form-group">
        <h4>Details</h4>
        </div>
       
          <div class="form-group  ">
             <div class="form-group-line">
                <label class="control-label required" for="property_type">Product type</label>
                <div class="input-elt input-elt-select ">
                   <div class="input-group">
                      <?php $property_type = $property_byid['sub_property_type']; ?>
                      <select id="property_type" name="property_type" data-hide-disabled="data-hide-disabled" data-live-search="data-live-search" class=" form-control form-control" data-container="body">
                         <optgroup label="Appartement">
                            <option value="1" data-type_property="1" data-forTypesOffres=",1,2,6,5,7,3," <?php echo ($property_type == 1) ? 'selected' : ''; ?>>Appartement</option>
                            <option value="2" data-type_property="1" data-forTypesOffres=",1,2,6,5,7,3," <?php echo ($property_type == 2) ? 'selected' : ''; ?>>Loft</option>
                            <option value="3" data-type_property="1" data-forTypesOffres=",1,2,6,5,7,3," <?php echo ($property_type == 3) ? 'selected' : ''; ?>>Penthouse/Roof</option>
                            <option value="4"  data-type_property="1" data-forTypesOffres=",1,2,6,5,7,3," <?php echo ($property_type == 4) ? 'selected' : ''; ?>>Duplex</option>
                            <option value="5"  data-type_property="1" data-forTypesOffres=",1,2,6,5,7,3," <?php echo ($property_type == 5) ? 'selected' : ''; ?>>Ground floor</option>
                            <option value="6"  data-type_property="1" data-forTypesOffres=",1,2,6,5,7,3," <?php echo ($property_type == 6) ? 'selected' : ''; ?>>Room of service</option>
                         </optgroup>
                         <optgroup label="House">
                            <option value="7" data-type_property="7" data-forTypesOffres=",1,2,6,5,7,3," <?php echo ($property_type == 7) ? 'selected' : ''; ?>>House</option>
                            <option value="15" data-type_property="7" data-forTypesOffres=",1,2,6,5,7,3," <?php echo ($property_type == 15) ? 'selected' : ''; ?>>Villa</option>
                            <option value="8" data-type_property="7" data-forTypesOffres=",1,2,6,5,7,3," <?php echo ($property_type == 8) ? 'selected' : ''; ?>>Bastide</option>
                            <option value="9" data-type_property="7" data-forTypesOffres=",1,2,6,5,7,3," <?php echo ($property_type == 9) ? 'selected' : ''; ?>>castle</option>
                            <option value="10" data-type_property="7" data-forTypesOffres=",1,2,6,5,7,3," <?php echo ($property_type == 10) ? 'selected' : ''; ?>>Closed</option>
                            <option value="12" data-type_property="7" data-forTypesOffres=",1,2,6,5,7,3," <?php echo ($property_type == 12) ? 'selected' : ''; ?>>Mas</option>
                            <option value="14" data-type_property="7" data-forTypesOffres=",1,2,6,5,7,3," <?php echo ($property_type == 14) ? 'selected' : ''; ?>>Ground villa</option>
                            <option value="13" data-type_property="7" data-forTypesOffres=",1,2,6,5,7,3," <?php echo ($property_type == 13) ? 'selected' : ''; ?>>Property</option>
                            <option value="16" data-type_property="7" data-forTypesOffres=",1,2,6,5,7,3," <?php echo ($property_type == 16) ? 'selected' : ''; ?>>Chalet</option>
                         </optgroup>
                         <optgroup label="Building">
                            <option value="17" data-type_property="29" data-forTypesOffres=",1,2,5," <?php echo ($property_type == 17) ? 'selected' : ''; ?>>Building plot</option>
                            <option value="18" data-type_property="29" data-forTypesOffres=",1,2,5," <?php echo ($property_type == 18) ? 'selected' : ''; ?>>Non constructible land</option>
                         </optgroup>
                         <optgroup label="Parking / Garage / Box">
                            <option value="22" data-type_property="11" data-forTypesOffres=",1,2,3,5," <?php echo ($property_type == 22) ? 'selected' : ''; ?>>Parking</option>
                            <option value="23" data-type_property="11" data-forTypesOffres=",1,2,3,5," <?php echo ($property_type == 23) ? 'selected' : ''; ?>>Garage</option>
                            <option value="24" data-type_property="11" data-forTypesOffres=",1,2,3,5," <?php echo ($property_type == 24) ? 'selected' : ''; ?>>Box</option>
                         </optgroup>
                         <optgroup label="Office">
                            <option value="19" data-type_property="19" data-forTypesOffres=",8,4," <?php echo ($property_type == 19) ? 'selected' : ''; ?>>Office</option>
                         </optgroup>
                         <optgroup label="Cellar">
                            <option value="20" data-type_property="20" data-forTypesOffres=",1,2,3,5," <?php echo ($property_type == 20) ? 'selected' : ''; ?>>Cellar</option>
                         </optgroup>
                         <optgroup label="Local">
                            <option value="21" data-type_property="21" data-forTypesOffres=",8,4," <?php echo ($property_type == 21) ? 'selected' : ''; ?>>Local</option>
                         </optgroup>
                         <optgroup label="Commercial property">
                            <option value="25" data-type_property="25" data-forTypesOffres=",4," <?php echo ($property_type == 25) ? 'selected' : ''; ?>>Commercial property</option>
                         </optgroup>
                         <optgroup label="Commercial premises walls">
                            <option value="27" data-type_property="27" data-forTypesOffres=",4," <?php echo ($property_type == 27) ? 'selected' : ''; ?>>Commercial premises walls</option>
                         </optgroup>
                         <optgroup label="Assignments of right to the lease">
                            <option value="26" data-type_property="26" data-forTypesOffres=",4," <?php echo ($property_type == 26) ? 'selected' : ''; ?>>Assignments of right to the lease</option>
                         </optgroup>
                         <optgroup label="Other property">
                            <option value="28" data-type_property="28" data-forTypesOffres=",1,4,5,7,8,6," <?php echo ($property_type == 28) ? 'selected' : ''; ?>>Other property</option>
                            <option value="29" data-type_property="28" data-forTypesOffres=",1,4,5,7,8,6," <?php echo ($property_type == 29) ? 'selected' : ''; ?>>Building</option>
                         </optgroup>
                      </select>
                   </div>
                </div>
             </div>
          </div>
          <div class="row">
             <div class="col-sm-6">
                <div class="form-group  ">
                  <label class="btn-block" for="property_mixeduse">
                     <input type="checkbox" id="property_mixeduse" name="property_mixeduse" class=" form-control" value="1" <?php if($property_byid['mixed_use']){echo "checked=checked";}?>> Mixed use
                  </label>
                </div>
             </div>
             <div class="col-sm-6" data-condition-selector="#property_type_transaction" data-condition-value="Location">
                <div class="form-group">
                  <label class="btn-block" for="property_furnished">
                     <input type="checkbox" id="property_furnished" name="property_furnished" class=" form-control" value="1" <?php if($property_byid['furnished']){echo "checked=checked";}?>/> Furnished
                  </label>
                </div>
             </div>
          </div>
          <div class="form-group">
             <div class="form-group-line">
                <label class="control-label" for="numrooms">Num rooms</label>
                <div class="input-elt input-elt-select ">
                   <div class="input-group">
                     <?php $num_rooms  = $property_byid['num_rooms']; ?>
                      <select id="numrooms" name="numrooms" class=" form-control form-control" data-container="body">
                         <option value="1" <?php echo ($num_rooms == 1) ? 'selected' : ''; ?>>Studio</option>
                         <option value="2" <?php echo ($num_rooms == 2) ? 'selected' : ''; ?>>2 rooms</option>
                         <option value="3" <?php echo ($num_rooms == 3) ? 'selected' : ''; ?>>3 rooms</option>
                         <option value="4" <?php echo ($num_rooms == 4) ? 'selected' : ''; ?>>4 rooms</option>
                         <option value="5" <?php echo ($num_rooms == 5) ? 'selected' : ''; ?>>5 rooms</option>
                         <option value="10"<?php echo ($num_rooms == 10) ? 'selected' : ''; ?>>+5 rooms</option>
                         <option value="0" <?php echo ($num_rooms == 0) ? 'selected' : ''; ?>>Openspace</option>
                         <option value="" <?php echo ($num_rooms == '') ? 'selected' : ''; ?>>Not defined</option>
                      </select>
                   </div>
                </div>
             </div>
          </div>
          <div class="form-group">
             <div class="form-group-line">
                <label class="control-label" for="numbedrooms">Num bedrooms</label>
                <div class="input-elt input-elt-select ">
                   <div class="input-group">
                    <?php $num_bedrooms  = $property_byid['num_bedrooms']; ?>
                      <select id="numbedrooms" name="numbedrooms" class=" form-control form-control" data-container="body">
                         <option value="1" <?php echo ($num_bedrooms == 1) ? 'selected' : ''; ?>>1</option>
                         <option value="2" <?php echo ($num_bedrooms == 2) ? 'selected' : ''; ?>>2</option>
                         <option value="3" <?php echo ($num_bedrooms == 3) ? 'selected' : ''; ?>>3</option>
                         <option value="4" <?php echo ($num_bedrooms == 4) ? 'selected' : ''; ?>>4</option>
                         <option value="5" <?php echo ($num_bedrooms == 5) ? 'selected' : ''; ?>>5</option>
                         <option value="10" <?php echo ($num_bedrooms == 10) ? 'selected' : ''; ?>>+5</option>
                         <option value=""  <?php echo ($num_bedrooms == '') ? 'selected' : ''; ?>>Not Defined/None</option>
                      </select>
                   </div>
                </div>
             </div>
          </div>
          <div class="form-group  ">
             <div class="form-group-line">
                <label class="control-label" for="numsbathrooms">Num bathrooms</label>
                <div class="input-elt input-elt-select ">
                   <div class="input-group">
                    <?php $num_bathrooms  = $property_byid['num_bathrooms']; ?>
                      <select id="numsbathrooms" name="numsbathrooms" class=" form-control form-control" data-container="body">
                         <option value="1" <?php echo ($num_bathrooms == 1) ? 'selected' : ''; ?>>1</option>
                         <option value="2" <?php echo ($num_bathrooms == 2) ? 'selected' : ''; ?>>2</option>
                         <option value="3" <?php echo ($num_bathrooms == 3) ? 'selected' : ''; ?>>3</option>
                         <option value="4" <?php echo ($num_bathrooms == 4) ? 'selected' : ''; ?>>4</option>
                         <option value="5" <?php echo ($num_bathrooms == 5) ? 'selected' : ''; ?>>5</option>
                         <option value="10"<?php echo ($num_bathrooms == 10) ? 'selected' : ''; ?>>+5</option>
                         <option value=""  <?php echo ($num_bathrooms == '') ? 'selected' : ''; ?>>Not Defined/None</option>
                      </select>
                   </div>
                </div>
             </div>
          </div>
          <div class="form-group  ">
             <div class="form-group-line">
                <label class="control-label" for="numparking">Num parkings</label>
                <div class="input-elt input-elt-select ">
                   <div class="input-group">
                    <?php $num_parking  = $property_byid['num_parking']; ?>
                      <select id="numparking" name="numparking" class=" form-control form-control" data-container="body">
                         <option value="1" <?php echo ($num_parking == 1) ? 'selected' : ''; ?>>1</option>
                         <option value="2" <?php echo ($num_parking == 2) ? 'selected' : ''; ?>>2</option>
                         <option value="3" <?php echo ($num_parking == 3) ? 'selected' : ''; ?>>3</option>
                         <option value="4" <?php echo ($num_parking == 4) ? 'selected' : ''; ?>>4</option>
                         <option value="5" <?php echo ($num_parking == 5) ? 'selected' : ''; ?>>5</option>
                         <option value="10"<?php echo ($num_parking == 10) ? 'selected' : ''; ?>>+5</option>
                         <option value=""  <?php echo ($num_parking == '') ? 'selected' : ''; ?>>Not Defined/None</option>
                      </select>
                   </div>
                </div>
             </div>
          </div>
          <div class="form-group  ">
             <div class="form-group-line">
                <label class="control-label" for="numbox">Num box</label>
                <div class="input-elt input-elt-select ">
                   <div class="input-group">
                    <?php $num_box  = $property_byid['num_box']; ?>
                      <select id="numbox" name="numbox" class=" form-control form-control" data-container="body">
                         <option value="1" <?php echo ($num_box == 1) ? 'selected' : ''; ?>>1</option>
                         <option value="2" <?php echo ($num_box == 2) ? 'selected' : ''; ?>>2</option>
                         <option value="3" <?php echo ($num_box == 3) ? 'selected' : ''; ?>>3</option>
                         <option value="4" <?php echo ($num_box == 4) ? 'selected' : ''; ?>>4</option>
                         <option value="5" <?php echo ($num_box == 5) ? 'selected' : ''; ?>>5</option>
                         <option value="10" <?php echo ($num_box == 10) ? 'selected' : ''; ?>>+5</option>
                         <option value=""  <?php echo ($num_box == '') ? 'selected' : ''; ?>>Not Defined/None</option>
                      </select>
                   </div>
                </div>
             </div>
          </div>
          <div class="form-group  ">
             <div class="form-group-line">
                <label class="control-label" for="numcaves">Num cellars</label>
                <div class="input-elt input-elt-select ">
                   <div class="input-group">
                    <?php $num_cellars  = $property_byid['num_cellars']; ?>
                      <select id="numcaves" name="numcaves" class=" form-control form-control" data-container="body">
                         <option value="1" <?php echo ($num_cellars == 1) ? 'selected' : ''; ?>>1</option>
                         <option value="2" <?php echo ($num_cellars == 2) ? 'selected' : ''; ?>>2</option>
                         <option value="3" <?php echo ($num_cellars == 3) ? 'selected' : ''; ?>>3</option>
                         <option value="4" <?php echo ($num_cellars == 4) ? 'selected' : ''; ?>>4</option>
                         <option value="5" <?php echo ($num_cellars == 5) ? 'selected' : ''; ?>>5</option>
                         <option value="10" <?php echo ($num_cellars == 10) ? 'selected' : ''; ?>>+5</option>
                         <option value=""  <?php echo ($num_cellars == '') ? 'selected' : ''; ?>>Not Defined/None</option>
                      </select>
                   </div>
                </div>
             </div>
          </div>
          <div class="form-group  ">
             <div class="form-group-line">
                <label class="control-label" for="property_floor">Floor</label>
                <div class="input-elt has-addons">
                   <div class="input-group">
                      <input type="text" id="property_floor" value="<?php echo $property_byid['floor']; ?>" name="property_floor" maxlength="255"  class=" form-control" />
                      <div class="input-group-addon"><i class="switcher fa fa-unlock-alt c-gray-light" data-icon-unchecked="fa-unlock-alt" data-icon-checked="fa-lock" title="Private information" data-checkbox="#property_etageIsPrivate"></i></div>
                   </div>
                </div>
             </div>
          </div>
         <!--  <div class="form-group  ">
             <div class="form-group-line">
                <label class="control-label" for="property_etageIsPrivate">Etage is private</label>
                <div class="input-elt input-checkbox ">
                   <div class="input-group"><input type="checkbox" id="property_etageIsPrivate" name="property[etageIsPrivate]" class=" form-control" value="1" /></div>
                </div>
             </div>
          </div-->
          <div class="form-group">
              <label class="btn-block" for="property_intrestingview">
                 <input type="checkbox" id="property_intrestingview" name="property_intrestingview" class=" form-control" value="1" <?php if($property_byid['interesting_view'] == 1){ echo "checked=checked";} ?> />Interesting view
              </label>
          </div>
          <div class="form-group  ">
             <div class="form-group-line">
                <label class="control-label" for="property_state">State</label>
                <div class="input-elt input-elt-select ">
                   <div class="input-group">
                    <?php $state = $property_byid['state']; ?>
                      <select id="property_state" name="property_state" class=" form-control form-control" data-container="body">
                         <option value="">Not defined</option>
                         <option value="1" <?php echo ($state == 1) ? 'selected' : ''; ?>>Work required</option>
                         <option value="2" <?php echo ($state == 2) ? 'selected' : ''; ?>>To refresh</option>
                         <option value="3" <?php echo ($state == 3) ? 'selected' : ''; ?>>Renovated</option>
                         <option value="4" <?php echo ($state == 4) ? 'selected' : ''; ?>>Good condition</option>
                         <option value="5" <?php echo ($state == 5) ? 'selected' : ''; ?>>Very good condition</option>
                         <option value="6" <?php echo ($state == 6) ? 'selected' : ''; ?>>Luxurious amenities</option>
                         <option value="7" <?php echo ($state == 7) ? 'selected' : ''; ?>>New</option>
                      </select>
                   </div>
                </div>
             </div>
          </div>


        </div>
        <div class="col-sm-6 col-lg-4 ">
          <div class="form-group">
            <h4>Areas</h4>
          </div>
          <div class="superficies">
            <div class="form-group  ">
              <div class="form-group-line">
                <label class="control-label" for="property_livingarea">Living area</label>
                <div class="input-elt has-addons">
                  <div class="input-group">
                    <input type="text" id="property_livingarea" name="property_livingarea"  class="numberf form-control" value="<?php echo $property_byid['living_area'];?>"/>
                    <div class="input-group-addon">m²</div>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group  ">
              <div class="form-group-line">
                <label class="control-label" for="property_terracearea">Terrace area</label>
                <div class="input-elt has-addons">
                  <div class="input-group">
                    <input type="text" id="property_terracearea" name="property_terracearea"  class="numberf form-control" value="<?php echo $property_byid['terrace_area'];?>" />
                    <div class="input-group-addon">m²</div>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group  ">
              <div class="form-group-line">
                <label class="control-label" for="property_landarea">Land area</label>
                <div class="input-elt has-addons">
                  <div class="input-group">
                    <input type="text" id="property_landarea" name="property_landarea"  class="numberf form-control" value="<?php echo $property_byid['land_area'];?>" />
                    <div class="input-group-addon">m²</div>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group  ">
              <div class="form-group-line">
                <label class="control-label" for="property_gardenarea">Garden area</label>
                <div class="input-elt has-addons">
                  <div class="input-group">
                    <input type="text" id="property_gardenarea" name="property_gardenarea"  class="numberf form-control" value="<?php echo $property_byid['garden_area'];?>"/>
                    <div class="input-group-addon">m²</div>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group  ">
              <div class="form-group-line">
                <label class="control-label" for="property_totalarea">Total area</label>
                <div class="input-elt has-addons">
                  <div class="input-group">
                    <input type="text" id="property_totalarea" name="property_totalarea"  class="numberf form-control"  value="<?php echo $property_byid['total_area'];?>"/>
                    <div class="input-group-addon"><i class="fa fa-calculator"></i></div>
                    <div class="input-group-addon">m²</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <script>
            $(function () {
                // $("#numrooms").change(function () {
                //     var $next = $(this).closest(".form-group").next();
                //     if ($next) {
                //         if ($(this).val()==10) {
                //             $next.removeClass("hidden");
                //             if (!$next.find(":input").val()) {
                //                 $next.find(":input").val(6);
                //             }
                //         }
                //         else {
                //             $next.addClass("hidden");
                //             $next.find(":input").val("");
                //         }
                //     }
                // }).change();
                $(".input-group-addon .fa-calculator").click(function () {
                    $(this).closest(".form-group").find("input[type='text']").val("");
                    var sum=0;
                    $(this).closest(".superficies").find("input[type='text']").each(function () {
                        var val = parseFloat($(this).val().replace(/ /g, ""));
                        console.log("val: "+val);
                        if (val!="Nan" && val>0) sum+=val;
                    });
                    $(this).closest(".form-group").find("input[type='text']").val(sum).change();
                });
            });
          </script>
        </div>
      </div>
      
    </div>
    <div id="menu-container" style="position:absolute;"></div>
    <script>
      $(function () {
      //    $(function () {
          $("#property_typeoffre").change(function () {
              var val = $(this).val();
              var for_type_transaction = $(this).find("option:selected").attr("data-type_transaction");
      
              if (for_type_transaction=="Location") {
                  $("#property_meuble").prop("checked", val==5).change().checkboxradio();
              }
              $("#property_lifetime").prop("checked", val==2).change().checkboxradio();
      
      
              $("#property_type optgroup").each(function () {

                  var test = $(this).find("option").first().attr("data-forTypesOffres");
                  if (test.indexOf(","+val+",")>=0) {
                      $(this).find("option").prop("disabled", false);
                  }
                  else {
                      $(this).find("option").prop("disabled", true);
                  }
              }).parent().change().selectpicker("refresh");
      
              if (val==2) {  
                  $(".sector_type_lifetime").removeClass("hidden").find("select").attr("required", "required");
                           
                  $('label[for="property_prifix"]').html('Bouquet');
              }
              else {
                  $(".sector_type_lifetime").addClass("hidden").find("select").attr("required", false);                     
                  $("#property_rentelifetime").val("");
                  $("#property_typelifetime").val("").selectpicker("refresh");
                  if (for_type_transaction=='Sale') {
                      $('label[for="property_prifix"]').html('Price');
                  }
                  else if (for_type_transaction=='Location') {
                      $('label[for="property_prifix"]').html('Rent');
                  }
                  else {
                      $('label[for="property_prifix"]').html('Price/Rent');
                  }
              }
              if (for_type_transaction=="Location" && val!=8) {   
                $(".location_rental").removeClass("hidden");
                $(".monthly_rent").removeClass("hidden");
                $(".annual_rent").addClass("hidden");
              }
              else{
                $(".location_rental").addClass("hidden");
                $(".monthly_rent").addClass("hidden");
                $(".annual_rent").removeClass("hidden");
              }

      
              $("#property_type_transaction").val(for_type_transaction).change();
          }).change();
          $("#property_monnaie").change(function () {
              if ($(this).val()) {
                  var $option = $(this).find('option:selected');
                  if ($option) {
                      $(".monnaie").html($option.html());
                  }
              }
          }).change();
          $("#property_type").change(function () {
              if (!$(this).val()) {
                  $("#property_type_property").val("").change();
              }
              else {
                  var for_type_property = $(this).find("option:selected").attr("data-type_property");
                  $("#property_type_property").val(for_type_property).change();
              }
          }).change();
      
      
          // $(".input-group-addon .switcher").not(".switched").each(function () {
          //     var $this = $(this);
          //     $this.addClass("switched");
          //     var basetitle = $this.attr("title");
          //     var $input = $(this).closest(".form-group").find(":input[id]");
          //     var $checkbox = $($this.data("checkbox"));
          //     $checkbox.data("i", $this.attr("id"));
          //     $checkbox.data("input", $input.attr("id"));
          //     $input.data("i", $this.attr("id"));
          //     $input.data("checkbox", $checkbox.attr("id"));
      
          //     $this.parent().addClass("action").data("checkbox", $checkbox.attr("id"));
          //     $checkbox.change(function () {
          //         var id = $(this).data("input");
          //         var $input = $("#"+id);
          //         var i = $input.closest(".form-group").find(".input-group-addon .switcher");
          //         var $input2 = $('[data-id="'+id+'"]');
      
          //         if (i.parent().data("original-title")) {
          //             i.parent().tooltip("destroy");
          //         }
          //         if ($(this).is(":checked")) {
          //             i.removeClass("c-gray-light").parent().addClass("bg-blue");
          //             if (i.attr("data-icon-checked")) {
          //                 i.addClass(i.attr("data-icon-checked"));
          //             }
          //             if (i.attr("data-icon-unchecked")) {
          //                 i.removeClass(i.attr("data-icon-unchecked"));
          //             }
      
          //             i.parent().attr("title", basetitle + " : OUI").myTooltip();
          //             if ($(this).attr("id").indexOf("IsPrivate")>0) {
          //                 $input.addClass("private");
          //                 $input2.addClass("private");
          //             }
          //         }
          //         else {
          //             i.addClass("c-gray-light").parent().removeClass("bg-blue");
          //             if (i.attr("data-icon-checked")) {
          //                 i.removeClass(i.attr("data-icon-checked"));
          //             }
          //             if (i.attr("data-icon-unchecked")) {
          //                 i.addClass(i.attr("data-icon-unchecked"));
          //             }
          //             i.parent().attr("title", basetitle + " : NON").myTooltip();
          //             if ($(this).attr("id").indexOf("IsPrivate")>0) {
          //                 $input.removeClass("private");
          //                 $input2.removeClass("private");
          //             }
      
          //         }
          //     });
          //     window.setTimeout(function () {
          //         if ($checkbox) $checkbox.change();
          //     }, 500);
          //     $this.parent().click(function () {
          //         var $checkbox = $("#"+$(this).data('checkbox'));
          //         $checkbox.prop("checked", !$checkbox.prop("checked")).change();
          //     });
          //     $checkbox.closest(".form-group").hide();
          // });
      });
    </script>
    <div class="tab-pane fade breaklocalisation" id="form_property_localisation">
      <!-- <div class="alert alert-danger hidden" id="alert-no-city">
        <h4>City not defined</h4>
        <p>
          Vous n'avez pas défini de city pour ce property. Une city est recommandée pour avoir une visibilité optimale sur les sites internet.        
        </p>
        <p>
          <a href="#anchorlocalisation" class="btn btn-sm btn-default"><i class="far fa-building"></i> Define the city</a>
        </p>
      </div> -->
      <!-- <div class="alert alert-danger hidden" id="alert-no-localisation">
        <h4>Location not defined</h4>
        <p>
          Vous n'avez pas défini de localisation pour ce property. Une localisation (approximative ou précise) est recommandée pour avoir une visibilité optimale sur les sites internet.        
        </p>
        <p>
          <a href="#anchorlocalisation" class="btn btn-sm btn-default"><i class="fas fa-map-marker-alt"></i> Define the location</a>
        </p>
      </div> -->
      <!-- <div class="alert alert-danger hidden" id="alert-no-district-monaco">
        <h4>district non défini</h4>
        <p>
          Vous n'avez pas défini de district pour ce property localisé à Monaco. Un district est recommandé pour avoir une visibilité optimale sur les sites internet.        
        </p>
        <p>
          <a href="#anchorlocalisation" class="btn btn-sm btn-default"><i class="fas fa-map-marker-alt"></i> Définir le district</a>
        </p>
      </div> -->
      <div class="row">
        <div class="col-sm-6 col-md-4">
          <div id="localisation">
            <div class="form-group  ">
              <div class="form-group-line">
                <label class="control-label" for="property_country">Country</label>
                <div class="input-elt input-elt-select ">
                  <div class="input-group">
                    <select id="property_country" name="property_country" data-live-search="data-live-search" data-show-subtext="data-show-subtext" data-live-search-normalize="data-live-search-normalize" class=" form-control form-control" data-container="body">

                      <?php $country_name =  countryName($property_byid['country']);
                                            
                        if(isset($allcountry) && $allcountry != 0){
                          while ($country = mysqli_fetch_assoc($allcountry)) { 
                              ?>
                               <option value="<?=$country['country_id']?>" data-code="<?=$country['iso_code']?>" <?php if($country_name['num_code'] == $country['num_code']) { echo "selected='selected' id='test'";}?>><?=$country['country_nicename']?></option>
                          <?php
                          }

                         }?> 
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <?php //if(isset($property_byid['building_id']) && $property_byid['building_id'] !='' &&  $property_byid['building_id'] != '0') {?>
            <div class="group-building">
              <div class="form-group  ">
                <div class="form-group-line">
                  <label class="control-label" for="property_building">Building</label>
                  <div class="input-elt input-elt-select has-addons">
                    <div class="input-group">
                      <select id="property_building" name="property_building" data-live-search="data-live-search" _data-city="45"  class=" form-control form-control" data-container="body">
                        <option value="<?php echo $property_byid['building_id'];?>" selected=""><?php echo $property_byid['building_id'];?></option>
                      </select>
                      <div class="input-group-addon"><i class="switcher fa fa-unlock-alt c-gray-light" data-icon-unchecked="fa-unlock-alt" data-icon-checked="fa-lock" title="Private information" data-checkbox="#property_buildingIsPrivate"></i></div>
                      <div class="input-group-addon"><i class="infosbuilding fa fa-info-circle" title="Building details" data-rel="tooltip"></i></div>
                      <div class="input-group-addon"><i class="propertysbuilding fa fa-search" title="Search properties in this building" data-rel="tooltip"></i></div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group hidden">
                <div class="form-group-line">
                  <label class="control-label" for="property_building_other">New Building Name</label>
                  <div class="input-elt ">
                    <div class="input-group"><input type="text" id="property_building_other" name="property_building_other" class=" form-control" /></div>
                  </div>
                </div>
              </div>
            
             <!--  <div class="form-group  ">
                <div class="form-group-line">
                  <label class="control-label" for="property_buildingIsPrivate">building is private</label>
                  <div class="input-elt input-checkbox ">
                    <div class="input-group"><input type="checkbox" id="property_buildingIsPrivate" name="property[buildingIsPrivate]" class=" form-control" value="1" /></div>
                  </div>
                </div>
              </div> -->
            </div>
            <?php// }?>
            <div class="group-address">
              <div class="form-group  ">
                <div class="form-group-line">
                  <label class="control-label" for="property_address">Property address</label>
                  <div class="input-elt has-addons">
                    <div class="input-group">
                      <textarea id="property_address" name="property_address" rows="1"  class="private form-control"><?php echo $property_byid['address'];?></textarea>
                      <div class="input-group-addon"><i class='fa fa-lock' title='Private information' data-rel='tooltip'></i></div>
                      <div class="input-group-addon"><i class="switcher switcher-manual fa fa-fw fa-map-pin c-gray-light" data-icon-unchecked="fa-magic" data-icon-checked="fa-map-pin" title="address manuelle" data-checkbox="#manual"></i></div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="hidden"><input type="checkbox" id="manual"></div>
            </div>
            <div class="group-details">
             <!--  <div class="input-elt input-elt-select ">
                <div class="input-group">
                  <select id="property_codepostal" name="property_codepostal" class="hidden form-control form-control" data-container="body">
                    <option value=""></option>
                  </select>
                </div>
              </div> -->
              <div class="form-group  ">
                <div class="form-group-line">
                  <label class="control-label" for="property_codepostal_other">Zip</label>
                  <div class="input-elt ">
                    <div class="input-group"><input type="text" id="property_codepostal_other" name="property_codepostal_other" readonly="readonly" value="<?php echo $property_byid['postalcode'];?>" class=" form-control" /></div>
                  </div>
                </div>
              </div>
              <div class="input-elt input-elt-select ">
                <div class="input-group">
                  <select id="property_city" name="property_city" class="hidden form-control form-control" data-country="" data-container="body">
                    
                  </select>
                </div>
              </div>
              <div class="form-group  ">
                <div class="form-group-line">
                  <label class="control-label" for="property_city_other">City</label>
                  <div class="input-elt ">
                    <div class="input-group"><input type="text" id="property_city_other" name="property_city_other" readonly="readonly" readonly="readonly" class=" form-control" value="<?php echo $property_byid['city'];?>"/></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="group-district">
              <div class="form-group  ">
                <div class="form-group-line">
                  <label class="control-label" for="property_district">District</label>
                  <div class="input-elt input-elt-select ">
                    <div class="input-group">
                      <select id="property_district" name="property_district"  data-city="" class=" form-control form-control" data-container="body">
                        <?php $district_name = districtName($property_byid['district']);?>
                        <option value="<?php echo $property_byid['district'];?>"><?php echo $district_name['district_name'];?></option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <!-- <div class="form-group  ">
                <div class="form-group-line">
                  <label class="control-label" for="property_district_other">New district</label>
                  <div class="input-elt has-addons">
                    <div class="input-group">
                      <input type="text" id="property_district_other" name="property_district_other"  class=" form-control" />
                      <div class="input-group-addon">
                        <div title="Entrez ici le nom d'un district pour le créer" data-rel="tooltip"><i class="fa fa-info"></i></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div> -->
            </div>
            <div class="group-administrative">
              <div class="form-group">
                <div class="form-group-line">
                  <label class="control-label" for="property_administrative2">Departement</label>
                  <div class="input-elt ">
                    <div class="input-group"><input type="text" id="property_administrative2" name="property_department" readonly="readonly" readonly="readonly" class=" form-control" /></div>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="form-group-line">
                  <label class="control-label" for="property_administrative1">Region</label>
                  <div class="input-elt ">
                    <div class="input-group"><input type="text" id="property_administrative1" name="property_region" readonly="readonly" readonly="readonly" class=" form-control" value="<?php echo $property_byid['region'];?>" /></div>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="form-group  ">
              <div class="form-group-line">
                <label class="control-label" for="property_additionallocation">Additional location information</label>
                <div class="input-elt ">
                  <div class="input-group"><input type="text" id="property_additionallocation" name="property_additionallocation" maxlength="255" class=" form-control" /></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-8">
          <div class="approxSwitch">
            <div class="form-group">
              <label class="btn-block" for="property_approxBool">
                 <input type="checkbox" id="property_approxBool" name="property_locationapprox" class=" form-control" value="1" />Location Approx
              </label>
          	</div>
          </div>

          <div class="row">
            <div class="col-xs-12 col-sm-6 mainmap">
              <div id="map" style="height: 300px;"></div>
              <div class="row">
                <div class="col-sm-4 noapprox">
                  <div class="form-group  ">
                    <div class="form-group-line">
                      <div class="input-elt ">
                        <div class="input-group"><input type="text" id="property_lat" name="property_lat" readonly="readonly" class=" form-control" value="<?php echo $property_byid['latitude'];?>" /></div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-sm-4 noapprox">
                  <div class="form-group  ">
                    <div class="form-group-line">
                      <div class="input-elt ">
                        <div class="input-group"><input type="text" id="property_lng" name="property_lng" readonly="readonly" class=" form-control" value="<?php echo $property_byid['longitude'];?>"/></div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-sm-4 inapprox">
                  <div class="form-group  ">
                    <div class="form-group-line">
                      <div class="input-elt ">
                        <div class="input-group"><input type="text" id="property_approxLat" name="property_approxLat" readonly="readonly" class=" form-control" /></div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-sm-4 inapprox">
                  <div class="form-group  ">
                    <div class="form-group-line">
                      <div class="input-elt ">
                        <div class="input-group"><input type="text" id="property_approxLng" name="property_approxLng" readonly="readonly" class=" form-control" /></div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-sm-4 inapprox"></div>
                <div class="col-sm-4">
                  <div class="form-group  ">
                    <div class="form-group-line">
                      <div class="input-elt ">
                        <div class="input-group"><input type="text" id="property_zoom" name="property_zoom" readonly="readonly" class=" form-control" /></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 mainpano">
              <div id="pano"></div>
              <div id="other_streetview" title="Utiliser cette address" data-toggle="tooltip"></div>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group  ">
                    <div class="form-group-line">
                      <div class="input-elt ">
                        <div class="input-group"><input type="text" id="property_heading" name="property_heading" readonly="readonly" class=" form-control" /></div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group  ">
                    <div class="form-group-line">
                      <div class="input-elt ">
                        <div class="input-group"><input type="text" id="property_pitch" name="property_pitch" readonly="readonly" class=" form-control" /></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
   
    <div class="tab-pane fade" id="form_property_textes">
      <!-- <div id="help_languessupp" class="help collapse in" role="tabpanel" aria-labelledby="switch_languessupp">
        <div class="clearfix"></div>
        <div class="alert alert-info alert-dismissible" role="alert">
          <button type="button" data-href="/agent/helpview/languessupp/1/" class="btn close getScript"  data-toggle="collapse" href="#help_languessupp" title="Close" data-rel="tooltip"><span aria-hidden="true"><i class="fa fa-times"></i></span><span class="sr-only"> Close</span></button>
          <div class="help-content">
            <h4><i class="fa fa-info-circle"></i> Need other languages?</h4>
            <p>
              <a href="mailto:support@immotoolbox.com">Contact Us</a> to enable additional languages for entering your products agency.
            </p>
          </div>
        </div>
      </div> -->
      <!-- <div class="help-toggle pull-right">
        <button type="button" data-href="/agent/helpview/languessupp/0/" data-toggle="collapse" href="#help_languessupp" class="btn btn-sm btn-link getScript" title="Help: Need other languages?" data-rel="tooltip"><i class="fa fa-lg fa-info-circle"></i></button>
      </div> -->
      <!-- <div class="alert alert-danger alert-retrocession hidden">
        <h4>Rétrocession</h4>
        <p>
          S'il n'y a pas de rétrocession pour ce property, merci d'utiliser la case "<em>Pas de rétrocession</em>" de l'onglet <a href="#form_property_general" data-toggle="tab">Général</a> pour le spécifier plutôt que dans le texte.            
        </p>
      </div> -->
      <div class="property_textes_list">
      <h5>Title / Ad title <small>(50 characters max.)</small></h5>
        <div class="row">
          <div class="col-md-12">              
              <div class="form-group">
                <input type="text" id="property_text_title" name="property_text_title" spellcheck="true" maxlength="50"  class=" form-control" value="<?=(isset($property_byid['property_title']) && $property_byid['property_title'] != '')?$property_byid['property_title']:''?>"/>
              </div>
          </div>
        </div>

      <h5>Excerpt <small>(1000 characters max.)</small></h5>
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <textarea id="property_text_excerpt" name="property_text_excerpt" rows="3" spellcheck="true"  class=" form-control"><?=(isset($property_byid['property_excerpt']) && $property_byid['property_excerpt'] != '')?$property_byid['property_excerpt']:''?></textarea>
            </div>
          </div>
        </div>
       
      <h5>Description</h5>
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
                <textarea id="property_text_description" name="property_text_description" rows="6"  spellcheck="true"  class=" form-control"><?=(isset($property_byid['property_description']) && $property_byid['property_description'] != '')?$property_byid['property_description']:''?></textarea>
            </div>
          </div>
        </div>
      <!-- <script>
        function checkRetrocession ()
        {
            var $hasRetrocession = $("#form_property_textes textarea").filter(function () { return $(this).val().indexOf("trocession")>=0; });
            if ($hasRetrocession.length) {
                $(".alert-retrocession").removeClass("hidden").trigger("classChanged");
            }
            else {
                $(".alert-retrocession").addClass("hidden").trigger("classChanged");
            }
        
        }
        $("#form_property_textes textarea").change(function () { checkRetrocession(); });
        checkRetrocession();
        
      </script> -->
      <div class="clearfix"></div>
    </div>
  </div>
    <div class="tab-pane fade" id="form_property_images">
      <div id="help_imagesproperty" class="help collapse in" role="tabpanel" aria-labelledby="switch_imagesproperty">
        <div class="clearfix"></div>
        <div class="alert alert-info alert-dismissible" role="alert">
          <button type="button" data-href="/agent/helpview/imagesproperty/1/" class="btn close getScript"  data-toggle="collapse" href="#help_imagesproperty" title="Close" data-rel="tooltip"><span aria-hidden="true"><i class="fa fa-times"></i></span><span class="sr-only"> Close</span></button>
          <div class="help-content">
            <h4><i class="fa fa-info-circle"></i> Product Pictures</h4>
            <p>
              The first active picture below will be used as a thumbnail in the products listings
            </p>
          </div>
        </div>
      </div>
      <div class="help-toggle pull-right">
        <button type="button" data-href="/agent/helpview/imagesproperty/0/" data-toggle="collapse" href="#help_imagesproperty" class="btn btn-sm btn-link getScript" title="Help: Product Pictures" data-rel="tooltip"><i class="fa fa-lg fa-info-circle"></i></button>
      </div>
      <h4>Product Pictures</h4>
      <div class="btn-group">
        <span class="btn btn-default btn-label">Add Images :</span>
        <button type="button" href="/image/newmult/?property=52709" data-toggle="modal" data-target="#modal_lg" class="btn btn-default" title="Send pictures from your computer" data-rel="tooltip"><i class="fas fa-cloud-upload-alt"></i> New</button>
        <button type="button" href="/image/embed?property=52709&ajax=1" data-toggle="modal" data-target="#modal_full" class="btn btn-default" title="Use pictures already sent" data-rel="tooltip"><i class="fas fa-cloud-download-alt"></i> Existing</button>
        <button type="button" href="/image/embed?property=52709&ajax=1" data-toggle="modal" data-target="#modal_full" class="btn btn-default btn-images-building" title="Use pictures  related to the building" data-rel="tooltip"><i class="fa fa-building"></i> Building</button>
      </div>
      <div class="row property_images_list m-t-10" data-item-class="col-lg-2 col-md-3 col-sm-4 col-xs-6">
      </div>
      <div class="alert alert-no-image alert-warning fade in hidden">
        <p><strong>Still no picture</strong></p>
      </div>
      <h4>Other pictures</h4>
      <div class="row">
        <div class="col-sm-6">
          <div class="form-group  ">
            <div class="form-group-line">
              <label class="control-label" for="property_urlVisiteVirtuelle">URL Virtual visit</label>
              <div class="input-elt ">
                <div class="input-group"><input type="text" id="property_urlVisiteVirtuelle" name="property[urlVisiteVirtuelle]" maxlength="255" placeholder="http://" class=" form-control" /></div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group  ">
            <div class="form-group-line">
              <label class="control-label" for="property_urlVideo">URL Video</label>
              <div class="input-elt ">
                <div class="input-group"><input type="text" id="property_urlVideo" name="property[urlVideo]" maxlength="255" placeholder="http://" class=" form-control" /></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <style>
      .ui-state-highlight { height: 1.5em; line-height: 1.2em;background: red; }
    </style>
  
    
   
  </div>
   <div class="saveline">
    <!-- <div class="btn-group m-r-10">
      <span class="modified label label-warning label-updated hidden" title="Modified property" data-rel="tooltip">
      <i class="fa fa-exclamation" title="Edited"></i>
      <span class="hidden-sm hidden-xs">Edited</span>
      </span>
    </div> -->
    <div class="btn-group">
      <button type="submit" id="property_save" name="property_save" title="Save" data-rel="tooltip" class="btn btn-primary"><i class="fa fa-check-square"></i> <small class="hidden-xs"> Save Changes</small></button>
     <!--  <button type="submit" id="property_save_and_close" name="property[save_and_close]" title="Save and close" data-rel="tooltip" class="btn btn-primary"><i class="far fa-save"></i> <small class="hidden-xs"> Save and close</small></button>
      <button type="reset" data-href="/property/?searchproperty%5BtypeTransaction%5D=Sale&amp;searchproperty%5BisActive%5D=0" class="btn btn-reset" title="Cancel" data-rel="tooltip"><i class="fas fa-times"></i> <small class="hidden-xs">Cancel</small></button> -->
    </div>
  </div>
</form>
           </div>
        </div>
      </div>
    </div>
  </div>
</div>
 <script type="text/javascript">
    jQuery(function ($) {
      $.getScript('https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js', function () {
          $("input[type='checkbox']").checkboxradio();
        });
      });
    
 </script>
 <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCGmRiNH9b_Ts8meG7fgcH2rhUYy7-FCqY&libraries=places&language=en"></script>
 <script src="dist/js/location.js"></script>

 <style type="text/css">
   .input-group{
    width: 100%;
   }
 </style>

<?php include('footer.php');?>

