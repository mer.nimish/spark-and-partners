<?php 
session_start();
require 'config/config.php';
require 'model/model.php';
global $conn;


if (isset($_SESSION['first_name']) && isset($_SESSION['last_name'])) {
    $first_name = $_SESSION['first_name'];
    $last_name = $_SESSION['last_name'];
} else {
    echo "<script>window.location='index.php'</script>";
    exit;
}

$avgsalepriceforpersqm = getavgpriceforsale();
$avgrentpriceforpersqm = getavgpriceforrent();

include('header.php');
include('left_sidebar.php');

?>
		<div class="page-wrapper">
            <div class="container-fluid pt-25">				
				<!-- Row -->
                <div class="row">
                        
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                        <div class="panel panel-default card-view connection">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-dark">Connections</h6>
                                </div>
                                <div class="pull-right">
                                    <a href="#" class="pull-left inline-block close-panel" data-effect="fadeOut">
                                        <i class="zmdi zmdi-close"></i>
                                    </a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div>
                                        <table class="table table-striped table-condensed table-hover table-sorter">
                                            <thead>
                                                <tr>
                                                    <th width="100%" class="text-center pa-5">Name</th>
                                                    <th class="text-center pa-5">Date</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr id="agent_782">
                                                    <td title="" data-rel="tooltip" class="hasTooltip pa-5" data-original-title="Rachel Fisher">
                                                        @spark-estate
                                                    </td>

                                                    <td class="text-center pa-5">
                                                        <span class="badge">
                                                            2018-10-11
                                                        </span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div> 
                                <div class="panel-footer pb-5">
                                    <a href="view_manage_agent.php" class="btn btn-primary btn-sm btn-block">Manage agents</a>
                                </div> 
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                        <div class="panel panel-default card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-dark">Quick search</h6>
                                </div>
                                <div class="pull-right">
                                    <a href="#" class="pull-left inline-block close-panel" data-effect="fadeOut">
                                        <i class="zmdi zmdi-close"></i>
                                    </a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <form method="POST" name="searchform" class="propsearchform" action="view_search_property.php">
                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body">
                                        <div>
                                            <div class="form-group">
                                                <input class="form-control" name="searchdata" id="searchdata" placeholder="Reference" type="text" required>
                                            </div>
                                        </div>
                                    </div>  
                                    <div class="panel-footer pb-5">
                                        <!-- <a href="#" class="btn btn-primary btn-sm btn-block">Search</a> -->
                                        <input type="submit" class="btn btn-primary btn-sm btn-block" value="Search">
                                    </div> 
                                </div>
                            </form>    
                        </div>
                    </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                        <div class="panel panel-default card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-dark">Market stats</h6>
                                </div>
                                <div class="pull-right">
                                    <a href="#" class="pull-left inline-block close-panel" data-effect="fadeOut">
                                        <i class="zmdi zmdi-close"></i>
                                    </a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div>
                                        <div class="btn-group btn-group-justified btn-group-full-height">
                                            <a href="#" class="btn btn-primary padding-sm number vente hasTooltip" title="" data-rel="tooltip" data-original-title="Average : 1193 produits">
                                            <strong class="block"><?= round($avgsalepriceforpersqm, 2);?> <span class="c-gray-light">€/sqm</span></strong>
                                            <small>Sales</small>
                                            </a>
                                            <a href="" class="btn btn-primary padding-sm number location hasTooltip" title="" data-rel="tooltip" data-original-title="Average : 485 produits">
                                            <strong class="block"><?= round($avgrentpriceforpersqm, 2);?> <span class="c-gray-light">€/sqm</span></strong>
                                            <small>Rentals</small>
                                            </a>
                                        </div>
                                    </div>
                                </div>  
                            </div>
                        </div>
                        
                    </div>
                    
                <!-- /Row -->
			</div>
		
<?php include('footer.php');?>