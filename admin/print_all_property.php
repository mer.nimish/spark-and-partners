<?php 
require 'config/config.php';
require 'model/model.php';
global $conn;


if (isset($_POST['temppropdata']) && !empty($_POST['temppropdata'])) {
    
    $filterarrydata = array_filter($_POST['temppropdata']);    
    $allpropid = implode(",", $filterarrydata);   
    $query = "SELECT * FROM tbl_property WHERE property_id IN (".$allpropid.")";    
    $res = mysqli_query($conn, $query);
    $html = '';
            if (mysqli_num_rows($res) > 0) { 
                while ($value = mysqli_fetch_array($res)) {                
                      
                $html .= '<body>
                    <div class="spark_main_container">
                        <div class="topsection">
                            <p>'.$value['property_title'].'</p>
                            <p>'.$value['city'].'</p>
                            <img src="../img/prop-1.jpg" alt="Property1" width="320px" style="border: 3px solid #000;">
                        </div>';

                if($value['property_description'] != ''){    
                    $html .= '<div class="description">
                        <span>DESCRIPTION<span>
                        <p>'.$value['property_description'].'</p>
                    </div>';
                }

                  $html .= '<div class="areadetails">
                            <span>DETAILS<span>
                            <hr class="detail_border">
                            <div class="col-md-3 areadiv">';
                if($value['total_area'] != ''){
                    $html .= '<p class="arealabel">Total Area:</p>
                            <p class="areaval">'.$value['total_area'].' m<sup>2</sup></p>';
                }
                if($value['terrace_area'] != ''){
                    $html .= '<p class="arealabel">Terrace Area:</p>
                            <p class="areaval">'.$value['terrace_area'].' m<sup>2</sup></p>';
                }   
                    $html .= '</div><div class="col-md-3 areadiv">';

                if($value['num_rooms'] != ''){
                    $html .= '<p class="arealabel">Type:</p>
                                <p class="areaval">'.$value['num_rooms'].' Rooms</p>';
                }
                if($value['num_cellars'] != ''){
                    $html .= '<p class="arealabel">Cellar:</p>
                                <p class="areaval">'.$value['num_cellars'].'</p>';
                }   
                    $html .= '</div><div class="col-md-3 areadiv">';
                
                if($value['num_bedrooms'] != ''){
                    $html .= '<p class="arealabel">Bedroom:</p>
                                <p class="areaval">'.$value['num_bedrooms'].' </p>';
                } 
                if($value['building_name'] != ''){
                    $html .= '<p class="arealabel">Building:</p>
                                <p class="areaval">'.$value['building_name'].'</p>';
                }                                                   
                    $html .= '</div><div class="col-md-3 areadiv">';            
                 
                if($value['num_bathrooms'] != ''){
                    $html .= '<p class="arealabel">Bathroom:</p>
                                <p class="areaval">'.$value['num_bathrooms'].'</p>';
                } 

                if($value['district_name'] != ''){
                    $html .= '<p class="arealabel">Situation:</p>
                                <p class="areaval">'.$value['district_name'].'</p>';
                }                                           
                    $html .= '</div>
                           </div><hr class="price_border">';

                if($value['price'] != ''){
                    $html .= '<div class="asked_price">
                            <p>ASKED PRICE: <span>'.$value['price'].'</span> EUROS</p>
                        </div>';
                }               
                    $html .= '<div class="benefits">
                            <span>BENEFITS</span>
                            <div class="benefits_div">
                                <ul>
                                    <li>Centrally Located</li>                      
                                </ul>   
                            </div>
                            <div class="benefits_div">
                                <ul>                        
                                    <li>Supermarket Nearby</li>                     
                                </ul>   
                            </div>
                            <div class="benefits_div">
                                <ul>                        
                                    <li>Fully Renovated</li>
                                </ul>   
                            </div>                               
                        </div>
                        <div class="prop_images">
                            <div class="property_images" style="width: 70%;margin: 0 auto;padding: 30px 0px 20px 0px;">
                                <img src="../img/prop-dt-1.jpg" style="border: 2px solid #000;">
                            </div>
                           <div class="property_images" style="width: 70%;margin: 0 auto;padding: 20px 0px;">
                                <img src="../img/prop-dt-2.jpg" style="border: 2px solid #000;">
                            </div>
                            <div class="property_images" style="width: 70%;margin: 0 auto;padding: 20px 0px;">
                                <img src="../img/prop-dt-3.jpg" style="border: 2px solid #000;">
                            </div>
                        </div>  
                    </div>
                </body>';
            }
             

        //==============================================================
        //==============================================================
        //==============================================================
        include("../mpdf/mpdf.php");

        //$mpdf=new mPDF('en-GB-x','A4','','',0,0,0,0,0,3); 
        $mpdf=new mPDF('en-GB-x','A4','','',0,0,33,32,0,-5); 

        $mpdf->SetTitle('Selection-Property-Data');

        $mpdf->SetHTMLHeader('<div class="header" style="float: left; width: 100%;padding: 10px 20px 0px 20px;">
                                <div class="logo" style="text-align:center;">
                                    <img src="../img/logopdf.jpg" />
                                </div>                        
                            </div>
                            <div class="headerborder">
                                <hr class="header_border">
                            </div>');

        $mpdf->SetHTMLFooter('<div class="headerborder">
                                <hr class="header_border">
                            </div>
                            <div class="footer" style="float: left;width: 100%;padding: 0px 20px 40px 20px;">
                               <p>SPARK & PARTNERS SARL – 57 rue Grimaldi 98000, MONACO</p>
                               <p>E: <a href="mailto:hello@spark-estate.com">hello@spark-estate.com</a> - T: <a href="tel:+37797777000">+377 97 77 70 00</a></p>
                               <p>Société au Capital de 150.000 euros - RC I: 15S06776 – TVA: FR 88 000117654</p>
                            </div>');

        $mpdf->SetDisplayMode('fullpage');

        $mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list

        // LOAD a stylesheet
        $stylesheetfont = file_get_contents('dist/css/font-awesome.min.css');
        $mpdf->WriteHTML($stylesheetfont,1);
        $stylesheetbootstrap = file_get_contents('dist/css/bootstrap.min.css');
        $mpdf->WriteHTML($stylesheetbootstrap,1);
        $pdf_style = file_get_contents('dist/css/pdf_style.css');
        $mpdf->WriteHTML($pdf_style,1);

        $mpdf->WriteHTML($html,2);

        $mpdf->Output('all_selected_property_sparkandpartners.pdf','I');

        exit;
        } 
        else{
            echo "<script>window.location='dashboard.php'</script>";
            exit;
        }

}else{
    echo "<script>window.location='dashboard.php'</script>";
    exit;
}




?>