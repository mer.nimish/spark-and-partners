<?php 
session_start();
require 'config/config.php';
require 'model/model.php';
global $conn;


if (isset($_SESSION['first_name']) && isset($_SESSION['last_name'])) {
    $first_name = $_SESSION['first_name'];
    $last_name = $_SESSION['last_name'];
} else {
    echo "<script>window.location='index.php'</script>";
    exit;
} 

$allcountry = getAllCountry();
$allcity =getAllCity();

if(isset($_REQUEST['building_savebtn'])) {

    $last_inserted_id = addBuilding($_REQUEST);
    if($last_inserted_id !== '' && isset($last_inserted_id)){
        //header('location: add_building.php');
      $successMsg = 'Building inserted successfully.';
    }
    else{
        $errorOtherMsg = 'Opps! Somthing went wrong.';
        $requestdata = $_REQUEST;
    }

}
include('header.php');
include('left_sidebar.php');

?>
 		<div class="page-wrapper">
            <div class="container-fluid pt-25">	
                    <div class="col-md-12">
                         <?php if(isset($successMsg)){ ?>
                          <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <i class="zmdi zmdi-check pr-15 pull-left"></i>
                            <p class="pull-left"><?=$successMsg?></p>
                            <div class="clearfix"></div>
                          </div>
                          <?php } ?>
                        <?php if(isset($errorOtherMsg)){ ?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <i class="zmdi zmdi-block pr-15 pull-left"></i><p class="pull-left"><?=$errorOtherMsg?></p>
                                <div class="clearfix"></div>
                            </div>
                        <?php } ?>
                    </div>
                			
				<!-- Title -->
                    <div class="row heading-bg">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <h5 class="txt-dark">New Building</h5>
                        </div>
                    </div>
                    <!-- /Title -->
                    
                    <!-- Row -->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default card-view">
         
            <div class="panel-wrapper collapse in">
                <div class="panel-body">    
               
                <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCGmRiNH9b_Ts8meG7fgcH2rhUYy7-FCqY&libraries=places&language=en"></script>

              

                    <form name="building" method="post" action="" id="building_form">
                       <div class="row">
                        <div class="col-md-6">
                           <div class="form-group  ">
                              <div class="form-group-line">
                                 <label class="control-label required" for="building_name">Name</label>
                                 <div class="input-elt ">
                                    <input type="text" id="building_name" name="building_name" required="required" maxlength="255" class=" form-control" />
                                 </div>
                              </div>
                           </div>
                        

                            <div id="localisation">
                               <div class="form-group  ">
                                  <div class="form-group-line">
                                     <label class="control-label required" for="country">Country</label>
                                     <div class="input-elt input-elt-select ">
                                       
                                          <select id="country" name="country" class="form-control" data-container="body" data-live-search="true">
                                           <?php if(isset($allcountry) && $allcountry != 0){
                                              while ($country = mysqli_fetch_assoc($allcountry)) { ?>
                                                  <option value="<?=$country['country_id']?>" data-code="<?=$country['iso_code']?>"><?=$country['country_nicename']?></option>
                                            <?php
                                              }
                                             }?> 
                                        </select>
                                      
                                     </div>
                                  </div>
                               </div>
                            

                             <div class="form-group  ">
                                <div class="form-group-line">
                                    <label class="control-label" for="city">city</label>
                                    <div class="input-elt ">
                                        <select id="city" name="city" data-pays="" class="form-control" data-container="body" data-container="body" data-live-search="true">
                                        	
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group  ">
              							   <div class="form-group-line">
              							      <label class="control-label" for="city_other">New city</label>
              							      <div class="input-elt has-addons">
              							         <div class="input-group">
              							            <input type="text" id="city_other" name="city_other"  class=" form-control" />
              							            <div class="input-group-addon">
              							               <div title="Entrez ici le nom d'une ville pour la créer" data-rel="tooltip"><i class="fa fa-info"></i></div>
              							            </div>
              							         </div>
              							      </div>
              							   </div>
              							</div>
                            <div class="form-group  ">
                                <div class="form-group-line">
                                    <label class="control-label" for="city">District</label>
                                    <div class="input-elt ">
                                      <select id="district" name="district" class="form-control" data-container="body" data-live-search="true">
                                           
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group  ">
              							   <div class="form-group-line">
              							      <label class="control-label" for="district_other">New district</label>
              							      <div class="input-elt has-addons">
              							         <div class="input-group">
              							            <input type="text" id="district_other" name="district_other"  class=" form-control" />
              							            <div class="input-group-addon">
              							               <div title="Entrez ici le nom d'un quartier pour le créer" data-rel="tooltip"><i class="fa fa-info"></i></div>
              							            </div>
              							         </div>
              							      </div>
              							   </div>
              							</div>
                            <div class="form-group  ">
                                <div class="form-group-line">
                                   <label class="control-label" for="address">Address</label>
                                   <div class="input-elt ">
                                     <textarea id="address" name="address" rows="2" class=" form-control"></textarea>
                                   </div>
                                </div>
                             </div>
                                                       
                           	</div>
                             <div class="form-group">
                                <div class="form-group-line">
                                   <label class="control-label"> </label>
                                   
                                      <div class="input-elt">
                                         <button type="button" class="btn btn-default btn-sm btn-block btn-geocode">
                                         <i class="fa fa-search"></i>
                                         Search for this address on the map
                                         </button>
                                      </div>
                                   
                                </div>
                             </div>
                          </div>
                          <div class="col-md-6">
                            <div class="row">
                             <div class="padding-0">
                                <div id="map" style="width: 100%; height: 300px;"></div> 
                             </div>
                           </div>
                             <div class="row">
                                <div class="col-sm-6">
                                   <div class="form-group  ">
                                      <div class="form-group-line">
                                         <label class="control-label" for="latitude">Latitude</label>
                                         <div class="input-elt ">
                                            <div class="input-group"><input type="text" id="latitude" name="latitude" readonly="readonly" readonly="readonly" class=" form-control" /></div>
                                         </div>
                                      </div>
                                   </div>
                                </div>
                                <div class="col-sm-6">
                                   <div class="form-group  ">
                                      <div class="form-group-line">
                                         <label class="control-label" for="longitude">Longitude</label>
                                         <div class="input-elt ">
                                            <div class="input-group"><input type="text" id="longitude" name="longitude" readonly="readonly" readonly="readonly" class=" form-control" /></div>
                                         </div>
                                      </div>
                                   </div>
                                </div>
                             </div>
                          </div>
                       </div>
                       
                       <div class="form-group  ">
                          <div class="form-group-line">
                             <label class="control-label" for="ranking">Ranking</label>
                             <div class="input-elt ">
                                <input type="text" id="ranking" name="ranking" maxlength="255" class=" form-control" />
                             </div>
                          </div>
                       </div>
                       <div class="form-group  ">
                          <div class="form-group-line">
                             <label class="control-label" for="floors">Floors</label>
                             <div class="input-elt ">
                               <input type="number" id="floors" name="floors" class=" form-control" />
                             </div>
                          </div>
                       </div>
                       <div class="form-group  ">
                          <div class="form-group-line">
                             <label class="control-label" for="immeuble_hauteur">Height</label>
                             <div class="input-elt ">
                                <input type="number" id="height" name="height" class=" form-control" />
                             </div>
                          </div>
                       </div>
                       <div class="form-group  ">
                          <div class="form-group-line">
                             
                              <label class="btn-block" for="swimming_pool">
                                   <input type="checkbox" id="swimming_pool" name="swimming_pool" class=" form-control" value="1" /> Swimming pool
                              </label>  
                          </div>
                       </div>
                      
                       <div class="saveline">
                          <button type="submit" id="building_savebtn" name="building_savebtn" class="btn btn-primary">Save</button>
                       </div>
                    </form>        
                      
                </div>
            </div>
        </div>
    </div>
</div>

<?php include('footer.php');?>

<script type="text/javascript">
  $("#localisation #country").change(function () {
    $('#city').attr('data-pays',$('#country').val());
                setupCity();
            }).change();

  $("#localisation #city").on("change",function () {
    //alert("hi");
            if ($(this).find("option:selected").hasClass("new")) {
                $("#city_other").closest(".form-group").show();
            }
            else {
                $("#city_other").closest(".form-group").hide();
            }
            setupDistrict();
      });
   $("#localisation #district").change(function () {
                if ($(this).find("option:selected").hasClass("new")) {
                    $("#district_other").closest(".form-group").show();
                }
                else {
                    $("#district_other").closest(".form-group").hide();
                }
            })
      
         function setupCity ()
        {
            if (!$("#country").val()) {
                $("#city").closest(".form-group").hide();
                $("#city").val("").selectpicker("render").change();
            }
            else {
                //if ($("#city").attr("data-pays") == $("#country").val()) {
                  //alert("hi");
                      var id = $("#country").val();
                        $.ajax({
                          method: 'post',
                          url: 'ajax/getcity.php',
                          data: {id: id}
                        }) .done(function(data){
              //console.log(data);
                        var val = $("#city").val();
                        $("#city").attr("data-pays",$("#country").val()).find("option").remove();

                        if (!data) {
                            $("#city").closest(".form-group").hide();
                        }
                        else {
                            var cpt= 0,curval=false;
                            $.each( JSON.parse(data), function( k, v ) {
                                cpt++;
                                var curval= v.city_id;
                                $("#city").append("<option value='"+(v.city_id>0?v.city_id:"")+"'>"+ v.city_name+"</option>");
                            });
                            if ($("#localisation #country").val() != 141) {
                                $("#city").append("<option value=''>Other</option>");
                                $("#city").append("<option value='' class='new'>[+] New city</option>");
                            }
                            else if (!val && cpt==1 && curval) {
                                val = curval;
                            }
                            $("#city").selectpicker("refresh");
                            $("#city").closest(".form-group").show();
                        }
                        $("#city").val(val?val:"").selectpicker("render").change();
                    });
                // }
                // else {
                //     //debugloc&&alert('setupVilles : liste déjà à jour');
                //     $("#city").closest(".form-group").show();
                //     $("#city").change();
                // }
            }
        }
        function setupDistrict()
        {
            //debugloc&&alert("setupQuartiers : "+$("#city").val());
            if (!$("#city").val()) {
                //debugloc&&alert('setupQuartiers : no parent');
                $("#district").closest(".form-group").hide();
                $("#district").val("").selectpicker("render").change();
            }
            else {
                //if ($("#district").attr("data-ville") != $("#city").val()) {
                    var id = $('#city').val();
                    $.ajax({
                        method: 'post',
                        url: 'ajax/getdistrict.php',
                        data: {id: id}
                      }) .done(function(data) {
                                  var val = $("#district").val();
                        $("#district").attr("data-ville",$("#city").val()).find("option").remove();

                        if (!data) {
                            //debugloc&&alert('no quartier...');
                            $("#district").closest(".form-group").hide();
                        }
                        else {
                            var cpt= 0,curval=false;
                            $.each( JSON.parse(data), function( k, v ) {
                                cpt++;
                                curval=v.district_id;
                                $("#district").append("<option value='"+(v.district_id>0?v.district_id:"")+"'>"+ v.district_name+"</option>");
                            });

                            if ($("#localisation #country").val() != 1) {
                                $("#district").append("<option value=''>Other</option>");
                                if ($("#localisation #city").val() > 0) {
                                    $("#district").append("<option value='' class='new'>[+] New district</option>");
                                }
                            }
                            else if (!val && cpt==1 && curval) {
                                val = curval;
                            }
                            $("#district").selectpicker("refresh");
                            $("#district").closest(".form-group").show();
                        }
                        if ($("#localisation #country").val() != 1) {
                            $("#district").append("<option value=''>Autre</option>");
                        }

                        $("#district").val(val?val:"").selectpicker("render").change();
                    });
               // }
                // else {
                //     debugloc&&alert('setupQuartiers : liste déjà à jour');
                //     $("#district").closest(".form-group").show();
                //     $("#district").change();
                // }
            }
        }
</script>
<script type="text/javascript">
   $(document).ready(function(){
        initialize();
        codeAddress();
      });
      $(".btn-geocode").click(function () {
        initialize();
        codeAddress();
      });
      var geocoder;
      var map;
      function initialize() {
          geocoder = new google.maps.Geocoder();
          var latlng = new google.maps.LatLng(36.1986623,-113.7990099,);
          var mapOptions = {
            zoom: 10,
            center: latlng
          }
          map = new google.maps.Map(document.getElementById('map'), mapOptions);
      }

      function codeAddress() {
          var e = document.getElementById("country");
          var strUser = e.options[e.selectedIndex].text;
          if(document.getElementById('address').value != ''){
            var address = document.getElementById('address').value .concat(strUser);
          }
          else if(strUser != ''){
            var address = strUser;
          }
          else{
            var address = 'monaco';
          }
      //alert(address);
      geocoder.geocode( { 'address': address}, function(results, status) {
        if (status == 'OK') {
          map.setCenter(results[0].geometry.location);
          var marker = new google.maps.Marker({
              map: map,
              icon: "/sparkandpartners/img/placeholder.png",
              position: results[0].geometry.location
          });

          /*var longitude=results[0].geometry.viewport['j']['j'];
          var latitude=results[0].geometry.viewport['l']['l'];*/

          var longitude = results[0].geometry.location.lng();
          var latitude = results[0].geometry.location.lat();

           $('#latitude').val(latitude);
           $('#longitude').val(longitude);
        } else {
          alert('Geocode was not successful for the following reason: ' + status);
        }
      });
      }
     jQuery(function ($) {
      
          $("input[type='checkbox']").checkboxradio();
      
      });
    </script>