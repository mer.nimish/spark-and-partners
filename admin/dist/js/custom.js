function property_selection() {
    $(document).on('click', '.addtempsel', function(event) {

          var checkselection = $(this).attr('dataselval');
          var propertyid = $(this).attr('datapropval');          
          if(checkselection == 0){
              $(this).attr('dataselval', 1);              
              var selectionval = $("#tempselection").attr('dataval');
              var newselectionval = parseInt(selectionval) + 1;
              $("#tempselection").attr('dataval', newselectionval);
              $("#tempselection").html(newselectionval);
              $("#temppropsel-"+propertyid).html('<i class="fa fa-folder-open"></i>-');
              $("#temppropsel-"+propertyid).removeClass('btn-default');
              $("#temppropsel-"+propertyid).addClass('btn-primary');

              $(".addselectiondata").append('<input type="hidden" name="tempinputdata[]" class="tempdatainput-'+propertyid+'" value="'+propertyid+'">');              
          }else if(checkselection == 1){                           
              var selectionval = $("#tempselection").attr('dataval');
              if(selectionval > 0){
                  var newselectionval = parseInt(selectionval) - 1;
                  $("#tempselection").attr('dataval', newselectionval);
                  $("#tempselection").html(newselectionval);
                  $("#temppropsel-"+propertyid).html('<i class="fa fa-folder-open"></i>+');
                  $("#temppropsel-"+propertyid).removeClass('btn-primary');
                  $("#temppropsel-"+propertyid).addClass('btn-default');
                  $(this).attr('dataselval', 0);
                  $(".tempdatainput-"+propertyid).remove();
              }                           
          }
          
     });
}

function property_sold() {
  $(document).on('click', '#property_archive', function(event) {
        event.preventDefault();
        var propertystatus = $(".property_status").val();
        var agentid = $(".agent_list").val();
        if(propertystatus == null){
            alert("Please select property status");
            return false;
        }else if(agentid == null && propertystatus == 'SOLD'){
            alert("Please select agent name");
            return false;
        }
        var property_id = $(this).attr('property_id');
        var data = {
            "action": "archive_property",
            "property_id": property_id,
            "propertystatus": propertystatus,
            "agentid" : agentid
        };
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "./ajax/archive_property.php", //Relative or absolute path to response.php file
            data: data,
            success: function (data) {                
                $(".response_message").html(data["message"]);
                $('#myModal').modal('hide');
                if (data["success"] === 'true') {
                    $('#row_' + property_id).remove();
                }
            }
        });
    });

}

function property_status() {
  $(document).on('change', '.property_status', function() {
        var selstatus = $(this).val();
        if(selstatus == "SOLD"){
          var data = {
            "action": "agent_list"
          };
          $.ajax({
            type: "POST",
            dataType: "json",
            url: "./ajax/agent_list.php", //Relative or absolute path to response.php file
            data: data,
            success: function (data) {               
                if(data != ''){
                    var agentlisthtml = '';
                    agentlisthtml += '<label>Sold By : </label>'
                    agentlisthtml += '<select name="agent_list" class="agent_list form-control select2"><option value="" selected disabled>Please select Agent</option>';
                    $.each(data, function(i, item) {
                        agentlisthtml += '<option value='+data[i].user_id+'>'+data[i].first_name +' '+data[i].last_name+'</option>';                        
                    })
                    agentlisthtml += '</select>';
                    $('.agentlist').html(agentlisthtml);
                }else{
                    alert("No agent data found");
                    return false;
                }                                                                       
            }
          });
        }else if(selstatus == "NO LONGER AVAILABLE"){
            $('.agentlist').html('');
            $('.agentcommission').html('');
        }
    });
}

function property_sold_commission(){
    $(document).on('change', '.agent_list', function() {
          var propertyid = $("#property_archive").attr('property_id'); 
          var data = {
            "action": "agent_commission",
            "propertyid" : propertyid
          };
          $.ajax({
            type: "POST",
            dataType: "json",
            url: "./ajax/agent_commission.php", //Relative or absolute path to response.php file
            data: data,
            success: function (data) {               
                if(data != ''){                    
                    $('.agentcommission').html("<label><strong>Commission Amount : &euro;"+ data + "</label></strong>");
                }else{
                    alert("No commission data found");
                    return false;
                }                                                                       
            }
        });
    });
}
