
// Validate signup form
$('form[id="signup"]').validate({
  rules: {
    first_name: 'required',
    last_name: 'required',
    email: {
            required: true,
            email: true,
            remote: {
                url: "ajax/checkemail.php",
                type: "post"
             }
            },
    password: {
      required: true,
    }
  },
  messages: {
    first_name: 'Please enter first name',
    last_name: 'Please enter last name',
    email: {
                    required: "Please Enter Email!",
                    email: "This is not a valid email!",
                    remote: "Email already in use please select other!"
                },
    password: 'Please enter password'
  },
  submitHandler: function(form) {
    form.submit();
  }
});


