<?php
session_start();
require 'config/config.php';
require 'model/model.php';
global $conn;


if(isset($_POST) && !empty($_POST)){
  if($_POST['draftval'] == 1 && empty($_POST['tempinputdata'])){
    echo "<script>window.location='view_draft_all_property.php'</script>";
    exit;
  }else if($_POST['offmarketval'] == 1 && empty($_POST['tempinputdata'])){
    echo "<script>window.location='view_offmarket_all_property.php'</script>";
    exit;
  }else if($_POST['publicval'] == 1 && empty($_POST['tempinputdata'])){
    echo "<script>window.location='view_public_all_property.php'</script>";
    exit;
  }else if($_POST['archivepropval'] == 1 && empty($_POST['tempinputdata'])){
    echo "<script>window.location='view_archive_all_property.php'</script>";
    exit;
  }
  else{
    $allpropid = implode(",", $_POST['tempinputdata']);
    $_SESSION['tempselection'] = $allpropid;
  
    $query = "SELECT * FROM tbl_property WHERE property_id IN (".$allpropid.")";
    $res = mysqli_query($conn, $query);
  }	
}
else{
	echo "<script>window.location='dashboard.php'</script>";
    exit;
}

include('header.php');
include('left_sidebar.php');
?>

<div class="page-wrapper">
  <div class="container-fluid pt-25"> 
    <div  class="pills-struct">
      <ul role="tablist" class="nav nav-pills" id="myTabs_6">        
        <li role="presentation" class=""><a id="my_sale_tab" role="tab" href="view_draft_sale_property.php" aria-expanded="false">My Sale</a></li>
        <li role="presentation" class=""><a id="my_rent_tab" role="tab" href="view_draft_rent_property.php" aria-expanded="false">My Rent</a></li>               
      </ul>
      <div class="tab-content" id="myTabContent_6">
        <div  id="all_tab" class="tab-pane fade active in" role="tabpanel">
         <div class="row">
           <div class="response_message" style="margin-top:20px;margin-bottom:5px; margin-left:0% "></div>
             <div class="col-sm-12">
                <div class="panel panel-default card-view">
                   <div class="panel-heading">
                      <div class="pull-left">
                         <h6 class="panel-title txt-dark">TEMPORARY SELECTION</h6>
                      </div>
                      <div class="clearfix"></div>
                   </div> 
                   <div class="panel-wrapper collapse in">
                      <div class="panel-body">                       
                        <h6 class="property_title"><i class="fa fa-fw fa-th-list color-theme"></i>MY PROPERTY</h6>                        
                        <!-- <div class="row mt-20">
                          <div class="col-md-1 export-btn">                         
                            <div id="buttons"></div>
                          </div>
                        </div> -->
                        <form method="POST" target="_blank" name="prinallproperty" action="print_all_property.php" id="printallprop" class="printallprop">
                         <div class="table-wrap">
                            <div class="table-responsive">
                               <table id="all_tbl" class="display mb-30" >
                                  <thead>
                                     <tr>
                                     	<th>#</th>
                                        <th>UPD</th>
                                        <th>REF</th>
                                        <th>DESIGNATION</th>
                                        <th>LOCATION</th>
                                        <th>PROPERTY TYPE</th>
                                        <th>TRANSECTION TYPE</th>
                                        <th>PRICE/RENT</th>
                                        <th>DIFFUSION</th>
                                        <th>ROOMS</th>
                                        <th>PRICE/M<sup>2</sup></th>
                                        <th>INFOS</th>
                                        <th>CREATED DATE</th>
                                        <th>DATE ON-LINE</th>
                                        <th>FLOOR</th>
                                        <th>NO. OF IMAGES</th>
                                        <th>TOTAL AREA</th>
                                        <th>RETROCESSION</th>                                        
                                                                                          
                                     </tr>
                                  </thead>
                                  <!-- Body part is called form ajx file (ajaxfile.php)    -->
                                 </table>
                            </div>
                         </div>
                         <div class="col-md-1">
                          <?php  if (mysqli_num_rows($res) > 0) {   ?>
                          	<a href="javascript:{}" onclick="document.getElementById('printallprop').submit();" class="btn btn-default left-label"> 
        					            <span class="btn-label">
        					            	<i class="fa fa-print"></i> 
        					            </span>
        					            <span class="btn-text">Print</span>
        					          </a>
                          <?php } ?> 				
                         </div>
                         </form>
                      </div>
                   </div>                  
                </div>
             </div>
          </div>
        </div>
        
      </div>
    </div> 
      
<script>
  
   $(document).ready(function() {
  		  	  
      var dataTable = $('#all_tbl').DataTable({
          sDom: '<lf<t>ip>',
          'processing': true,
          'serverSide': true,
          //'stateSave': true,
          'serverMethod': 'post',
          'searching': false, // Remove default Search Control
         
          'ajax': {
              'url':'ajax/ajaxallpropselection.php',
              'data': function(data){
                  // Read values
                  
                  var transectiontype = "";
                  var propertytype = "";
                  var statepublication = "";
                  var reference = "";
                  var country = "";
                  var nubrooms = "";
                  var startdate = "";
                  var enddate = "";
                  var minarea = "";
                  var maxarea = "";
                  var othercriteria = "";

                                   
                  // Append to data
                  data.transectionType = transectiontype;
                  data.proType = propertytype;
                  data.statePublication = statepublication;
                  data.reference = reference;
                  data.country = country;
                  data.nubRooms = nubrooms;
                  data.startDate = startdate;
                  data.endDate = enddate;
                  data.minArea = minarea;
                  data.maxArea = maxarea;
                  data.otherCriteria = othercriteria;

              }
          },
              'columns': [
              { data: '#' },
              { data: 'updated_at' },
              { data: 'reference' },
              { data: 'property_title' },
              { data: 'building_id'},
              { data: 'property_type' },
              { data: 'transaction_type' },              
              { data: 'price' },
              { data: 'diffusion' },
              { data: 'num_rooms' },
              { data: 'monthly_rent' }, // actully it is a price/m2.
              { "visible": false,data: 'property_title' },
              { "visible": false,data: 'created_at' },
              { "visible": false,data: 'release_date' },
              { "visible": false,data: 'floor' },
              { "visible": false,data: 'photo_id' },
              { "visible": false,data: 'total_area' },
              { "visible": false,data: 'retrocession' },              
          ]
          
      });
      new $.fn.dataTable.Buttons( dataTable, {
           buttons: [
          {
            extend: 'excel',
            text: 'Export',
            className: 'exportExcel',
            filename: 'allselectionpropertydata',
            exportOptions: {
              modifier: {
                page: 'all'
              }
            }
          }]
      } ).container().appendTo($('#buttons'));

          

      $(document).on('click', '.tempselectionpropdata', function(event) {
      		var propid = $(this).attr('dataval');    
      		if($(this).prop("checked") == true){
               $(".temppropdata-"+propid).val(propid);
            }
            else if($(this).prop("checked") == false){                  		      		
      			$(".temppropdata-"+propid).val('');
            }
      		
      });   

     
});


</script>


<?php include('footer.php');?>